/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { AddAnnouncement, EditAnnouncementData, DeleteAnnouncementData } from '../functions/Announcements';

    const addAnnouncementData = {
        start: '05/02/2023',
        end:  '12/01/2023',
        description: 'Test Announcement Automation'
    }

    const editAnnouncementData = {
        start: '05/03/2023',
        end:  '05/04/2023',
        description: 'Test Announcement Automation Edited'
    }
    
    When('i add new annoucement', () => {
        AddAnnouncement(addAnnouncementData, true) // add announcement data, the second parameter is to save or not
    })

    Then('added annoucement should be appear', () => {

    })

    When('i edit the created annoucement', () => {
        EditAnnouncementData(editAnnouncementData, true) // edit announcement data, the second parameter is to save or not
    })

    Then('created annoucement should be updated', () => {
        
    })

    When('i delete the created annoucement', () => {
        DeleteAnnouncementData()
    })

    Then('created annoucement should be deleted', () => {

    })



