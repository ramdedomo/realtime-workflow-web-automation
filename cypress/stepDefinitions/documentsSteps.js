/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { AddDocument } from '../functions/Documents';

    When('i add new document on directory {string}', (directory) => {
        let addDocumentData = {
            directory: directory,
            file: "cypress/functions/data/Realtime_01.png"
        }
      
        AddDocument(addDocumentData) // add document
    })

    Then('document should be added', () => {

    })

