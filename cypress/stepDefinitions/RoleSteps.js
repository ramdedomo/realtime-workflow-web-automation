/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import roleData from '../functions/data/roleData';
import { AddRole,EditRole } from '../functions/RoleTemplate';

    When('I add new role template', () => {
        AddRole(roleData, false) // add role data, second parameter is to save or not
    })

    Then('added new role template should appear', () => {
        
    })

    When('I edit role template {string}', (template) => {
        EditRole(roleData, template, false) // edit role data, second parameter is what role template should be edited and the last parameter is to save or not
    })

    Then('the edited role template should be updated', () => {
        
    })
     
     

 