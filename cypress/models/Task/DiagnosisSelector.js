export default class DiagnosisSelector{
    get M1028_1(){
        return cy.get('#M1028_ACTV_DIAG_PVD_PAD > .ng-pristine')
    }
    get M1028_2(){
        return cy.get('#M1028_ACTV_DIAG_DM > .ng-pristine')
    }
    get M1028_3(){
        return cy.get('#M1028_ACTV_DIAG_NOA > .ng-pristine')
    }

    get M1033_1(){
        return cy.get('#M1033_HOSP_RISK_HSTRY_FALLS > .ng-pristine')
    }
    get M1033_2(){
        return cy.get('#M1033_HOSP_RISK_WEIGHT_LOSS > .ng-pristine')
    }
    get M1033_3(){
        return cy.get('#M1033_HOSP_RISK_MLTPL_HOSPZTN > .ng-pristine')
    }
    get M1033_4(){
        return cy.get('#M1033_HOSP_RISK_MLTPL_ED_VISIT > .ng-pristine')
    }
    get M1033_5(){
        return cy.get('#M1033_HOSP_RISK_MNTL_BHV_DCLN > .ng-pristine')
    }
    get M1033_6(){
        return cy.get('#M1033_HOSP_RISK_COMPLIANCE > .ng-pristine')
    }
    get M1033_7(){
        return cy.get('#M1033_HOSP_RISK_5PLUS_MDCTN > .ng-pristine')
    }
    get M1033_8(){
        return cy.get('#M1033_HOSP_RISK_CRNT_EXHSTN > .ng-pristine')
    }
    get M1033_9(){
        return cy.get('#M1033_HOSP_RISK_OTHR_RISK > .ng-pristine')
    }
    get M1033_9_other(){
        return cy.get('.m-l-230 > .global__txtbox')
    }
    get M1033_10(){
        return cy.get('#M1033_HOSP_RISK_NONE_ABOVE > .ng-pristine')
    }

    get PHRL_high(){
        return cy.get('tr > :nth-child(1) > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get PHRL_moderate(){
        return cy.get('tr > :nth-child(2) > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get PHRL_low(){
        return cy.get('tr > :nth-child(3) > .ng-isolate-scope > .radio > .ng-pristine')
    }

    get sensory_no(){
        return cy.get(':nth-child(2) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get sensory_yes(){
        return  cy.get(':nth-child(2) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get sensory_yes_vision(){
        return  cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[3]/td[2]/table/tbody/tr/td[1]/label/input')
    }
    get sensory_yes_hearing(){
        return  cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[3]/td[2]/table/tbody/tr/td[2]/label/input')
    }
    get sensory_yes_other(){
        return  cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[3]/td[2]/table/tbody/tr/td[3]/input')
    }

    get integumentary_no(){
        return cy.get(':nth-child(4) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get integumentary_yes(){
        return cy.get(':nth-child(4) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get integumentary_yes_lesions(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[5]/td[2]/table/tbody/tr/td[1]/label/input')
    }
    get integumentary_yes_wound(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[5]/td[2]/table/tbody/tr/td[2]/label/input')
    }
    get integumentary_yes_other(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[5]/td[2]/table/tbody/tr/td[3]/input')
    }


    get endocrine_no(){
        return cy.get(':nth-child(6) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get endocrine_yes(){
        return cy.get(':nth-child(6) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get endocrine_yes_diabetes(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[7]/td[2]/table/tbody/tr/td[1]/label/input')
    }
    get endocrine_yes_thyroid(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[7]/td[2]/table/tbody/tr/td[2]/label/input')
    }
    get endocrine_yes_other(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[7]/td[2]/table/tbody/tr/td[3]/input')
    }
   

    get respiratory_no(){
        return cy.get(':nth-child(8) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get respiratory_yes(){
        return cy.get(':nth-child(8) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get respiratory_yes_COPD(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[9]/td[2]/table/tbody/tr/td[1]/div[1]/label/input')
    }
    get respiratory_yes_asthma(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[9]/td[2]/table/tbody/tr/td[1]/div[2]/label/input')
    }
    get respiratory_yes_pnuemonia(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[9]/td[2]/table/tbody/tr/td[2]/div[1]/label/input')  
    }
    get respiratory_yes_emphysema(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[9]/td[2]/table/tbody/tr/td[2]/div[2]/label/input')
    }
    get respiratory_yes_other(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[9]/td[2]/table/tbody/tr/td[3]/div/input')
    }
 


    get cardiovascular_no(){
        return cy.get(':nth-child(10) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get cardiovascular_yes(){
        return cy.get(':nth-child(10) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get cardiovascular_yes_CHF(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[11]/td[2]/table/tbody/tr/td[1]/div[1]/label/input')
    }
    get cardiovascular_yes_HTN(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[11]/td[2]/table/tbody/tr/td[1]/div[2]/label/input')
    }
    get cardiovascular_yes_arrhythmia(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[11]/td[2]/table/tbody/tr/td[1]/div[3]/label/input')
    }
    get cardiovascular_yes_cardiomyopathy(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[11]/td[2]/table/tbody/tr/td[2]/div[1]/label/input')
    }
    get cardiovascular_yes_CAD(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[11]/td[2]/table/tbody/tr/td[2]/div[2]/label/input')
    }
    get cardiovascular_yes_PVD(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[11]/td[2]/table/tbody/tr/td[2]/div[3]/label/input')
    }
    get cardiovascular_yes_other(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[11]/td[2]/table/tbody/tr/td[3]/div/input')
    }


    get gastrointestinal_no(){
        return cy.get(':nth-child(12) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get gastrointestinal_yes(){
        return cy.get(':nth-child(12) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get gastrointestinal_yes_constipation(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[13]/td[2]/table/tbody/tr/td[1]/div[1]/label/input')
    }
    get gastrointestinal_yes_crohn(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[13]/td[2]/table/tbody/tr/td[1]/div[2]/label/input')
    }
    get gastrointestinal_yes_fecal(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[13]/td[2]/table/tbody/tr/td[1]/div[3]/label/input')
    }
    get gastrointestinal_yes_diverticulitis(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[13]/td[2]/table/tbody/tr/td[2]/div[1]/label/input')
    }
    get gastrointestinal_yes_liver(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[13]/td[2]/table/tbody/tr/td[2]/div[2]/label/input')
    }
    get gastrointestinal_yes_IBS(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[13]/td[2]/table/tbody/tr/td[2]/div[3]/label/input')
    }
    get gastrointestinal_yes_other(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[13]/td[2]/table/tbody/tr/td[3]/div/input')
    }





    get genitourinary_no(){
        return cy.get(':nth-child(14) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get genitourinary_yes(){
        return cy.get(':nth-child(14) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get genitourinary_yes_urinaryincontinence(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[15]/td[2]/table/tbody/tr/td[1]/div[1]/label/input')
    }
    get genitourinary_yes_chronic(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[15]/td[2]/table/tbody/tr/td[1]/div[2]/label/input')
    }
    get genitourinary_yes_urinaryretention(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[15]/td[2]/table/tbody/tr/td[1]/div[3]/label/input')
    }
    get genitourinary_yes_dialysis(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[15]/td[2]/table/tbody/tr/td[2]/div[1]/label/input')
    }
    get genitourinary_yes_recent(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[15]/td[2]/table/tbody/tr/td[2]/div[2]/label/input')
    }
    get genitourinary_yes_BPH(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[15]/td[2]/table/tbody/tr/td[2]/div[3]/label/input')
    }
    get genitourinary_yes_other(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[15]/td[2]/table/tbody/tr/td[3]/input')
    }
    
 

    get neurological_no(){
        return cy.get(':nth-child(16) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get neurological_yes(){
        return cy.get(':nth-child(16) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get neurological_yes_headaches(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[17]/td[2]/table/tbody/tr/td[1]/div[1]/label/input')
    }
    get neurological_yes_dizziness(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[17]/td[2]/table/tbody/tr/td[1]/div[2]/label/input')
    }
    get neurological_yes_parkinson(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[17]/td[2]/table/tbody/tr/td[1]/div[3]/label/input')
    }
    get neurological_yes_CVA(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[17]/td[2]/table/tbody/tr/td[2]/div[1]/label/input')
    }
    get neurological_yes_TIA(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[17]/td[2]/table/tbody/tr/td[2]/div[2]/label/input')
    }
    get neurological_yes_alzheimer(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[17]/td[2]/table/tbody/tr/td[2]/div[3]/label/input')
    }
    get neurological_yes_multiple(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[17]/td[2]/table/tbody/tr/td[3]/div[1]/label/input')
    }
    get neurological_yes_seizure(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[17]/td[2]/table/tbody/tr/td[3]/div[2]/label/input')
    }
    get neurological_yes_paralysis(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[17]/td[2]/table/tbody/tr/td[3]/div[3]/label/input')
    }
    get neurological_yes_other(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[17]/td[2]/table/tbody/tr/td[4]/div[2]/div/input')
    }

    

    get musculoskeletal_no(){
        return cy.get(':nth-child(18) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get musculoskeletal_yes(){
        return cy.get(':nth-child(18) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get musculoskeletal_yes_rheumatoid(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[19]/td[2]/table/tbody/tr/td[1]/div[1]/label/input')
    }
    get musculoskeletal_yes_osteoarthritis(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[19]/td[2]/table/tbody/tr/td[1]/div[2]/label/input')
    }
    get musculoskeletal_yes_fall(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[19]/td[2]/table/tbody/tr/td[1]/div[3]/label/input')
    }
    get musculoskeletal_yes_gait(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[19]/td[2]/table/tbody/tr/td[2]/div[1]/label/input')
    }
    get musculoskeletal_yes_fractures(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[19]/td[2]/table/tbody/tr/td[2]/div[2]/label/input')
    }
    get musculoskeletal_yes_joint(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[19]/td[2]/table/tbody/tr/td[2]/div[3]/label/input')
    }
    get musculoskeletal_yes_spinal(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[19]/td[2]/table/tbody/tr/td[3]/div[1]/label/input')
    }
    get musculoskeletal_yes_muscular(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[19]/td[2]/table/tbody/tr/td[3]/div[2]/label/input')
    }
    get musculoskeletal_yes_other(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[19]/td[2]/table/tbody/tr/td[3]/div[3]/input')
    }
    

    get circulatory_no(){
        return cy.get(':nth-child(20) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get circulatory_yes(){
        return cy.get(':nth-child(20) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get circulatory_yes_anemia(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[21]/td[2]/table/tbody/tr/td[1]/label/input')
    }
    get circulatory_yes_abnormal(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[21]/td[2]/table/tbody/tr/td[2]/label/input')
    }
    get circulatory_yes_other(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[21]/td[2]/table/tbody/tr/td[3]/input')
    }


    get other_no(){
        return cy.get(':nth-child(22) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get other_yes(){
        return cy.get(':nth-child(22) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get other_yes_cancer(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[23]/td[2]/table/tbody/tr/td[1]/div[1]/label/input')
    }
    get other_yes_infectious(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[23]/td[2]/table/tbody/tr/td[1]/div[2]/label/input')
    }
    get other_yes_tobacco(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[23]/td[2]/table/tbody/tr/td[1]/div[3]/label/input')
    }
    get other_yes_substance(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[23]/td[2]/table/tbody/tr/td[2]/div[1]/label/input')
    }
    get other_yes_trauma(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[23]/td[2]/table/tbody/tr/td[2]/div[2]/label/input')
    }
    get other_yes_mental(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[23]/td[2]/table/tbody/tr/td[2]/div[3]/label/input')
    }
    get other_yes_other(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[2]/tbody/tr[23]/td[2]/table/tbody/tr[1]/td[3]/div[2]/div/input')
    }
 

    get surgical_no(){
        return cy.get(':nth-child(24) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get surgical_yes(){
        return cy.get(':nth-child(24) > .b-l-0 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get surgical_yes_surgeries(){
        return cy.get('[style="width: 60%;"] > .global__txtbox')
    }

    get hospitalizations_none(){
        return cy.get('.inner__table-nb > tbody > tr > [style="width: 7%;"] > .checkbox > .ng-valid')
    }
    get hospitalizations(){
        return cy.get(':nth-child(25) > .subheadv3 > .inner__table-nb > tbody > tr > td.ng-isolate-scope > .global__txtbox')
    }
    

    get exam(){
        return cy.get('[style="width: 36%; margin-top: 2px;"] > .global__txtbox')
    }
    get examDate(){
        return cy.get('[style="width: 10%;"] > .global__txtbox')
    }
    get examResult(){
        return cy.get('[style="width: 33%;"] > .global__txtbox')
    }
    get examAdd(){
        return cy.get('.bg__primary > .zmdi')
    }
    get allergies_none(){
        return cy.get('#tooltip_wr15 > .checkbox > .ng-valid')
    }

    get datecontacted(){
        return cy.get(':nth-child(1) > .p-5 > .global__txtbox')
    }
    get datevisit(){
        return cy.get('.p-l-5 > .global__txtbox')
    }
    get datereason(){
        return cy.get(':nth-child(3) > .p-5 > .global__txtbox')
    }

    get influenza_yes(){
        return cy.get('[style="margin-right: 14px;"] > .ng-valid')
    } 
    get influenza_yes_date(){
        return cy.get('[name="SOOMEDICAL0058"]')
    } 
    get influenza_yes_agency(){
        return cy.get('.m-r-5 > .p-b-5 > .ng-valid')
    } 
    get influenza_yes_pcp(){
        return cy.get('.m-r-5 > :nth-child(5) > .ng-pristine')
    } 
    get influenza_yes_facility(){
        return cy.get('.m-r-5 > :nth-child(6) > .ng-pristine')
    } 
    get influenza_yes_pharmacy(){
        return cy.get('.m-r-5 > [style="margin: 0px; margin-right: 10px;"] > .ng-pristine')
    } 
    get influenza_yes_other(){
        return cy.get(':nth-child(1) > :nth-child(3) > .m-r-5 > .global__txtbox.ng-pristine')
    } 
    get influenza_no(){
        return cy.get(':nth-child(4) > :nth-child(1) > table > tbody > tr > td > .radio > .ng-valid')
    } 
    get influenza_no_reason(){
        return cy.get('[style="vertical-align: top; border-bottom: thin solid #888; padding: 3px 4.5px;"] > table > tbody > tr > td > .global__txtbox')
    } 
    get influenza_offered_agree(){
        return cy.get(':nth-child(5) > td.ng-isolate-scope > .radio.ng-isolate-scope > .ng-pristine')
    } 
    get influenza_offered_agency(){
        return cy.get(':nth-child(6) > td.ng-isolate-scope > :nth-child(1) > .ng-pristine')
    } 
    get influenza_offered_clinic(){
        return cy.get(':nth-child(6) > td.ng-isolate-scope > :nth-child(2) > .ng-pristine')
    } 
    get influenza_offered_pharmacy(){
        return cy.get(':nth-child(6) > td.ng-isolate-scope > :nth-child(3) > .ng-pristine')
    } 
    get influenza_offered_disagree(){
        return cy.get(':nth-child(5) > td.ng-isolate-scope > :nth-child(2) > .ng-pristine')
    } 
    
     
    get pneumonia_yes(){
        return cy.get('[style="margin-right: 10px;"] > .ng-pristine')
    } 
    get pneumonia_yes_date(){
        return cy.get('[name="SOOMEDICAL0062"]')
    } 
    get pneumonia_yes_agency(){
        return cy.get('.p-b-5 > .ng-pristine')
    } 
    get pneumonia_yes_pcp(){
        return cy.get(':nth-child(8) > td > :nth-child(5) > .ng-pristine')
    } 
    get pneumonia_yes_facility(){
        return cy.get(':nth-child(8) > td > :nth-child(6) > .ng-pristine')
    } 
    get pneumonia_yes_pharmacy(){
        return cy.get(':nth-child(8) > td > [style="margin: 0px; margin-right: 10px;"] > .ng-pristine')
    } 
    get pneumonia_yes_other(){
        return cy.get(':nth-child(1) > :nth-child(3) > .m-r-5 > .global__txtbox.ng-pristine')
    } 
    get pneumonia_no(){
        return cy.get('[style="vertical-align: top; border-bottom: thin solid #888; padding: 3px 4.5px;"] > table > tbody > tr > td > .radio > .ng-pristine')
    } 
    get pneumonia_no_reason(){
        return cy.get('[style="vertical-align: top; border-bottom: thin solid #888; padding: 3px 4.5px;"] > table > tbody > tr > td > .global__txtbox')
    } 
    get pneumonia_offered_agree(){
        return cy.get(':nth-child(10) > td.ng-isolate-scope > .radio.ng-isolate-scope > .ng-valid')
    } 
    get pneumonia_offered_agency(){
        return cy.get(':nth-child(11) > td.ng-isolate-scope > :nth-child(1) > .ng-pristine')
    } 
    get pneumonia_offered_clinic(){
        return cy.get(':nth-child(11) > td.ng-isolate-scope > :nth-child(2) > .ng-pristine')
    } 
    get pneumonia_offered_pharmacy(){
        return cy.get(':nth-child(11) > td.ng-isolate-scope > :nth-child(3) > .ng-pristine')
    } 
    get pneumonia_offered_disagree(){
        return cy.get(':nth-child(10) > td.ng-isolate-scope > :nth-child(2) > .ng-pristine')
    } 

}
