/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';

    Given('I navigate to Realtime Workflow Website', () => {
        cy.visit('https://qado.medisource.com/login')  // visit website
    })
    When('I enter login credentials {string} and {string}', (email, password) => {
        cy.get('#loginemail').type(email) // type email
        cy.get('#loginpassword').type(password) // type password
        cy.get('.mhLP-cta-py').click() // click login
    })

    Then('I should be logged in', () => {
        cy.contains('Dashboard').should('be.visible') // check if redirected to dashboard
    })

    When('I log out', () => {
        cy.get('.with-nameplate').click() // click name
        cy.get('.user-btn-default').click() // click logout
    })

    Then('I should be logged out', () => {
        cy.contains('Login').should('be.visible') // check if redirected to login page
    })

    
