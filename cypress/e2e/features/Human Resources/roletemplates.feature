Feature: User List

    Scenario: Finding a User and Edit Information

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Human Resources'
        Then a selected main nav should be expand

        # When I click the sub nav 'Add Role Template'
        # Then a selected sub nav should be display 

        # When I add new role template
        # Then added new role template should appear

        When I click the sub nav 'Role Template List'
        Then a selected sub nav should be display 

        When I edit role template 'Test Role Template Automation'
        Then the edited role template should be updated