export default class CognitiveSelector{
    get C0100(){
        return cy.get('#C0100Txt')
    }
    get C0200(){
        return cy.get('.form-group > #C0100')
    }

    get C0300A(){
        return cy.get('#C0300A')
    }
    get C0300B(){
        return cy.get('#C0300B')
    }
    get C0300C(){
        return cy.get('#C0300C')
    }

    get C0400A(){
        return cy.get('#C0400A')
    }
    get C0400B(){
        return cy.get('#C0400B')
    }
    get C0400C(){
        return cy.get('#C0400C')
    }
    
    get C05001(){
        return cy.get('#code1')
    }
    get C05002(){
        return cy.get('#code2')
    }

    get C1310A(){
        return cy.get('#codea')
    }
    get C1310B(){
        return cy.get('#codeb')
    }
    get C1310C(){
        return cy.get('#codec')
    }
    get C1310D(){
        return cy.get('#coded')
    }

    get M1700(){
        return cy.get('#M1700_COG_FUNCTION_TEMP')
    }
    get M1710(){
        return cy.get('#M1710_WHEN_CONFUSED_TEMP')
    }
    get M1720(){
        return cy.get('#M1720_WHEN_ANXIOUS_TEMP')
    }

    get D0150A_0(){
        return cy.get(':nth-child(6) > :nth-child(3) > div > .radio > .ng-valid')
    }
    get D0150A_1(){
        return cy.get(':nth-child(6) > :nth-child(4) > div > .radio > .ng-valid')
    }
    get D0150A_9(){
        return cy.get(':nth-child(6) > :nth-child(5) > div > .radio > .ng-valid')
    }
    get D0150A_ZERO(){
        return cy.get(':nth-child(6) > :nth-child(7) > div > .radio > .ng-valid')
    }
    get D0150A_ONE(){
        return cy.get(':nth-child(6) > :nth-child(8) > div > .radio > .ng-valid')
    }
    get D0150A_TWO(){
        return cy.get(':nth-child(6) > :nth-child(9) > div > .radio > .ng-valid')
    }
    get D0150A_THREE(){
        return cy.get(':nth-child(6) > .b-r-0.ng-isolate-scope > div > .radio > .ng-valid')
    }


    get D0150B_0(){
        return cy.get(':nth-child(7) > :nth-child(3) > div > .radio > .ng-valid')
    }
    get D0150B_1(){
        return cy.get(':nth-child(7) > :nth-child(4) > div > .radio > .ng-valid')
    }
    get D0150B_9(){
        return cy.get(':nth-child(7) > :nth-child(5) > div > .radio > .ng-valid')
    }
    get D0150B_ZERO(){
        return cy.get(':nth-child(7) > :nth-child(7) > div > .radio > .ng-valid')
    }
    get D0150B_ONE(){
        return cy.get(':nth-child(7) > :nth-child(8) > div > .radio > .ng-valid')
    }
    get D0150B_TWO(){
        return cy.get(':nth-child(7) > :nth-child(9) > div > .radio > .ng-pristine')
    }
    get D0150B_THREE(){
        return cy.get(':nth-child(7) > .b-r-0.ng-isolate-scope > div > .radio > .ng-pristine')
    }






    get D0150C_0(){
        return cy.get(':nth-child(9) > :nth-child(3) > div > .radio > .ng-valid')
    }
    get D0150C_1(){
        return cy.get(':nth-child(9) > :nth-child(4) > div > .radio > .ng-valid')
    }
    get D0150C_9(){
        return cy.get(':nth-child(9) > :nth-child(5) > div > .radio > .ng-valid')
    }
    get D0150C_ZERO(){
        return cy.get(':nth-child(9) > :nth-child(7) > div > .radio > .ng-pristine')
    }
    get D0150C_ONE(){
        return cy.get(':nth-child(9) > :nth-child(8) > div > .radio > .ng-pristine')
    }
    get D0150C_TWO(){
        return cy.get(':nth-child(9) > :nth-child(9) > div > .radio > .ng-pristine')
    }
    get D0150C_THREE(){
        return cy.get(':nth-child(9) > .b-r-0.ng-isolate-scope > div > .radio > .ng-pristine')
    }


    get D0150D_0(){
        return cy.get(':nth-child(10) > :nth-child(3) > div > .radio > .ng-valid')
    }
    get D0150D_1(){
        return cy.get(':nth-child(10) > :nth-child(4) > div > .radio > .ng-valid')
    }
    get D0150D_9(){
        return cy.get(':nth-child(10) > :nth-child(5) > div > .radio > .ng-valid')
    }
    get D0150D_ZERO(){
        return cy.get(':nth-child(10) > :nth-child(7) > div > .radio > .ng-pristine')
    }
    get D0150D_ONE(){
        return cy.get(':nth-child(10) > :nth-child(8) > div > .radio > .ng-pristine')
    }
    get D0150D_TWO(){
        return cy.get(':nth-child(10) > :nth-child(9) > div > .radio > .ng-pristine')
    }
    get D0150D_THREE(){
        return cy.get(':nth-child(10) > .b-r-0.ng-isolate-scope > div > .radio > .ng-pristine')
    }

  

    get D0150E_0(){
        return cy.get(':nth-child(11) > :nth-child(3) > div > .radio > .ng-valid')
    }
    get D0150E_1(){
        return cy.get(':nth-child(11) > :nth-child(4) > div > .radio > .ng-valid')
    }
    get D0150E_9(){
        return cy.get(':nth-child(11) > :nth-child(5) > div > .radio > .ng-valid')
    }
    get D0150E_ZERO(){
        return cy.get(':nth-child(11) > :nth-child(7) > div > .radio > .ng-pristine')
    }
    get D0150E_ONE(){
        return cy.get(':nth-child(11) > :nth-child(8) > div > .radio > .ng-pristine')
    }
    get D0150E_TWO(){
        return cy.get(':nth-child(11) > :nth-child(9) > div > .radio > .ng-pristine')
    }
    get D0150E_THREE(){
        return cy.get(':nth-child(11) > .b-r-0.ng-isolate-scope > div > .radio > .ng-pristine')
    }


    get D0150F_0(){
        return cy.get(':nth-child(12) > :nth-child(3) > div > .radio > .ng-valid')
    }
    get D0150F_1(){
        return cy.get(':nth-child(12) > :nth-child(4) > div > .radio > .ng-valid')
    }
    get D0150F_9(){
        return cy.get(':nth-child(12) > :nth-child(5) > div > .radio > .ng-valid')
    }
    get D0150F_ZERO(){
        return cy.get(':nth-child(12) > :nth-child(7) > div > .radio > .ng-pristine')
    }
    get D0150F_ONE(){
        return cy.get(':nth-child(12) > :nth-child(8) > div > .radio > .ng-pristine')
    }
    get D0150F_TWO(){
        return cy.get(':nth-child(12) > :nth-child(9) > div > .radio > .ng-pristine')
    }
    get D0150F_THREE(){
        return cy.get(':nth-child(12) > .b-r-0.ng-isolate-scope > div > .radio > .ng-pristine')
    }


    
    get D0150G_0(){
        return cy.get(':nth-child(13) > :nth-child(3) > div > .radio > .ng-valid')
    }
    get D0150G_1(){
        return cy.get(':nth-child(13) > :nth-child(4) > div > .radio > .ng-valid')
    }
    get D0150G_9(){
        return cy.get(':nth-child(13) > :nth-child(5) > div > .radio > .ng-valid')
    }
    get D0150G_ZERO(){
        return cy.get(':nth-child(13) > :nth-child(7) > div > .radio > .ng-pristine')
    }
    get D0150G_ONE(){
        return cy.get(':nth-child(13) > :nth-child(8) > div > .radio > .ng-pristine')
    }
    get D0150G_TWO(){
        return cy.get(':nth-child(13) > :nth-child(9) > div > .radio > .ng-pristine')
    }
    get D0150G_THREE(){
        return cy.get(':nth-child(13) > .b-r-0.ng-isolate-scope > div > .radio > .ng-pristine')
    }
    




    get D0150H_0(){
        return cy.get(':nth-child(14) > :nth-child(3) > div > .radio > .ng-valid')
    }
    get D0150H_1(){
        return cy.get(':nth-child(14) > :nth-child(4) > div > .radio > .ng-valid')
    }
    get D0150H_9(){
        return cy.get(':nth-child(14) > :nth-child(5) > div > .radio > .ng-valid')
    }
    get D0150H_ZERO(){
        return cy.get(':nth-child(14) > :nth-child(7) > div > .radio > .ng-pristine')
    }
    get D0150H_ONE(){
        return cy.get(':nth-child(14) > :nth-child(8) > div > .radio > .ng-pristine')
    }
    get D0150H_TWO(){
        return cy.get(':nth-child(14) > :nth-child(9) > div > .radio > .ng-pristine')
    }
    get D0150H_THREE(){
        return cy.get(':nth-child(14) > .b-r-0.ng-isolate-scope > div > .radio > .ng-pristine')
    }


    get D0150I_0(){
        return cy.get(':nth-child(15) > :nth-child(3) > div > .radio > .ng-valid')
    }
    get D0150I_1(){
        return cy.get(':nth-child(15) > :nth-child(4) > div > .radio > .ng-valid')
    }
    get D0150I_9(){
        return cy.get(':nth-child(15) > :nth-child(5) > div > .radio > .ng-valid')
    }
    get D0150I_ZERO(){
        return cy.get(':nth-child(15) > :nth-child(7) > div > .radio > .ng-pristine')
    }
    get D0150I_ONE(){
        return cy.get(':nth-child(15) > :nth-child(8) > div > .radio > .ng-pristine')
    }
    get D0150I_TWO(){
        return cy.get(':nth-child(15) > :nth-child(9) > div > .radio > .ng-pristine')
    }
    get D0150I_THREE(){
        return cy.get(':nth-child(15) > .b-r-0.ng-isolate-scope > div > .radio > .ng-pristine')
    }

    
    get D0700(){
        return cy.get('#D0700Txt')
    }

    get M1740_1(){
        return cy.get('#M1740_BD_MEM_DEFICIT > td > .m-b-5 > .checkbox > .ng-pristine')
    }
    get M1740_2(){
        return cy.get(':nth-child(2) > td > .m-b-5 > .checkbox > .ng-pristine')
    }
    get M1740_3(){
        return cy.get(':nth-child(3) > td > .m-b-5 > .checkbox > .ng-pristine')
    }
    get M1740_4(){
        return cy.get(':nth-child(4) > td > .m-b-5 > .checkbox > .ng-pristine')
    }
    get M1740_5(){
        return cy.get(':nth-child(5) > td > .m-b-5 > .checkbox > .ng-pristine')
    }
    get M1740_6(){
        return cy.get('#M1740_BD_DELUSIONS > td > .m-b-5 > .checkbox > .ng-pristine')
    }
    get M1740_7(){
        return cy.get('#M1740_BD_NONE > td > .m-b-5 > .checkbox > .ng-pristine')
    }

    get M1745(){
        return cy.get('.form-group > #M1745_BEH_PROB_FREQ')
    }


}