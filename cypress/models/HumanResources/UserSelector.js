export default class UserSelector{
    //User Profile and Credentials
     get editbutton(){
        return cy.get('.btn-edit > .zmdi')
    }

    //User Account
    get editaccountbutton(){
        return cy.get('.btn-editacc')
    }

    //Reset Password
    get resetpwbutton(){
        return cy.get('.btn-resetpw')
    }

    //firsttab
       get personalinfo(){
        return cy.get('#content > data > div.patient-manager.globallist__header_patient.ng-scope > div > div > div > div.clearfix > ul > li.active > a')
    }

    //secondtab
    get professionalcredentials(){
        return cy.get('#content > data > div.patient-manager.globallist__header_patient.ng-scope > div > div > div > div.clearfix > ul > li:nth-child(2) > a')
    }

    //thirdtab
    get healthcredentials(){
        return cy.get('#content > data > div.patient-manager.globallist__header_patient.ng-scope > div > div > div > div.clearfix > ul > li:nth-child(3) > a')
    }
    
   //next
    get next(){
        return cy.get('.ng-scope > .btn')
    }

    get save(){
        return cy.get('.btn__success')
    }
    
}