import Template485Selector from "../models/Templates/Template485Selector";
import TemplateClassificationSelector from '../models/Templates/TemplateClassificationSelector'
import TemplateWoundSelector from "../models/Templates/TemplateWoundSelector";
import TemplateMDOSelector from "../models/Templates/TemplateMDOSelector";

const template485 = new Template485Selector();
const templateClassification = new TemplateClassificationSelector();
const templateWound = new TemplateWoundSelector();
const templateMDO = new TemplateMDOSelector();

function Add485Template(templateData = {}, issave = false){
    cy.visit('https://qado.medisource.com/narratives/templates')
    cy.get('.horizontal__inline > .btn').click()
    cy.wait(2000)

    template485.ICD.type(templateData.ICD)

    const bodysystem = templateData.bodysystem
    if(bodysystem.newbodysystem.add){
        //new bodysystem
        const newbodysystem = bodysystem.newbodysystem

        template485.newbodysystemClick.click()
        template485.newbodysystemDomain.type(newbodysystem.domain)
        template485.newbodysystemGeneral.type(newbodysystem.general)
        template485.modalsaveNew.click();

        template485.bodysystemClick.click()
        template485.bodysystemEnter.type(newbodysystem.domain)
        cy.get('.highlighted').click();
    }else{
        //select bodysystem
        template485.bodysystemClick.click()
        template485.bodysystemEnter.type(bodysystem.bodysystem)
        cy.get('.highlighted').click();
    }


    const category = templateData.category
    if(category.newcategory.add){
        //new category
        const newcategory = category.newcategory

        template485.newcategoryClick.click()
        template485.newcategoryClassification.type(newcategory.classification)
        template485.newcategoryGeneral.type(newcategory.general)
        template485.modalsaveNew.click();

        template485.categoryClick.click()
        template485.categoryEnter.type(newcategory.classification)
        cy.get('.highlighted').click();
    }else{
        //select category
        template485.categoryClick.click()
        template485.categoryEnter.type(newcategory.classification)
        cy.get('.highlighted').click();
    }


    template485.diagnosis.type(templateData.diagnosis)
    template485.goal.type(templateData.goal)
    
    const sntoperform = templateData.sntoperform.string
    const sntoteach = templateData.sntoteach.string
    const sntoreport = templateData.sntoreport.string

    for (let index = 1; index < sntoperform.length; index++) {
        template485.sntoperformAdd.click()
    }
    for (let index = 1; index < sntoteach.length; index++) {
        template485.sntoteachAdd.click()
    }
    for (let index = 1; index < sntoreport.length; index++) {
        template485.sntoreportAdd.click()
    }
    cy.wait(1000)


    for (let index = 1; index <= sntoperform.length; index++) {
        cy.get(':nth-child(8) > .oasis__answer > .row > .col-sm-8 > :nth-child('+String(index)+') > table > tbody > tr > td > .form-control').type(sntoperform[index-1])
    }
    for (let index = 1; index <= sntoteach.length; index++) {
        cy.get(':nth-child(10) > .oasis__answer > .row > .col-sm-8 > :nth-child('+String(index)+') > table > tbody > tr > td > .form-control').type(sntoteach[index-1])
    }
    for (let index = 1; index <= sntoreport.length; index++) {
        cy.get(':nth-child(14) > .oasis__answer > .row > .col-sm-8 > :nth-child('+String(index)+') > table > tbody > tr > td > .form-control').type(sntoreport[index-1])
    }

    if(issave){
        cy.get('.btn__success').click()
    }

}

function AddClassificationTemplate(templateData = {}, issave = false){
    cy.get('[ui-sref="narratives.classifications"] > .navtitle').click()
    cy.get('.horizontal__inline > .btn').click()
    cy.wait(2000)

    templateClassification.categoryClick.click()
    templateClassification.categoryEnter.type(templateData.category)
    cy.get('.highlighted').click();

    templateClassification.classification.type(templateData.classification)

    const general = templateData.generalassesment

    for (let index = 1; index < general.length; index++) {
        templateClassification.generalassesmentAdd.click()
    }

    for (let index = 1; index <= general.length; index++) {
        cy.get(`:nth-child(${index}) > table > tbody > tr > td > .form-control`).type(general[index-1])
    }

    if(issave){
        cy.get('.btn__success').click()
    }

}

function AddWoundTemplate(templateData = {}, issave = false){
    cy.get('[ui-sref="narratives.treatmentorder"] > .navtitle').click()
    cy.get('.horizontal__inline > .btn').click()
    cy.wait(2000)

    templateWound.fieldClick.click()
    templateWound.fieldEnter.type(templateData.field)
    cy.get('.highlighted').click();

    templateWound.entry.type(templateData.entry)

    issave && cy.get('.btn__success').click()
}

function AddMDOTemplate(templateData = {}, issave = false){
    cy.get('[ui-sref="narratives.mdorder"] > .navtitle').click()
    cy.get('.horizontal__inline > .btn').click()
    cy.wait(2000)

    templateMDO.title.type(templateData.title)
    templateMDO.description.type(templateData.description)

    issave && cy.get('.btn__success').click()
}


export { Add485Template, AddClassificationTemplate, AddWoundTemplate, AddMDOTemplate }