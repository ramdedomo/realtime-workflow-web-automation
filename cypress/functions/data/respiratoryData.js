export default {
    M1400: "1",
    respiratory: {
        wnl: false,
        isbreathclear: false,
        options: {
            respiratory_diminished_left: true,
            respiratory_diminished_right: false,
            respiratory_diminished_anterior: true,
            respiratory_diminished_posterior: false,
            respiratory_diminished_upper: true,
            respiratory_diminished_middle: false,
            respiratory_diminished_lower: true,
            //
            respiratory_absent_left: false,
            respiratory_absent_right: true,
            respiratory_absent_anterior: false,
            respiratory_absent_posterior: true,
            respiratory_absent_upper: false,
            respiratory_absent_middle: true,
            respiratory_absent_lower: false,
            //
            respiratory_rales_left: true,
            respiratory_rales_right: false,
            respiratory_rales_anterior: true,
            respiratory_rales_posterior: false,
            respiratory_rales_upper: true,
            respiratory_rales_middle: false,
            respiratory_rales_lower: true,
            //
            respiratory_rhonchi_left: false,
            respiratory_rhonchi_right: true,
            respiratory_rhonchi_anterior: false,
            respiratory_rhonchi_posterior: true,
            respiratory_rhonchi_upper: false,
            respiratory_rhonchi_middle: true,
            respiratory_rhonchi_lower: false,
            //
            respiratory_wheeze_left: true,
            respiratory_wheeze_right: false,
            respiratory_wheeze_anterior: true,
            respiratory_wheeze_posterior: false,
            respiratory_wheeze_upper: true,
            respiratory_wheeze_middle: false,
            respiratory_wheeze_lower: true,
            //
            respiratory_stridor_left: false,
            respiratory_stridor_right: true,
            respiratory_stridor_anterior: false,
            respiratory_stridor_posterior: true,
            respiratory_stridor_upper: false,
            respiratory_stridor_middle: true,
            respiratory_stridor_lower: false,
        }
    },
    abnormal: {
        abnormal: true,
        options: {
            respiratory_abnormal_rapid: true,
            respiratory_abnormal_irregular: true,
            respiratory_abnormal_apnes: true,
            respiratory_abnormal_shallow: true,
            respiratory_abnormal_deep: true,
            respiratory_abnormal_gasping: true,
            respiratory_abnormal_noisy: true,
            respiratory_abnormal_cheyne: true,
            respiratory_abnormal_orthopnea: true,
            respiratory_abnormal_accesory: true,
            respiratory_abnormal_other: "Test Other",
        }
    },
    cough: {
        cough: true,
        options: {
            respiratory_character_productive: true,
            respiratory_character_nonproductive: false,
            respiratory_character_unable: false,
            //
            respiratory_sputum_white: false,
            respiratory_sputum_yellow: false,
            respiratory_sputum_brown: true,
            respiratory_sputum_green: false,
            respiratory_sputum_bloody: false,
            //
            respiratory_sputumcharacter_thin: true,
            respiratory_sputumcharacter_thick: false,
            respiratory_sputumcharacter_tenacious: false,
            respiratory_sputumcharacter_frothy: false,
            //
            respiratory_sputumamount_small: false,
            respiratory_sputumamount_moderate: true,
            respiratory_csputumamount_large: false,
        }
    },
    specialprod: {
        specialprod: true,
        o2therapy:{
            o2therapy: true,
            options: {
                special_o2_q1_yes: true,
                special_o2_q1_no: false,
                //
                special_o2_q2_yes: false,
                special_o2_q2_no: true,
                //
                special_o2_q3_yes: true,
                special_o2_q3_no: false,
                //
                special_o2_q4_yes: false,
                special_o2_q4_no: true,
                //
                special_o2_q5_yes: true,
                special_o2_q5_no: false,
                //
                special_o2_q6_yes: false,
                special_o2_q6_no: true,
                //
                special_o2_q7_yes: true,
                special_o2_q7_no: false,
                //
                special_o2_q8_yes: false,
                special_o2_q8_no: true,
                //
                special_o2_q9_gas: false,
                special_o2_q9_fire: true,
                special_o2_q9_lantern: false,
                special_o2_q9_candles: true,
                special_o2_q9_kerosene: false,
                special_o2_q9_yes: true,
                special_o2_q9_no: false,
                special_o2_observation: "Test Observation",
                special_o2_intervention: "Test Intervention"
            },
        
        },
        tracheostomy:{
            tracheostomy: true,
            options: {
                special_trancheostomy_brand: "Test Brand",
                special_trancheostomy_tubechange: "3", 
                special_trancheostomy_lastchanged: "05/08/2023", 
                special_trancheostomy_innercannula_none: true, 
            }
        },
        BiPAP:{
            BiPAP: true,
            options: {
                special_BiPAP_brand: "Test Brand",
                special_BiPAP_working_yes: true,
                special_BiPAP_working_no: false,
                special_BiPAP_complaint_yes: true,
                special_BiPAP_complaint_no: false,
                special_BiPAP_observation: "Test Observation",
                special_BiPAP_intervention: "Test Intervention"
            }
        },
        suctioning :{
            suctioning: true,
            options: {
                special_suctioning_oral: true,
                special_suctioning_trancheostomy: false,
                special_suctioning_nasopharyngeal: false,
                //
                special_suctioning_asneeded: true,
                //
                special_suctioning_sunctionsched: true,
                special_suctioning_sunctionscheddate: "05/08/2023",
                //
                special_suctioning_q1_yes: false,
                special_suctioning_q1_no: true,
                //
                special_suctioning_q2_yes: false,
                special_suctioning_q2_no: true,
                //
                special_suctioning_doneby_patient: true,
                special_suctioning_doneby_caregiver: true,
                special_suctioning_doneby_sn: true,
                //
                special_suctioning_observation: "Test Observation",
                special_suctioning_intervention: "Test Intervention"
            },
        },
        ventilator:{
            ventilator: true,
            options: {
                special_ventilator_brand: "Test Brand",
                special_ventilator_tidal: "Test tidal",
                special_ventilator_fi02: "Test fi02",
                special_ventilator_assist: "Test assist",
                special_ventilator_PEEP: "Test PEEP",
                special_ventilator_SIMV: "Test SIMV",
                special_ventilator_pressure: "Test pressure",
                special_ventilator_PRVC: "Test PRVC",
                special_ventilator_observation: "Test observation",
                special_ventilator_intervention: "Test intervention",
                //
                special_ventilator_observation: "Test Observation",
                special_ventilator_intervention: "Test Intervention"
            }
        },
        pleurX:{
            pleurX: true,
            options: {
                special_pleurx_catheterinserted: "05/08/2023",
                //
                special_pleurx_frequency_daily: true,
                special_pleurx_frequency_every2: false,
                special_pleurx_frequency_twiceaweek: false,
                special_pleurx_frequency_other: "test other",
                //
                special_pleurx_amount_ml: "80",
                special_pleurx_amount_done: true,
                //
                special_pleurx_doneby_sn: true,
                special_pleurx_doneby_caregiver: true,
                special_pleurx_doneby_patient: false,
                //
                special_pleurx_observation: "Test observation",
                special_pleurx_intervention: "Test intervention"
            }
        },
        other: "Test Other"
    },
    respiratory_observation: "Test Observation",
    respiratory_intervention: "Test Intervention",
    cardiovascular: {
        wnl: false,
        options: {
            cardiovascular_rythm_regular: true,
            cardiovascular_rythm_irregular: false,
            cardiovascular_rythm_tachycardia: false,
            cardiovascular_rythm_bradycardia: false,
            cardiovascular_rythm_svt: false,

            cardiovascular_pulses_normal: true,
            cardiovascular_pulses_abnormal: false,

            cardiovascular_capillary_lt3: true,
            cardiovascular_capillary_gt3: false,

            cardiovascular_jvd_no: true,
            cardiovascular_jvd_yes: false,

            cardiovascular_peripheral_no: false,
            cardiovascular_peripheral_yes: true,

            cardiovascular_chestpain_no: false,
            cardiovascular_chestpain_yes: true,

            cardiovascular_cardiacdevice_no: true,
            cardiovascular_cardiacdevice_yes: false,

            cardiovascular_weightgain_no: false,
            cardiovascular_weightgain_yes: true,
            cardiovascular_weightgain_yes_describe: "Test Describe"
         }
    },
    abnormal_pulses: {
        abnormal_bounding_pedal_l: true,
        abnormal_bounding_popliteal_l: true,
        abnormal_bounding_femoral_l: true,
        abnormal_bounding_brachial_l: true,
        abnormal_bounding_radial_l: true,
        abnormal_bounding_pedal_r: true,
        abnormal_bounding_popliteal_r: true,
        abnormal_bounding_femoral_r: true,
        abnormal_bounding_brachial_r: true,
        abnormal_bounding_radial_r: true,

        abnormal_weak_pedal_l: true,
        abnormal_weak_popliteal_l: true,
        abnormal_weak_femoral_l: true,
        abnormal_weak_brachial_l: true,
        abnormal_weak_radial_l: true,
        abnormal_weak_pedal_r: true,
        abnormal_weak_popliteal_r: true,
        abnormal_weak_femoral_r: true,
        abnormal_weak_brachial_r: true,
        abnormal_weak_radial_r: true,

        abnormal_absent_pedal_l: true,
        abnormal_absent_popliteal_l: true,
        abnormal_absent_femoral_l: true,
        abnormal_absent_brachial_l: true,
        abnormal_absent_radial_l: true,
        abnormal_absent_pedal_r: true,
        abnormal_absent_popliteal_r: true,
        abnormal_absent_femoral_r: true,
        abnormal_absent_brachial_r: true,
        abnormal_absent_radial_r: true,

        abnormal_observation: "Test Observation",
        abnormal_intervention: "Test Intervention",
    },
    peripheral: {
        peripheral_left_1: true,
        peripheral_left_2: false,
        peripheral_left_3: false,
        peripheral_left_4: false,
        peripheral_left_none: false,

        peripheral_right_1: true,
        peripheral_right_2: false,
        peripheral_right_3: false,
        peripheral_right_4: false,
        peripheral_right_none: false,

        ankle_left_1: true,
        ankle_left_2: false,
        ankle_left_3: false,
        ankle_left_4: false,
        ankle_left_none: false,

        ankle_right_1: true,
        ankle_right_2: false,
        ankle_right_3: false,
        ankle_right_4: false,
        ankle_right_none: false,

        leg_left_1: true,
        leg_left_2: false,
        leg_left_3: false,
        leg_left_4: false,
        leg_left_none: false,

        leg_right_1: true,
        leg_right_2: false,
        leg_right_3: false,
        leg_right_4: false,
        leg_right_none: false,

        sacral_1: true,
        sacral_2: false,
        sacral_3: false,
        sacral_4: false,
        sacral_none: false,

        general_1: true,
        general_2: false,
        general_3: false,
        general_4: false,
        general_none: false,

        peripheral_observation: "Test Observation",
        peripheral_intervention: "Test Intervention",
    },
    chest: {
        chest_character_angina: true,
        chest_character_stabbing: true,
        chest_character_viselike: true,
        chest_character_sharp: true,
        chest_character_localized: true,
        chest_character_postural: true,
        chest_character_substernal: true,
        chest_character_dullache: true,
        chest_radiating_shoulder_l: true,
        chest_radiating_shoulder_r: true,
        chest_radiating_jaw_l: true,
        chest_radiating_jaw_r: true,
        chest_radiating_neck_l: true,
        chest_radiating_neck_r: true,
        chest_radiating_arm_l: true,
        chest_radiating_arm_r: true,
        chest_accompanied_dyspnea: true,
        chest_accompanied_diaphoresis: true,

        chest_freq_pain: "10",
        chest_duration_pain: "10",
        chest_observation: "Test Observation",
        chest_intervention: "Test Intervention",
    }

}