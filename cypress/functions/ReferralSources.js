import ReferralSourcesSelector from "../models/ReferralSourcesSelector";
const referral = new ReferralSourcesSelector();

function AddReferral(addReferralData = {}, issave = false){

    cy.get('.globallist__header_content > :nth-child(1) > .btn').click()

    cy.wait(2000)
    
    referral.fname.type(addReferralData.fname)
    referral.lname.type(addReferralData.lname)
    referral.company.type(addReferralData.company)
    referral.address.type(addReferralData.address)
    referral.city.type(addReferralData.city)

    referral.stateClick.click();
    referral.stateEnter.type(addReferralData.state)
    cy.get('.highlighted').click();

    referral.zip.type(addReferralData.zip)
    referral.phone.type(addReferralData.phone)
    referral.fax.type(addReferralData.fax)

    if(issave){
        referral.save.click();
    }
}

function EditReferral(referralData = {}, person = "", issave = false) {

    cy.wait(2000)

    cy.get('.searchbar__content > .ng-pristine').type(person)
    cy.wait(2000)
    cy.get('[style=""] > :nth-child(5) > .lv-actions > .btn').click()
    cy.wait(2000)

    //check what to edit
    for (const property in referralData) {
        if (referralData.hasOwnProperty(property)) {    
            referral[property].clear()
            referral[property].type(referralData[property])
            cy.log(`referralData has ${property} property`);
        }
    }

    if(issave){
        referral.save.click();
    }
}





export { AddReferral, EditReferral }