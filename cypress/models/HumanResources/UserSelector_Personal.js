export default class UserSelector{
   
 

    get firstname(){
        return cy.get(':nth-child(1) > :nth-child(2) > .fg-line > .global__txtbox')
    }
    get lastname(){
        return cy.get(':nth-child(2) > :nth-child(2) > .fg-line > .global__txtbox')
    }
    get middleinitial(){
        return cy.get(':nth-child(3) > :nth-child(2) > .fg-line > .global__txtbox')
    }
    get suffix(){
        return cy.get(':nth-child(4) > :nth-child(2) > .fg-line > .global__txtbox')
    }
    get maleCheck(){
        return cy.get('[opt-name="gender"]')
    }
    get femaleCheck(){
        return cy.get('[opt-name="gender2"]')
    }
    get birthdate(){
        return cy.get('#birthdate')
    }

    get raceClick(){
        return cy.get('.user_tabs > :nth-child(1) > tbody > :nth-child(7) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get raceEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(1) > tbody > tr:nth-child(7) > td:nth-child(2) > div > div > div > div > div > input[type=text]')
    }

    get martialClick(){
        return cy.get('.user_tabs > :nth-child(1) > tbody > :nth-child(8) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get martialEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(1) > tbody > tr:nth-child(8) > td:nth-child(2) > div > div > div > div > div > input[type=text]')
    }

    get disciplineClick(){
        return cy.get('.user_tabs > :nth-child(1) > tbody > :nth-child(9) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get disciplineEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > div > div > div > div > div > input[type=text]')
    }

    get titleClick(){
        return cy.get('.user_tabs > :nth-child(1) > tbody > tr.ng-scope > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get titleEnter(){
        return  cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(1) > tbody > tr.ng-scope > td:nth-child(2) > div > div > div > div > div > input[type=text]')
    }

    get hireClick(){
        return cy.get(':nth-child(11) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get hireEnter(){
        return  cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(1) > tbody > tr:nth-child(11) > td:nth-child(2) > div > div > div > div > div > input[type=text]')
    }

    get positionClick(){
        return cy.get(':nth-child(12) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get positionEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > div > div > div > div > div > input[type=text]')
    }

    get ssn(){
        return cy.get(':nth-child(13) > :nth-child(2) > .fg-line > .global__txtbox')
    }
    get clinicianid(){
        return cy.get(':nth-child(14) > :nth-child(2) > .fg-line > .global__txtbox')
    }
    get company(){
        return cy.get(':nth-child(15) > :nth-child(2) > .fg-line > .global__txtbox')
    }
    get address1(){
        return cy.get(':nth-child(16) > :nth-child(2) > .fg-line > .form-control')
    }
    get address2(){
        return cy.get(':nth-child(17) > :nth-child(2) > .fg-line > .form-control')
    }
    get city(){
        return cy.get(':nth-child(18) > :nth-child(2) > .fg-line > .global__txtbox')
    }
 
    get stateClick(){
        return cy.get(':nth-child(19) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get stateEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(1) > tbody > tr:nth-child(19) > td:nth-child(2) > div > div > div > div > div > input[type=text]')
    }

    get zip(){
        return cy.get(':nth-child(20) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get phone(){
        return cy.get(':nth-child(21) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get mobile(){
        return cy.get(':nth-child(22) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get fax(){
        return cy.get(':nth-child(23) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get email(){
        return cy.get(':nth-child(24) > .oasis__answer > .fg-line > .global__txtbox')
    }


    get hiredate(){
        return cy.get('#hireddate')
    }
    get terminationdate(){
        return cy.get('#terminationdate')
    }

    get languageprimaryClick(){
        return cy.get(':nth-child(3) > tbody > :nth-child(1) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get languageprimaryEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > div > div > div > div > div > input[type=text]')
    }

    get languagesecondaryClick(){
        return cy.get(':nth-child(1) > :nth-child(3) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get languagesecondaryEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(3) > div > div > div > div > div > input[type=text]')
    }


    get verbalprimaryClick(){
        return cy.get(':nth-child(3) > tbody > :nth-child(2) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get verbalprimaryEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > div > div > input[type=text]')
    }

    get verbalsecondaryClick(){
        return cy.get(':nth-child(2) > :nth-child(3) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get verbalsecondaryEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(3) > div > div > div > div > div > input[type=text]')
    }



    get readingprimaryClick(){
        return cy.get(':nth-child(3) > tbody > :nth-child(3) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get readingprimaryEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > div > div > div > input[type=text]')
    }

    get readingsecondaryClick(){
        return cy.get(':nth-child(3) > :nth-child(3) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get readingsecondaryEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(3) > div > div > div > div > div > input[type=text]')
    }

   
    get writingprimaryClick(){
        return cy.get(':nth-child(3) > tbody > :nth-child(4) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get writingprimaryEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > div > div > div > div > div > input[type=text]')
    }

    get writingsecondaryClick(){
        return cy.get(':nth-child(4) > :nth-child(3) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get writingsecondaryEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div:nth-child(2) > fieldset > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(3) > div > div > div > div > div > input[type=text]')
    }
}