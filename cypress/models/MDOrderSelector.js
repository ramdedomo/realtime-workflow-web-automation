export default class MDOrderSelector{
    get search(){
        return cy.get('.lvhs-input')
    }
    get filter(){
        return cy.get('.mhc__btn-affirmative')
    }

    get patientClick(){
        return cy.get('#patients_chosen > .chosen-single > span')
    }
    get patientEnter(){
        return cy.get('#patients_chosen > div > div > input[type=text]')
    }

    get clinicianClick(){
        return cy.get('.mdorder-clinician > .select > .chosen-container > .chosen-single > span')
    }
    get clinicianEnter(){
        return cy.get('#content > data > form > div > div.card > div > div.advanced__search_container.clearfix > div > div.m-t-5.in.collapse > div > div.mdorder-clinician.advanced__search_filter.col-sm-2.p-l-0.p-r-5 > div > div > div > div > input[type=text]')
    }

    get physicianClick(){
        return cy.get('.mdorder-physician > .select > .chosen-container > .chosen-single > span')
    }
    get physicianEnter(){
        return cy.get('#content > data > form > div > div.card > div > div.advanced__search_container.clearfix > div > div.m-t-5.in.collapse > div > div.mdorder-physician.advanced__search_filter.col-sm-2.p-l-0.p-r-5 > div > div > div > div > input[type=text]')
    }

    get typeClick(){
        return cy.get('.mdorder-type > .select > .chosen-container > .chosen-single > span')
    }
    get typeEnter(){
        return cy.get('#content > data > form > div > div.card > div > div.advanced__search_container.clearfix > div > div.m-t-5.in.collapse > div > div.mdorder-type.advanced__search_filter.col-sm-2.p-l-0.p-r-5 > div > div > div > div > input[type=text]')
    }

    get groupClick(){
        return cy.get('.mdorder-group.p-r-5 > .select > .chosen-container > .chosen-single > span')
    }
    get groupEnter(){
        return cy.get('#content > data > form > div > div.card > div > div.advanced__search_container.clearfix > div > div.m-t-5.in.collapse > div > div.mdorder-group.advanced__search_filter.col-sm-1.p-0.p-r-5 > div > div > div > div > input[type=text]')
    }

    get ageorderClick(){
        return cy.get(':nth-child(6) > .select > .chosen-container > .chosen-single > span')
    }
    get ageorderEnter(){
        return cy.get('#content > data > form > div > div.card > div > div.advanced__search_container.clearfix > div > div.m-t-5.in.collapse > div > div:nth-child(6) > div > div > div > div > input[type=text]')
    }

    get dateEnd(){
        return cy.get('#dateFrom')
    }

    get dateStart(){
        return cy.get('#dateTo')
    }

    get print(){
        return cy.get('.lv-actions > button.btn > .zmdi')
    }
    
}