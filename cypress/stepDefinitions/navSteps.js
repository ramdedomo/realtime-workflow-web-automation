/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import mainNavSelector from '../models/NavSelector'

    let mainNavSelected
    let subNavSelected

    //mainnav
    When('I click the main nav {string}', (mainNav) => {
        mainNavSelected = mainNavSelector.filter((nav) => { // find selected nav
            return nav.name == mainNav
        })
        cy.get(mainNavSelected[0].selector).click(); // click nav
    })

    Then('a selected main nav should be expand', () => {
        cy.get(mainNavSelected[0].selector).contains(mainNavSelected[0].name) // check if nav is selected
    })

    //subnav
    When('I click the sub nav {string}', (subNav) => {
        subNavSelected = mainNavSelected[0].subNav.filter((nav) => { // find selected sub nav
            return nav.name == subNav
        })
        cy.get(subNavSelected[0].selector).click(); // click sub nav
    })

    Then('a selected sub nav should be display', () => {
        cy.get(subNavSelected[0].selector).contains(subNavSelected[0].name)  // check if sub nav is selected
    })
     

