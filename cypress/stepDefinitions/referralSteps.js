/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { AddReferral, EditReferral } from '../functions/ReferralSources';

    const addReferralData = {
        fname: "Ellie",
        lname: "Williams",
        company: "EWILLIAMS",
        address: "315 High Sierra Dr",
        city: "Exeter",
        state: "CA",
        zip: "93221",
        phone: "98347534555",
        fax: "9848347236"
    }

    const editReferralData = {
        lname: "Williams",
        company: "EWILLIAMS UPDATED",
    }

    When('I add new referral sources', () => {
        AddReferral(addReferralData, false)  // add referral data, the second parameter is to save or not
    })

    Then('added new referral sources should appear', () => {
        
    })


    When('I edit referral sources {string}', (referral) => {
        EditReferral(editReferralData, referral, false) // edit referral data, the second parameter is what refferal should be edited and the last parameter is to save or not
    })

    Then('the edited referral sources should be updated', () => {
        
    })

