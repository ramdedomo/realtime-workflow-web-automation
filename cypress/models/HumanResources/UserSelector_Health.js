export default class UserSelector_Health{

    get physicalexamination(){
        return cy.get('div[ng-show="active==3"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(1) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get physicalexaminationEffective(){
        return cy.get('#hpefdate0')
    }
    get physicalexaminationExpire(){
        return cy.get('#hpexpdate0')
    }
    get physicalexaminationVerify(){
        return cy.get('#hpdatev0')
    }



    get PPD(){
        return cy.get('div[ng-show="active==3"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(2) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get PPDEffective(){
        return cy.get('#hpefdate1')
    }
    get PPDExpire(){
        return cy.get('#hpexpdate1')
    }
    get PPDVerify(){
        return cy.get('#hpdatev1')
    }



    get Chest(){
        return cy.get('div[ng-show="active==3"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(3) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get ChestEffective(){
        return cy.get('#hpefdate2')
    }
    get ChestExpire(){
        return cy.get('#hpexpdate2')
    }
    get ChestVerify(){
        return cy.get('#hpdatev2')
    }

    get tuberculosis(){
        return cy.get('div[ng-show="active==3"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(4) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get tuberculosisEffective(){
        return cy.get('#hpefdate3')
    }
    get tuberculosisExpire(){
        return cy.get('#hpexpdate3')
    }
    get tuberculosisVerify(){
        return cy.get('#hpdatev3')
    }

    get medicalhistory(){
        return cy.get('div[ng-show="active==3"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(5) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get medicalhistoryEffective(){
        return cy.get('#hpefdate4')
    }
    get medicalhistoryExpire(){
        return cy.get('#hpexpdate4')
    }
    get medicalhistoryVerify(){
        return cy.get('#hpdatev4')
    }   
    
   
    

    get healthquestionnaire(){
        return cy.get('div[ng-show="active==3"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(6) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get healthquestionnaireEffective(){
        return cy.get('#hpefdate5')
    }
    get healthquestionnaireExpire(){
        return cy.get('#hpexpdate5')
    }
    get healthquestionnaireVerify(){
        return cy.get('#hpdatev5')
    }

    get hepatitis(){
        return cy.get('div[ng-show="active==3"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(7) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get hepatitisEffective(){
        return cy.get('#hpefdate6')
    }
    get hepatitisExpire(){
        return cy.get('#hpexpdate6')
    }
    get hepatitisVerify(){
        return cy.get('#hpdatev6')
    }
}