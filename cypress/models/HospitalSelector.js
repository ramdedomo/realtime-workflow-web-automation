export default class HospitalSelector {
    
    get typeClick(){
        return cy.get('.oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }

    get typeEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(2) > td.oasis__answer > div > div.select.global__select.display-ib > div > div > div > input[type=text]');
    }

    get name(){
        return cy.get(':nth-child(3) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get address(){
        return cy.get(':nth-child(4) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get city(){
        return cy.get(':nth-child(5) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get stateClick(){
        return cy.get('#state_chosen > .chosen-single')
    }

    get stateEnter(){
        return cy.get('#state_chosen > .chosen-drop > .chosen-search > input')
    }

    get zip(){
        return cy.get('#zipCode')
    }

    get phone(){
        return cy.get(':nth-child(8) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get fax(){
        return cy.get(':nth-child(9) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get contact(){
        return cy.get(':nth-child(10) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get save(){
        return cy.get('.btn__success')
       
    }
}