/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { AddEmergency, EditEmergency } from '../functions/EmergencyServices';

    const addEmergencyData = {
        category: "Automation Emergency Category",
        name: "Automation Emergency",
        phone: "09839384932",
        otherphone: "09784739211"
    }
    
    const editEmergencyData = {
        name: "Automation Emergency Updated",
    }

    When('I add new emergency service', () => {
        AddEmergency(addEmergencyData, true)  // add emergency data, the second parameter is to save or not
    })
    
    Then('added new emergency service should appear', () => {

    })


    When('I edit emergency service {string}', (service) => {
        EditEmergency(editEmergencyData, service, false)
    })

    Then('the edited emergency service should be updated', () => {

    })
     


 