import HospitalSelector from "../models/HospitalSelector";
const hospital = new HospitalSelector();

function AddHospital(hospitalData = {}, issave = false){
    cy.get('.globallist__header_content > :nth-child(1) > .btn').click()

    //wait elements to load
    cy.wait(2000)

    hospital.typeClick.click()
    hospital.typeEnter.type(hospitalData.type)
    cy.get('.highlighted').click();

    hospital.name.type(hospitalData.name)
    hospital.address.type(hospitalData.address)
    hospital.city.type(hospitalData.city)

    hospital.stateClick.click();
    hospital.stateEnter.type(hospitalData.state);
    cy.get('.highlighted').click();

    hospital.zip.type(hospitalData.zip)
    hospital.phone.type(hospitalData.phone)
    hospital.fax.type(hospitalData.fax)
    hospital.contact.type(hospitalData.contact)

    if(issave){
        hospital.save.click()
    }
}

function EditHospital(hospitalData = {}, facility = "", issave = false){
    //wait elements to load
    cy.wait(2000)

    cy.get('#searchbar__wrapper > div > input').type(facility)
    cy.wait(2000)
    cy.get('.lv-actions > .btn').click()
    cy.wait(2000)

    //check what to edit
    for (const property in hospitalData) {
        if (hospitalData.hasOwnProperty(property)) {    
            hospital[property].clear()
            hospital[property].type(hospitalData[property])
            cy.log(`hospitalData has ${property} property`);
        }
    }

    if(issave){
        hospital.save.click()
    }

}

export { AddHospital, EditHospital }
