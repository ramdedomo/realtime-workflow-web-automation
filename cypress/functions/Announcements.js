import AnnouncementSelector from "../models/AnnouncementSelector";
const announce = new AnnouncementSelector()

function AddAnnouncement(announcementData, issave = false){
    cy.get('.horizontal__inline > .btn').click()
    cy.wait(1000)

    announce.start.type(announcementData.start)
    announce.end.type(announcementData.end)
    announce.description.type(announcementData.description)

    issave && announce.post.click()
}

function EditAnnouncementData(announcementData, issave = false){
     //check what to edit
    announce.edit.click()
    cy.wait(1000)

    for (const property in announcementData) {
        if (announcementData.hasOwnProperty(property)) {    
            announce[property].clear()
            announce[property].type(announcementData[property])
            cy.log(`announcementData has ${property} property`);
        }
    }


    issave && announce.post.click()
    //back
    cy.get('.horizontal__inline > .btn').click()
}

function DeleteAnnouncementData(){
    announce.delete.click()
    announce.deleteConfirm.click()
}

export { AddAnnouncement, EditAnnouncementData, DeleteAnnouncementData }