Feature: Login

    Scenario: Login user with correct email and password

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'System'
        Then a selected main nav should be expand

        When I click the sub nav 'Settings'
        Then a selected sub nav should be display 
 
        When I log out
        Then I should be logged out