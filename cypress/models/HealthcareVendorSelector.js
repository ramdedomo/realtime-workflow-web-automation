export default class HealthcareVendorSelector{
    get typeClick(){
        return cy.get(':nth-child(2) > .oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get typeEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(2) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }

    get name(){
        return cy.get(':nth-child(3) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get contactperson(){
        return cy.get(':nth-child(4) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get address(){
        return cy.get(':nth-child(5) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get city(){
        return cy.get(':nth-child(6) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get stateClick(){
        return cy.get('#state_chosen > .chosen-single')
    }
    get stateEnter(){
        return cy.get('#state_chosen > div > div > input[type=text]')
    }

    get zip(){
        return cy.get('#zipCode')
    }
    get phone(){
        return cy.get(':nth-child(9) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get fax(){
        return cy.get(':nth-child(10) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get email(){
        return cy.get(':nth-child(11) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get productsorservicesClick(){
        return cy.get(':nth-child(12) > .oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get productsorservicesEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(12) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }

    get BAACyes(){
        return cy.get('div.ng-isolate-scope > .m-b-5')
    }
    get BAACno(){
        return cy.get('div.ng-isolate-scope > :nth-child(2)')
    }
    get BAACna(){
        return cy.get('div.ng-isolate-scope > :nth-child(3)')
    }

    get BAAupload(){
        return cy.get(':nth-child(14) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get BAAlink(){
        return cy.get(':nth-child(15) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get save(){
        return  cy.get('.btn__success')
    }
}