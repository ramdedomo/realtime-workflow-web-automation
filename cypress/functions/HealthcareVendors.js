import HealthcareVendorSelector from "../models/HealthcareVendorSelector";
const healthcare = new HealthcareVendorSelector();

function AddHealthcare(healthcareData = {}, issave = false){
    cy.get('.globallist__header_content > :nth-child(1) > .btn').click()
    cy.wait(2000)

    healthcare.typeClick.click()
    healthcare.typeEnter.type(healthcareData.type)
    cy.get('.highlighted').click()

    healthcare.name.type(healthcareData.name)
    healthcare.contactperson.type(healthcareData.contactperson)
    healthcare.address.type(healthcareData.address)
    healthcare.city.type(healthcareData.city)

    healthcare.stateClick.click()
    healthcare.stateEnter.type(healthcareData.state)
    cy.get('.highlighted').click()

    healthcare.zip.type(healthcareData.zip)
    healthcare.phone.type(healthcareData.phone)
    healthcare.fax.type(healthcareData.fax)
    healthcare.email.type(healthcareData.email)

    healthcare.productsorservicesClick.click()
    healthcare.productsorservicesEnter.type(healthcareData.productsorservices)
    cy.get('.highlighted').click()

    const BAACcheckbox = healthcareData.baac
  
    for (const property in BAACcheckbox) {
        if (BAACcheckbox.hasOwnProperty(property)) {
            if(BAACcheckbox[property]){
                healthcare[property].click();
            }   
            cy.log(`BAACcheckbox has ${property} property`);
        }
    }

    if(issave){
        healthcare.save.click()
    }
}

function EditHealthcare(healthcareData = {}, vendor = "", issave = false){

    cy.get('.searchbar__content > .ng-valid').type(vendor)
    cy.wait(5000)
    cy.get('.lv-actions > .btn').click()
    cy.wait(2000)

      //check what to edit
    for (const property in healthcareData) {
        if (healthcareData.hasOwnProperty(property)) {    
            healthcare[property].clear()
            healthcare[property].type(healthcareData[property])
            cy.log(`healthcareData has ${property} property`);
        }
    }

    if(issave){
        healthcare.save.click()
    }
}

export { AddHealthcare, EditHealthcare }