export default class RespiratorySelector{
    get M1400(){
        return cy.get('#code')
    }

    get respiratorywnl(){
        return cy.get(':nth-child(2) > :nth-child(1) > :nth-child(1) > .oasis__primary > .m-l-15 > .checkbox > .ng-valid')
    }
    get respiratory_breath_yes(){
        return cy.get(':nth-child(2) > .subheadv3 > .cont-opt > :nth-child(1) > .ng-valid')
    }
    get respiratory_breath_no(){
        return cy.get(':nth-child(2) > .subheadv3 > .cont-opt > span.ng-isolate-scope > .radio > .ng-valid')
    }

    get respiratory_diminished_left(){
        return cy.get('.inner__table-wb > tbody > :nth-child(2) > :nth-child(2) > .checkbox > .ng-valid')
    }
    get respiratory_diminished_right(){
        return cy.get('.inner__table-wb > tbody > :nth-child(2) > :nth-child(3) > .checkbox > .ng-valid')
    }
    get respiratory_diminished_anterior(){
        return cy.get('tbody > :nth-child(2) > :nth-child(4) > .checkbox > .ng-valid')
    }
    get respiratory_diminished_posterior(){
        return cy.get('tbody > :nth-child(2) > :nth-child(5) > .checkbox > .ng-valid')
    }
    get respiratory_diminished_upper(){
        return cy.get(':nth-child(2) > :nth-child(6) > .checkbox > .ng-valid')
    }
    get respiratory_diminished_middle(){
        return cy.get(':nth-child(2) > :nth-child(7) > .checkbox > .ng-valid')
    }
    get respiratory_diminished_lower(){
        return cy.get(':nth-child(2) > :nth-child(8) > .checkbox > .ng-valid')
    }

  
    get respiratory_absent_left(){
        return cy.get('.inner__table-wb > tbody > :nth-child(3) > :nth-child(2) > .checkbox > .ng-pristine')
    }
    get respiratory_absent_right(){
        return cy.get(':nth-child(3) > :nth-child(3) > .checkbox > .ng-pristine')
    }
    get respiratory_absent_anterior(){
        return cy.get(':nth-child(3) > :nth-child(4) > .checkbox > .ng-pristine')
    }
    get respiratory_absent_posterior(){
        return cy.get(':nth-child(3) > :nth-child(5) > .checkbox > .ng-pristine')
    }
    get respiratory_absent_upper(){
        return cy.get(':nth-child(3) > :nth-child(6) > .checkbox > .ng-pristine')
    }
    get respiratory_absent_middle(){
        return cy.get(':nth-child(3) > :nth-child(7) > .checkbox > .ng-pristine')
    }
    get respiratory_absent_lower(){
        return cy.get(':nth-child(3) > :nth-child(8) > .checkbox > .ng-pristine')
    }


    get respiratory_rales_left(){
        return cy.get(':nth-child(4) > :nth-child(2) > .checkbox > .ng-pristine')
    }
    get respiratory_rales_right(){
        return cy.get(':nth-child(4) > :nth-child(3) > .checkbox > .ng-pristine')
    }
    get respiratory_rales_anterior(){
        return cy.get(':nth-child(4) > :nth-child(4) > .checkbox > .ng-pristine')
    }
    get respiratory_rales_posterior(){
        return cy.get(':nth-child(4) > :nth-child(5) > .checkbox > .ng-pristine')
    }
    get respiratory_rales_upper(){
        return cy.get(':nth-child(4) > :nth-child(6) > .checkbox > .ng-pristine')
    }
    get respiratory_rales_middle(){
        return cy.get(':nth-child(4) > :nth-child(7) > .checkbox > .ng-pristine')
    }
    get respiratory_rales_lower(){
        return cy.get(':nth-child(4) > :nth-child(8) > .checkbox > .ng-pristine')
    }


    get respiratory_rhonchi_left(){
        return cy.get('.inner__table-wb > tbody > :nth-child(5) > :nth-child(2) > .checkbox > .ng-pristine')
    }
    get respiratory_rhonchi_right(){
        return cy.get(':nth-child(5) > :nth-child(3) > .checkbox > .ng-pristine')
    }
    get respiratory_rhonchi_anterior(){
        return cy.get('.inner__table-wb > tbody > :nth-child(5) > :nth-child(4) > .checkbox > .ng-pristine')
    }
    get respiratory_rhonchi_posterior(){
        return cy.get(':nth-child(5) > :nth-child(5) > .checkbox > .ng-pristine')
    }
    get respiratory_rhonchi_upper(){
        return cy.get(':nth-child(5) > :nth-child(6) > .checkbox > .ng-pristine')
    }
    get respiratory_rhonchi_middle(){
        return cy.get(':nth-child(5) > :nth-child(7) > .checkbox > .ng-pristine')
    }
    get respiratory_rhonchi_lower(){
        return cy.get(':nth-child(5) > :nth-child(8) > .checkbox > .ng-pristine')
    }


    get respiratory_wheeze_left(){
        return cy.get(':nth-child(6) > :nth-child(2) > .checkbox > .ng-pristine')
    }
    get respiratory_wheeze_right(){
        return cy.get(':nth-child(6) > :nth-child(3) > .checkbox > .ng-pristine')
    }
    get respiratory_wheeze_anterior(){
        return cy.get(':nth-child(6) > :nth-child(4) > .checkbox > .ng-pristine')
    }
    get respiratory_wheeze_posterior(){
        return cy.get(':nth-child(6) > :nth-child(5) > .checkbox > .ng-pristine')
    }
    get respiratory_wheeze_upper(){
        return cy.get(':nth-child(6) > :nth-child(6) > .checkbox > .ng-pristine')
    }
    get respiratory_wheeze_middle(){
        return cy.get(':nth-child(6) > :nth-child(7) > .checkbox > .ng-pristine')
    }
    get respiratory_wheeze_lower(){
        return cy.get(':nth-child(6) > :nth-child(8) > .checkbox > .ng-pristine')
    }


    get respiratory_stridor_left(){
        return cy.get('.inner__table-wb > tbody > :nth-child(7) > :nth-child(2) > .checkbox > .ng-pristine')
    }
    get respiratory_stridor_right(){
        return cy.get(':nth-child(7) > :nth-child(3) > .checkbox > .ng-pristine')
    }
    get respiratory_stridor_anterior(){
        return cy.get(':nth-child(7) > :nth-child(4) > .checkbox > .ng-pristine')
    }
    get respiratory_stridor_posterior(){
        return cy.get(':nth-child(7) > :nth-child(5) > .checkbox > .ng-pristine')
    }
    get respiratory_stridor_upper(){
        return cy.get(':nth-child(7) > :nth-child(6) > .checkbox > .ng-pristine')
    }
    get respiratory_stridor_middle(){
        return cy.get(':nth-child(7) > :nth-child(7) > .checkbox > .ng-pristine')
    }
    get respiratory_stridor_lower(){
        return cy.get(':nth-child(7) > :nth-child(8) > .checkbox > .ng-pristine')
    }

    get respiratory_abnormal_no(){
        return cy.get(':nth-child(4) > .subheadv3 > .cont-opt > :nth-child(1) > .ng-valid')
    }
    get respiratory_abnormal_yes(){
        return cy.get(':nth-child(4) > .subheadv3 > .cont-opt > span.ng-isolate-scope > .radio > .ng-valid')
    }
    get respiratory_abnormal_rapid(){
        return cy.get('[style="width: 14.5%;"] > .m-0 > .checkbox > .ng-scope')
    }       
    get respiratory_abnormal_irregular(){
        return cy.get(':nth-child(2) > :nth-child(1) > .m-0 > .checkbox > .ng-scope')
    }
    get respiratory_abnormal_apnes(){
        return cy.get(':nth-child(3) > :nth-child(1) > .m-0 > .checkbox > .ng-scope')
    }
    get respiratory_abnormal_shallow(){
        return cy.get('[style="width: 14%;"] > .m-0 > .checkbox > .ng-scope')
    }
    get respiratory_abnormal_deep(){
        return cy.get(':nth-child(2) > :nth-child(2) > .m-0 > .checkbox > .ng-scope')
    }
    get respiratory_abnormal_gasping(){
        return cy.get(':nth-child(3) > :nth-child(2) > .m-0 > .checkbox > .ng-scope')
    }
    get respiratory_abnormal_noisy(){
        return cy.get('[style="width: 21.2%;"] > .m-0 > .checkbox > .ng-scope')
    }
    get respiratory_abnormal_cheyne(){
        return cy.get(':nth-child(2) > :nth-child(3) > .m-0 > .checkbox > .ng-scope')
    }
    get respiratory_abnormal_orthopnea(){
        return cy.get(':nth-child(3) > :nth-child(3) > .m-0 > .checkbox > .ng-scope')
    }
    get respiratory_abnormal_accesory(){
        return cy.get(':nth-child(4) > .m-0 > .checkbox > .ng-scope')
    }
    get respiratory_abnormal_other(){
        return cy.get(':nth-child(2) > :nth-child(4) > :nth-child(2) > .fg-line > .global__txtbox')
    }

    get respiratory_cough_yes(){
        return cy.get(':nth-child(1) > span.ng-isolate-scope > .radio > .ng-valid')
    }
    get respiratory_cough_no(){
        return cy.get('.cont-opt > :nth-child(1) > :nth-child(1) > .ng-valid')
    }

    get respiratory_character_productive(){
        return cy.get('[style="width: 14.3%;"] > div > .radio > .ng-pristine')
    }
    get respiratory_character_nonproductive(){
        return cy.get('[style="width: 21%;"] > div > .radio > .ng-pristine')
    }
    get respiratory_character_unable(){
        return cy.get('[style="width: 18%;"] > .checkbox > .ng-pristine')
    }

    get respiratory_sputum_white(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input')
    }
    get respiratory_sputum_yellow(){
        return cy.get('.inner__table-wb > tbody > :nth-child(2) > :nth-child(3) > .radio > .ng-pristine')
    }
    get respiratory_sputum_brown(){
        return cy.get(':nth-child(2) > :nth-child(4) > div > .radio > .ng-pristine')
    }
    get respiratory_sputum_green(){
        return cy.get('.inner__table-wb > tbody > :nth-child(2) > :nth-child(5) > .radio > .ng-pristine')
    }
    get respiratory_sputum_bloody(){
        return cy.get('.inner__table-wb > tbody > :nth-child(2) > :nth-child(6) > .radio > .ng-pristine')
    }

    get respiratory_sputumcharacter_thin(){
        return cy.get(':nth-child(3) > :nth-child(2) > div > .radio > .ng-pristine')
    }
    get respiratory_sputumcharacter_thick(){
        return cy.get(':nth-child(3) > :nth-child(3) > div > .radio > .ng-pristine')
    }
    get respiratory_sputumcharacter_tenacious(){
        return cy.get(':nth-child(3) > :nth-child(4) > div > .radio > .ng-pristine')
    }
    get respiratory_sputumcharacter_frothy(){
        return cy.get('[colspan="2"] > div > .radio > .ng-pristine')
    }

    get respiratory_sputumamount_small(){
        return cy.get(':nth-child(4) > :nth-child(2) > div > .radio > .ng-pristine')
    }
    get respiratory_sputumamount_moderate(){
        return cy.get(':nth-child(4) > :nth-child(3) > div > .radio > .ng-pristine')
    }
    get respiratory_csputumamount_large(){
        return cy.get('[colspan="3"] > div > .radio > .ng-pristine')
    }

    get respiratory_special_no(){
        return cy.get('.subheadv3 > .b-l-n > :nth-child(1) > .ng-pristine')
    }
    get respiratory_special_yes(){
        return cy.get('span.ng-isolate-scope > .checkbox > .ng-valid')
    }

    get respiratory_special_o2(){
        return cy.get('.p-l-5 > table > tbody > :nth-child(1) > :nth-child(1) > .radio > .ng-valid')
    }
    get special_o2_q1_yes(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-isolate-scope > td:nth-child(2) > label > input')
    }
    get special_o2_q1_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-isolate-scope > td:nth-child(3) > label > input')
    }
    get special_o2_q2_yes(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > label > input')
    }
    get special_o2_q2_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(3) > label > input')
    }
    get special_o2_q3_yes(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > label > input')
    }
    get special_o2_q3_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(3) > label > input')
    }
    get special_o2_q4_yes(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > label > input')
    }
    get special_o2_q4_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(3) > label > input')
    }
    get special_o2_q5_yes(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(6) > td.text-center.b-l-n.b-r-n.ng-isolate-scope > label > input')
    }
    get special_o2_q5_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(6) > td:nth-child(3) > label > input')
    }
    get special_o2_q6_yes(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-isolate-scope > td.text-center.b-l-n.b-r-n.ng-isolate-scope > label > input')
    }
    get special_o2_q6_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-isolate-scope > td:nth-child(6) > label > input')
    }
    get special_o2_q7_yes(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(5) > label > input')
    }
    get special_o2_q7_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(6) > label > input')
    }
    get special_o2_q8_yes(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(5) > label > input')
    }
    get special_o2_q8_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(6) > label > input')
    }
    get special_o2_q9_gas(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label.checkbox.checkbox-inline.m-l-5.ng-isolate-scope > input')
    }
    get special_o2_q9_fire(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(3) > input')
    }
    get special_o2_q9_lantern(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(4) > input')
    }
    get special_o2_q9_candles(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(5) > input')
    }
    get special_o2_q9_kerosene(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(6) > input')
    }
    get special_o2_q9_yes(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(5) > label > input')
    }
    get special_o2_q9_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(6) > label > input')
    }
    get special_o2_observation(){
        return cy.get(':nth-child(7) > [colspan="6"] > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get special_o2_intervention(){
        return cy.get(':nth-child(8) > [colspan="6"] > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }

    get respiratory_special_trancheostomy(){
        return cy.get('.p-l-5 > table > tbody > :nth-child(2) > :nth-child(1) > .radio > .ng-valid')
    }
    get special_trancheostomy_brand(){
        return cy.get('.b-t-n > span > .global__txtbox')
    }
    get special_trancheostomy_tubechange(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.ng-isolate-scope > div > input')
    }
    get special_trancheostomy_lastchanged(){
        return cy.get('#SOOCARDIO0089')
    }
    get special_trancheostomy_innercannula_none(){
        return cy.get('.fg-line > .checkbox > .ng-pristine')
    }
    get special_trancheostomy_innercannula_specs(){
        return cy.get('span.ng-isolate-scope > .global__txtbox')
    }


    get respiratory_special_BiPAP(){
        return cy.get('.p-l-5 > table > tbody > :nth-child(1) > :nth-child(2) > .radio > .ng-pristine')
    }    
    get special_BiPAP_brand(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td.ng-isolate-scope > div > input')
    }
    get special_BiPAP_working_yes(){
        return cy.get(':nth-child(3) > td.ng-isolate-scope > .m-r-10 > .ng-pristine')
    }
    get special_BiPAP_working_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td.ng-isolate-scope > label:nth-child(2) > input')
    }
    get special_BiPAP_complaint_yes(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(4) > td.ng-isolate-scope > label.radio.radio-inline.m-r-10.ng-isolate-scope > input')
    }
    get special_BiPAP_complaint_no(){
        return cy.get('td.ng-isolate-scope > .m-r-5 > .ng-pristine')
    }
    get special_BiPAP_observation(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td.ng-isolate-scope > input')
    }
    get special_BiPAP_intervention(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(6) > td.ng-isolate-scope > input')
    }


    get respiratory_special_suctioning(){
        return cy.get('.p-l-5 > table > tbody > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine')
    }
    get special_suctioning_oral(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > label:nth-child(1) > input')
    }
    get special_suctioning_trancheostomy(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > label:nth-child(2) > input')
    }
    get special_suctioning_nasopharyngeal(){
        return cy.get(':nth-child(2) > td.ng-isolate-scope > :nth-child(3) > .ng-valid')
    }
    get special_suctioning_asneeded(){
        return cy.get('[opt="data.SOOCARDIO0398"] > .ng-valid')
    }
    get special_suctioning_sunctionsched(){
        return cy.get(':nth-child(3) > td > [style="margin-left: 14px;"] > .ng-valid')
    }
    get special_suctioning_sunctionscheddate(){
        return cy.get('#SOOCARDIO0400')
    }
    get special_suctioning_q1_yes(){
        return cy.get('[style="margin-left: 14px;"] > .ng-pristine')
    }
    get special_suctioning_q1_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td > label:nth-child(2) > input')
    }
    get special_suctioning_q2_yes(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td > label:nth-child(2) > input')
    }
    get special_suctioning_q2_no(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td > label:nth-child(2) > input')
    }
    get special_suctioning_doneby_patient(){
        return cy.get('.b-t-n > :nth-child(2) > .ng-scope')
    }
    get special_suctioning_doneby_caregiver(){
        return cy.get('.b-t-n > :nth-child(3) > .ng-scope')
    }
    get special_suctioning_doneby_sn(){
        return cy.get('.b-t-n > :nth-child(4) > .ng-scope')
    }
    get special_suctioning_observation(){
        return cy.get(':nth-child(7) > .ng-isolate-scope > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get special_suctioning_intervention(){
        return cy.get(':nth-child(8) > .ng-isolate-scope > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }

    get respiratory_special_ventilator(){
        return cy.get('tbody > :nth-child(1) > :nth-child(3) > .radio > .ng-pristine')
    }
    get special_ventilator_brand(){
        return cy.get(':nth-child(2) > .b-b-n.b-l-n > .fg-line > .global__txtbox')
    }
    get special_ventilator_tidal(){
        return cy.get(':nth-child(3) > .b-b-n.b-l-n > .fg-line > .global__txtbox')
    }
    get special_ventilator_fi02(){
        return cy.get('tbody > :nth-child(4) > :nth-child(2) > .fg-line > .global__txtbox')
    }
    get special_ventilator_assist(){
        return cy.get(':nth-child(5) > :nth-child(2) > .fg-line > .global__txtbox')
    }
    get special_ventilator_PEEP(){
        return cy.get(':nth-child(2) > .b-t-n.b-l-n > .fg-line > .global__txtbox')
    }
    get special_ventilator_SIMV(){
        return cy.get(':nth-child(3) > .b-t-n.b-l-n > .fg-line > .global__txtbox')
    }
    get special_ventilator_pressure(){
        return cy.get(':nth-child(4) > :nth-child(4) > .fg-line > .global__txtbox')
    }
    get special_ventilator_PRVC(){
        return cy.get(':nth-child(5) > :nth-child(4) > .fg-line > .global__txtbox')
    }
    get special_ventilator_observation(){
        return cy.get(':nth-child(6) > [colspan="4"] > .global__txtbox')
    }
    get special_ventilator_intervention(){
        return cy.get(':nth-child(7) > [colspan="4"] > .global__txtbox')
    }
   

    get respiratory_special_pleurx(){
        return cy.get(':nth-child(2) > :nth-child(3) > div > .radio > .ng-pristine')
    }
    get special_pleurx_catheterinserted(){
        return cy.get('#SOOCARDIO0292')
    }
    get special_pleurx_frequency_daily(){
        return cy.get(':nth-child(1) > #SOOCARDIO0293')
    }
    get special_pleurx_frequency_every2(){
        return cy.get(':nth-child(2) > #SOOCARDIO0293')
    }
    get special_pleurx_frequency_twiceaweek(){
        return cy.get(':nth-child(3) > #SOOCARDIO0293')
    }
    get special_pleurx_frequency_other(){
        return cy.get(':nth-child(3) > .b-l-n > .global__txtbox')
    }
    get special_pleurx_amount_ml(){
        return cy.get(':nth-child(4) > .b-l-n > .global__txtbox')
    }
    get special_pleurx_amount_done(){
        return cy.get('.b-l-n > .display-ib > .p-l-18 > .ng-pristine')
    }
    get special_pleurx_doneby_sn(){
        return cy.get('.b-l-n > :nth-child(1) > .ng-scope')
    }
    get special_pleurx_doneby_caregiver(){
        return cy.get('.b-l-n > :nth-child(2) > .ng-scope')
    }
    get special_pleurx_doneby_patient(){
        return cy.get('.b-l-n > :nth-child(3) > .ng-scope')
    }
    get special_pleurx_observation(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td.ng-isolate-scope > input')
    }
    get special_pleurx_intervention(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(7) > td.ng-isolate-scope > input')
    }


    get respiratory_special_other(){
        return cy.get('.oasis-w-368-px > .fg-line > .global__txtbox')
    }
    get respiratory_observation(){
        return cy.get(':nth-child(1) > .p-r-5 > .global__txtbox')
    }
    get respiratory_intervention(){
        return cy.get(':nth-child(2) > .p-r-5 > .global__txtbox')
    }

    get cardiovascularwnl(){
        return cy.get(':nth-child(10) > :nth-child(1) > :nth-child(1) > .oasis__primary > .m-l-15 > .checkbox > .ng-untouched')
    }
    get cardiovascular_rythm_regular(){
        return cy.get('[style="padding-top: 3px;"] > .radio > .ng-pristine')
    }
    get cardiovascular_rythm_irregular(){
        return cy.get('.display-ib.ng-isolate-scope > .radio > .ng-pristine')
    }
    get cardiovascular_rythm_tachycardia(){
        return cy.get('[is-string="true"] > .checkbox > .ng-scope')
    }
    get cardiovascular_rythm_bradycardia(){
        return cy.get(':nth-child(4) > .checkbox > .ng-scope')
    }
    get cardiovascular_rythm_svt(){
        return cy.get(':nth-child(5) > .checkbox > .ng-scope')
    }

    get cardiovascular_pulses_normal(){
        return cy.get(':nth-child(3) > [colspan="12"] > .inner__table-nb > tbody > tr > .p-r-5 > .radio > .ng-pristine')
    }
    get cardiovascular_pulses_abnormal(){
        return cy.get(':nth-child(3) > [colspan="12"] > .inner__table-nb > tbody > tr > td.ng-isolate-scope > .radio > .ng-pristine')
    }

    get cardiovascular_capillary_lt3(){
        return cy.get(':nth-child(4) > [colspan="12"] > .inner__table-nb > tbody > tr > .p-r-5 > .radio > .ng-pristine')
    }
    get cardiovascular_capillary_gt3(){
        return cy.get(':nth-child(4) > [colspan="12"] > .inner__table-nb > tbody > tr > td.ng-isolate-scope > .radio > .ng-pristine')
    }

    get cardiovascular_jvd_no(){
        return cy.get(':nth-child(5) > [colspan="12"] > .inner__table-nb > tbody > tr > .p-r-5 > .radio > .ng-pristine')
    }
    get cardiovascular_jvd_yes(){
        return cy.get(':nth-child(5) > [colspan="12"] > .inner__table-nb > tbody > tr > td.ng-isolate-scope > .radio > .ng-pristine')
    }

    get cardiovascular_peripheral_no(){
        return cy.get(':nth-child(6) > [colspan="12"] > .inner__table-nb > tbody > tr > .p-r-5 > .checkbox > .ng-pristine')
    }
    get cardiovascular_peripheral_yes(){
        return cy.get(':nth-child(6) > [colspan="12"] > .inner__table-nb > tbody > tr > td.ng-isolate-scope > .checkbox > .ng-pristine')
    }

    
    get cardiovascular_chestpain_no(){
        return cy.get(':nth-child(7) > [colspan="12"] > .inner__table-nb > tbody > tr > .p-r-5 > .checkbox > .ng-pristine')
    }
    get cardiovascular_chestpain_yes(){
        return cy.get(':nth-child(7) > [colspan="12"] > .inner__table-nb > tbody > tr > td.ng-isolate-scope > .checkbox > .ng-pristine')
    }

    get cardiovascular_cardiacdevice_no(){
        return cy.get(':nth-child(8) > [colspan="12"] > .inner__table-nb > tbody > tr > td.p-r-5 > .checkbox > .ng-pristine')
    }
    get cardiovascular_cardiacdevice_yes(){
        return cy.get('[style="width: 7%;"] > .checkbox > .ng-valid')
    }
    get cardiovascular_cardiacdeviceClick(){
        return cy.get('.select__no__search > .select > .chosen-container > .chosen-single')
    }
    get cardiovascular_cardiacdeviceEnter(){
        
    }

    get cardiovascular_weightgain_no(){
        return cy.get('[colspan="12"] > :nth-child(2) > .ng-valid')
    }
    get cardiovascular_weightgain_yes(){
        return cy.get('.m-l-20 > .ng-valid')
    }
    get cardiovascular_weightgain_yes_describe(){
        return cy.get('[colspan="12"] > .global__txtbox')
    }

    get abnormal_bounding_pedal_l(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(3) > :nth-child(2) > .radio > .ng-pristine')
    }
    get abnormal_bounding_popliteal_l(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(3) > :nth-child(4) > .radio > .ng-valid')
    }
    get abnormal_bounding_femoral_l(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(3) > :nth-child(6) > .radio > .ng-valid')
    }
    get abnormal_bounding_brachial_l(){
        return cy.get(':nth-child(3) > :nth-child(8) > .radio > .ng-pristine')
    }
    get abnormal_bounding_radial_l(){
        return cy.get(':nth-child(3) > :nth-child(10) > .radio > .ng-pristine')
    }
    get abnormal_bounding_pedal_r(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(3) > :nth-child(3) > .radio > .ng-pristine')
    }
    get abnormal_bounding_popliteal_r(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(3) > :nth-child(5) > .radio > .ng-valid')
    }
    get abnormal_bounding_femoral_r(){
        return cy.get(':nth-child(3) > :nth-child(7) > .radio > .ng-pristine')
    }
    get abnormal_bounding_brachial_r(){
        return cy.get(':nth-child(3) > :nth-child(9) > .radio > .ng-pristine')
    }
    get abnormal_bounding_radial_r(){
        return cy.get(':nth-child(3) > :nth-child(11) > .radio > .ng-pristine')
    }

    get abnormal_weak_pedal_l(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(4) > :nth-child(2) > .radio > .ng-pristine')
    }
    get abnormal_weak_popliteal_l(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(4) > :nth-child(4) > .radio > .ng-pristine')
    }
    get abnormal_weak_femoral_l(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(4) > :nth-child(6) > .radio > .ng-pristine')
    }
    get abnormal_weak_brachial_l(){
        return cy.get(':nth-child(4) > :nth-child(8) > .radio > .ng-pristine')
    }
    get abnormal_weak_radial_l(){
        return cy.get(':nth-child(4) > :nth-child(10) > .radio > .ng-pristine')
    }
    get abnormal_weak_pedal_r(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(4) > :nth-child(3) > .radio > .ng-pristine')
    }
    get abnormal_weak_popliteal_r(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(4) > :nth-child(5) > .radio > .ng-pristine')
    }
    get abnormal_weak_femoral_r(){
        return cy.get(':nth-child(4) > :nth-child(7) > .radio > .ng-pristine')
    }
    get abnormal_weak_brachial_r(){
        return cy.get(':nth-child(4) > :nth-child(9) > .radio > .ng-pristine')
    }
    get abnormal_weak_radial_r(){
        return cy.get(':nth-child(4) > :nth-child(11) > .radio > .ng-pristine')
    }

    get abnormal_absent_pedal_l(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(5) > :nth-child(2) > .radio > .ng-pristine')
    }
    get abnormal_absent_popliteal_l(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(5) > :nth-child(4) > .radio > .ng-pristine')
    }
    get abnormal_absent_femoral_l(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(5) > :nth-child(6) > .radio > .ng-pristine')
    }
    get abnormal_absent_brachial_l(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(5) > :nth-child(6) > .radio > .ng-pristine')
    }
    get abnormal_absent_radial_l(){
        return cy.get(':nth-child(5) > :nth-child(10) > .radio > .ng-pristine')
    }
    get abnormal_absent_pedal_r(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(5) > :nth-child(3) > .radio > .ng-pristine')
    }
    get abnormal_absent_popliteal_r(){
        return cy.get(':nth-child(11) > :nth-child(1) > :nth-child(5) > :nth-child(5) > .radio > .ng-pristine')
    }
    get abnormal_absent_femoral_r(){
        return cy.get(':nth-child(5) > :nth-child(7) > .radio > .ng-pristine')
    }
    get abnormal_absent_brachial_r(){
        return cy.get(':nth-child(5) > :nth-child(9) > .radio > .ng-pristine')
    }
    get abnormal_absent_radial_r(){
        return cy.get(':nth-child(5) > :nth-child(11) > .radio > .ng-pristine')
    }

    get abnormal_observation(){
        return cy.get(':nth-child(6) > .oasis__answer > .inner__table-wb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get abnormal_intervention(){
        return cy.get(':nth-child(7) > .oasis__answer > .inner__table-wb > tbody > tr > :nth-child(2) > .global__txtbox')
    }

    get peripheral_left_1(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[3]/td[2]/label/input')
    }
    get peripheral_left_2(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[3]/td[3]/label/input')
    }
    get peripheral_left_3(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[3]/td[4]/label/input')
    }
    get peripheral_left_4(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[3]/td[5]/label/input')
    }
    get peripheral_left_none(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[3]/td[6]/label/input')
    }


    get peripheral_right_1(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[4]/td[2]/label/input')
    }
    get peripheral_right_2(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[4]/td[3]/label/input')
    }
    get peripheral_right_3(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[4]/td[4]/label/input')
    }
    get peripheral_right_4(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[4]/td[5]/label/input')
    }
    get peripheral_right_none(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[4]/td[6]/label/input')
    }

    get ankle_left_1(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[5]/td[2]/label/input')
    }
    get ankle_left_2(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[5]/td[3]/label/input')
    }
    get ankle_left_3(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[5]/td[4]/label/input')
    }
    get ankle_left_4(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[5]/td[5]/label/input')
    }
    get ankle_left_none(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[5]/td[6]/label/input')
    }


    get ankle_right_1(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[6]/td[2]/label/input')
    }
    get ankle_right_2(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[6]/td[3]/label/input')
    }
    get ankle_right_3(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[6]/td[4]/label/input')
    }
    get ankle_right_4(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[6]/td[5]/label/input')
    }
    get ankle_right_none(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[6]/td[6]/label/input')
    }


    get leg_left_1(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[7]/td[2]/label/input')
    }
    get leg_left_2(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[7]/td[3]/label/input')
    }
    get leg_left_3(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[7]/td[4]/label/input')
    }
    get leg_left_4(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[7]/td[5]/label/input')
    }
    get leg_left_none(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[7]/td[6]/label/input')
    }


    get leg_right_1(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[8]/td[2]/label/input')
    }
    get leg_right_2(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[8]/td[3]/label/input')
    }
    get leg_right_3(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[8]/td[4]/label/input')
    }
    get leg_right_4(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[8]/td[5]/label/input')
    }
    get leg_right_none(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[8]/td[6]/label/input')
    }


    get sacral_1(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[9]/td[2]/label/input')
    }
    get sacral_2(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[9]/td[3]/label/input')
    }
    get sacral_3(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[9]/td[4]/label/input')
    }
    get sacral_4(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[9]/td[5]/label/input')
    }
    get sacral_none(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[9]/td[6]/label/input')
    }


    get general_1(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[10]/td[2]/label/input')
    }
    get general_2(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[10]/td[3]/label/input')
    }
    get general_3(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[10]/td[4]/label/input')
    }
    get general_4(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[10]/td[5]/label/input')
    }
    get general_none(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[10]/td[6]/label/input')
    }

    get peripheral_observation(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[11]/td[2]/input')
    }
    get peripheral_intervention(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[11]/tbody/tr[12]/td[2]/input')
    }


    get chest_character_angina(){
        return cy.get('[style="width: 14.3%;"] > .checkbox > .ng-scope')
    }
    get chest_character_stabbing(){
        return cy.get('[colspan="2"] > .inner__table-nb > tbody > :nth-child(2) > :nth-child(1) > .checkbox > .ng-scope')
    }
    get chest_character_viselike(){
        return cy.get('[colspan="2"] > .inner__table-nb > tbody > :nth-child(3) > :nth-child(1) > .checkbox > .ng-scope')
    }
    get chest_character_sharp(){
        return cy.get('[style="width: 15%;"] > .checkbox > .ng-scope')
    }
    get chest_character_localized(){
        return cy.get('[colspan="2"] > .inner__table-nb > tbody > :nth-child(2) > :nth-child(2) > .checkbox > .ng-scope')
    }
    get chest_character_postural(){
        return cy.get(':nth-child(3) > .p-r-5 > .checkbox > .ng-scope')
    }
    get chest_character_substernal(){
        return cy.get(':nth-child(1) > :nth-child(3) > .checkbox > .ng-scope')
    }
    get chest_character_dullache(){
        return cy.get('tbody > :nth-child(2) > :nth-child(3) > .checkbox > .ng-scope')
    }

    get chest_radiating_shoulder_l(){
        return cy.get(':nth-child(3) > [colspan="2"] > .inner__table-nb > tbody > :nth-child(1) > .p-r-5 > :nth-child(1) > .ng-pristine')
    }
    get chest_radiating_shoulder_r(){
        return cy.get(':nth-child(1) > .p-r-5 > :nth-child(2) > .ng-pristine')
    }

    
    get chest_radiating_jaw_l(){
        return cy.get(':nth-child(3) > [colspan="2"] > .inner__table-nb > tbody > :nth-child(2) > .p-r-5 > :nth-child(1) > .ng-pristine')
    }
    get chest_radiating_jaw_r(){
        return cy.get(':nth-child(2) > .p-r-5 > :nth-child(2) > .ng-pristine')
    }

    
    get chest_radiating_neck_l(){
        return cy.get(':nth-child(3) > [colspan="2"] > .inner__table-nb > tbody > :nth-child(3) > .p-r-5 > :nth-child(1) > .ng-pristine')
    }
    get chest_radiating_neck_r(){
        return cy.get(':nth-child(3) > .p-r-5 > :nth-child(2) > .ng-pristine')
    }

    
    get chest_radiating_arm_l(){
        return cy.get('td.ng-isolate-scope > [style=""] > .ng-pristine')
    }
    get chest_radiating_arm_r(){
        return cy.get('.inner__table-nb > tbody > :nth-child(4) > td.ng-isolate-scope > :nth-child(2) > .ng-pristine')
    }

    get chest_accompanied_dyspnea(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[12]/tbody/tr[4]/td[2]/table/tbody/tr/td[1]/label/input')
    }
    get chest_accompanied_diaphoresis(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[12]/tbody/tr[4]/td[2]/table/tbody/tr/td[2]/label/input')
    }


    get chest_freq_pain(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[12]/tbody/tr[5]/td[2]/input')
    }
    get chest_duration_pain(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[12]/tbody/tr[6]/td[2]/input')
    }

    get chest_observation(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[12]/tbody/tr[7]/td[2]/input')
    }
    get chest_intervention(){
        return cy.xpath('//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[12]/tbody/tr[8]/td[2]/input')
    }


    
}
