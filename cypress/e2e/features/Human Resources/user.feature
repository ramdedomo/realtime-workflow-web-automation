Feature: User List

    Scenario: Finding a User and Edit Information

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Human Resources'
        Then a selected main nav should be expand

        When I click the sub nav 'User List'
        Then a selected sub nav should be display 

        When I add new user
        Then added new user should appear