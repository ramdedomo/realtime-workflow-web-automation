const touch = vitalData.touch
if(touch.wnl){
    vital.touch_wnl.click()
}else{
    const touch_option = touch.option
    for (const property in touch_option) {
        if (touch_option.hasOwnProperty(property)) {
            if(typeof touch_option[property] === 'string'){
                vital[property].type(touch_option[property]);
            }else{
                if(touch_option[property]){
                    vital[property].click();
                }
            }
        }   
    }
}