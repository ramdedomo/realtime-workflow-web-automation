/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { AddHealthcare, EditHealthcare } from '../functions/HealthcareVendors';

    const addHealthcareData = {
        type: "Health",
        name: "Healthcare Automation",
        contactperson: "Test Contact Person",
        address: "580 Salem St #26",
        city: "Wakefield",
        state: "CA",
        zip: "01880",
        phone: "09273829182",
        fax: "3434-454-567",
        email: "testemail@email.com",
        productsorservices: "First",
        baac: {
            BAACyes: true,
            BAACno: true,
            BAACna: true,
        },
        BAAupload: "",
        BAAlink: ""
    }

     const editHealthcareData = {
        contactperson: "Test Contact Person",
    }
    
    When('I add new healthcare vendor', () => {
        AddHealthcare(addHealthcareData, false)  // add healthcare data, the second parameter is to save or not
    })

    Then('added new healthcare vendor should appear', () => {
      
    })

    When('I edit healthcare vendor {string}', (vendor) => {
        EditHealthcare(editHealthcareData, vendor, true)  // edit healthcare data, the second parameter is what vendor should be edited and the last parameter is to save or not
    })

    Then('the edited healthcare vendor should be updated', () => {
      
    })