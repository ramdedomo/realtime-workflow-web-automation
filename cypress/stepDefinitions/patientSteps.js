/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { Admission, EditPatient, Task } from '../functions/Patient';
import PatientData from '../functions/data/addPatientData';

    const editPatientData = {
        middleinitial: "C"
    }

    When('I add a new Patient', () => {
        //data, admit?, save?
        Admission(PatientData, true, true)
    })

    Then('a new patient should be added', () => {

    })


    When('I edit patient {string} info', (patient) => {
        EditPatient(editPatientData, patient, false)
    })

    Then('the patient info should be updated', () => {

    })


    When('I edit patient {string} documents', (patient) => {
        Task(patient, false)
    })

    Then('the patient documents should be updated', () => {

    })


     

    
         