export default class AdministrativeSelector{
    get physicianClick(){
        return cy.get('.p-lr-0 > .select > .chosen-container > .chosen-single > span')
    }
    get physicianEnter(){
        return 
    }
    get patientID(){
        return cy.get('.oasis__input_container > #M0020_PAT_ID')
    }
    get SOCdate(){
        return cy.get('#M0030_START_CARE_DT1')
    }
    get firstname(){
        return cy.get('.oasis-border-b > #M0040_PAT_FNAME')
    }
    get lastname(){
        return cy.get('#M0040_PAT_LNAME')
    }
    get middle(){
        return cy.get('#M0040_PAT_MI')
    }
    get suffix(){
        return cy.get('#M0040_PAT_SUFFIX')
    }
    get address(){
        return cy.get('#SOOPAT0004')
    }
    get city(){
        return cy.get('#SOOPAT0005')
    }
    get phone(){
        return cy.get('#SOOPAT0001')
    }
    get emergencyname(){
        return cy.get('.oasis-w-256-px')
    }
    get emergencyrelationship(){
        return cy.get(':nth-child(3) > .oasis-border-b > .oasis-w-130-px')
    }
    get emergencyphone(){
        return cy.get('#ECPHONE1')
    }

    get primaryphysicianClick(){
        return cy.get('#pcpSelect_chosen > .chosen-single > span')
    }
    get primaryphysicianEnter(){
        return cy.get('')
    }

    get stateClick(){
        return cy.get('.row > .select > .chosen-container > .chosen-single > span')
    }
    get stateEnter(){
        return cy.get('')
    }
    get zip(){
        return cy.get('.col-xs-12 > #M0060_PAT_ZIP')
    }
    get ssn(){
        return cy.get('.oasis__input_container > #M0064_SSN')
    }
    get ukssn(){
        return cy.get('[mos-name="M0064"] > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .m-0 > .checkbox > .ng-pristine')
    }
    get medicareno(){
        return cy.get('.oasis__input_container > #M0063_MEDICARE_NUM')
    }
    get ukmedicareno(){
        return cy.get('[mos-name="M0063"] > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .m-0 > .checkbox > .ng-pristine')
    }
    get medicadeno(){
        return cy.get('.oasis__input_container > #M0065_MEDICAID_NUM')
    }
    get ukmedicadeno(){
        return cy.get('[mos-name="M0065"] > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .m-0 > .checkbox > .ng-pristine')
    }
    get male(){
        return cy.get('#M0069_PAT_GENDER > td:nth-child(3) > div > label:nth-child(1) > input')
    }
    get female(){
        return cy.get('#M0069_PAT_GENDER > td:nth-child(3) > div > label:nth-child(2) > input')
    }
    get birthdate(){
        return cy.get('#M0066_PAT_BIRTH_DT1')
    }

    get A1005_A(){
        return cy.get('#A1005A > .ng-pristine')
    }
    get A1005_B(){
        return cy.get('#A1005B > .ng-pristine')
    }
    get A1005_C(){
        return cy.get('#A1005C > .ng-pristine')
    }
    get A1005_D(){
        return cy.get('#A1005D > .ng-pristine')
    }
    get A1005_E(){
        return cy.get('#A1005E > .ng-pristine')
    }
    get A1005_X(){
        return cy.get('#A1005X > .ng-pristine')
    }
    get A1005_Y(){
        return cy.get('#A1005Y > .ng-pristine')
    }


    get A1010_A(){
        return cy.get('#A1010A > .ng-pristine')
    }
    get A1010_B(){
        return cy.get('#A1010B > .ng-pristine')
    }
    get A1010_C(){
        return cy.get('#A1010C > .ng-pristine')
    }
    get A1010_D(){
        return cy.get('#A1010D > .ng-pristine')
    }
    get A1010_E(){
        return cy.get('#A1010E > .ng-pristine')
    }
    get A1010_F(){
        return cy.get('#A1010F > .ng-pristine')
    }
    get A1010_G(){
        return cy.get('#A1010G > .ng-pristine')
    }
    get A1010_H(){
        return cy.get('#A1010H > .ng-pristine')
    }
    get A1010_I(){
        return cy.get('#A1010I > .ng-pristine')
    }
    get A1010_J(){
        return cy.get('#A1010J > .ng-pristine')
    }
    get A1010_K(){
        return cy.get('#A1010K > .ng-pristine')
    }
    get A1010_L(){
        return cy.get('#A1010L > .ng-pristine')
    }
    get A1010_M(){
        return cy.get('#A1010M > .ng-pristine')
    }
    get A1010_N(){
        return cy.get('#A1010N > .ng-pristine')
    }
    get A1010_X(){
        return cy.get('#A1010X > .ng-pristine')
    }
    get A1010_Y(){
        return cy.get('#A1010Y > .ng-pristine')
    }
    get A1010_Z(){
        return cy.get('#A1010Z > .ng-pristine')
    }

    get M0150_0(){
        return cy.get('#M0150_CPAY_NONE > .ng-pristine')
    }
    get M0150_1(){
        return cy.get('#M0150_CPAY_MCARE_FFS > .ng-valid')
    }
    get M0150_2(){
        return cy.get('#M0150_CPAY_MCARE_HMO > .ng-pristine')
    }
    get M0150_3(){
        return cy.get('#M0150_CPAY_MCAID_FFS > .ng-valid')
    }
    get M0150_4(){
        return cy.get('#M0150_CPAY_MCAID_HMO > .ng-pristine')
    }
    get M0150_5(){
        return cy.get('#M0150_CPAY_WRKCOMP > .ng-pristine')
    }
    get M0150_6(){
        return cy.get('#M0150_CPAY_TITLEPGMS > .ng-pristine')
    }
    get M0150_7(){
        return cy.get('#M0150_CPAY_OTH_GOVT > .ng-pristine')
    }
    get M0150_8(){
        return cy.get('#M0150_CPAY_PRIV_INS > .ng-pristine')
    }
    get M0150_9(){
        return cy.get('#M0150_CPAY_PRIV_HMO > .ng-pristine')
    }
    get M0150_10(){
        return cy.get('#M0150_CPAY_SELFPAY > .ng-pristine')
    }
    get M0150_11(){
        return cy.get('#M0150_CPAY_OTHER > .ng-pristine')
    }
    get M0150_11_specify(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(4) > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td.v-top > div > div:nth-child(3) > div.m-l-160 > input')
    }
    get M0150_uk(){
        return cy.get('#M0150_CPAY_UK > .ng-pristine')
    }
    get M0150_na(){
        return cy.get('#SOOM0150004 > .ng-pristine')
    }

    get A1110(){
        return cy.get('div.m-b-5 > .global__txtbox')
    }
    get A1110_no(){
        return cy.get('#A1110B1')
    }
    get A1110_yes(){
        return cy.get('#A1110B2')
    }
    get A1110_unable(){
        return cy.get('#A1110B3')
    }

    get M0090(){
        return cy.get('#M0090_INFO_COMPLETED_DT')
    }

    get M0102(){
        return cy.get('#M0102_PHYSN_ORDRD_SOCROC_DT')
    }
    get M0102_na(){
        return cy.get('#M0102_PHYSN_ORDRD_SOCROC_DT')
    }

    get M0104(){
        return cy.get('#M0104_PHYSN_RFRL_DT')
    }

    get M0110_early(){
        return cy.get('.oasis-w-30 > .ng-isolate-scope > :nth-child(1) > .radio > .ng-valid')
    }
    get M0110_later(){
        return cy.get('.oasis-w-30 > .ng-isolate-scope > :nth-child(2) > .radio > .ng-valid')
    }
    get M0110_unknown(){
        return cy.get('.ng-isolate-scope > :nth-child(3) > .radio > .ng-valid')
    }
    get M0110_na(){
        return cy.get('.ng-isolate-scope > :nth-child(4) > .radio > .ng-pristine')
    }

    get A1250_A(){
        return cy.get('#A1250A > .ng-valid')
    }
    get A1250_B(){
        return cy.get('#A1250B > .ng-valid')
    }
    get A1250_C(){
        return cy.get('#A1250C > .ng-valid')
    }
    get A1250_X(){
        return cy.get('#A1250X > .ng-valid')
    }
    get A1250_Y(){
        return cy.get('#A1250Y > .ng-valid')
    }

    get M1000_1(){
        return cy.get('#M1000_DC_LTC_14_DA > .ng-valid')
    }
    get M1000_2(){
        return cy.get('#M1000_DC_SNF_14_DA > .ng-pristine')
    }
    get M1000_3(){
        return cy.get('#M1000_DC_IPPS_14_DA > .ng-pristine')
    }
    get M1000_4(){
        return cy.get('#M1000_DC_LTCH_14_DA > .ng-pristine')
    }
    get M1000_5(){
        return cy.get('#M1000_DC_IRF_14_DA > .ng-pristine')
    }
    get M1000_6(){
        return cy.get('#M1000_DC_PSYCH_14_DA > .ng-pristine')
    }
    get M1000_7(){
        return cy.get('#M1000_DC_OTH_14_DA > .ng-pristine')
    }
    get M1000_7_specify(){
        return cy.get(':nth-child(9) > :nth-child(1) > :nth-child(2) > .oasis__answer > .inner__table-nb > tbody > tr > .v-top > :nth-child(1) > .display-block > .m-l-160 > .global__txtbox')
    }
    get M1000_NA(){
        return cy.get('#M1000_DC_NONE_14_DA > .ng-pristine')
    }


    get M1005(){
        return cy.get('#M1005_INP_DISCHARGE_DT')
    }
    get M1005_uk(){
        return cy.get('#M1005_INP_DSCHG_UNKNOWN > .ng-pristine')
    }

 







    

}

