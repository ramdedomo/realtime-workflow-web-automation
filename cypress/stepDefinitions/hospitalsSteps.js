/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { AddHospital, EditHospital } from '../functions/Hospital';

    const addHospitalData = {
        type: "In",
        name: "test hospital 123",
        address: "70785 Ironwood Dr",
        city: "Rancho Mirage",
        state: "CA",
        zip: "92270",
        phone: "09347329428",
        fax: "98347534555",
        contact: "Test Contact Person"
    }

    const editHospitalData = {
        contact: "Test Contact Person Updated"
    }

    When('I add new hospital', () => {
        AddHospital(addHospitalData, false)   // add hospital data, the second parameter is to save or not
    })
    
    Then('added new hospital should appear', () => {

    })

    When('I edit the hospital named {string}', (facility) => {
        EditHospital(editHospitalData, facility, false)   // edit healthcare data, the second parameter is to save or not
    })

    Then('the edited hospital should be updated', () => {
        
    })
   


 