export default class UserSelector_Professional{

        get professionallicense(){
            return cy.get('div[ng-show="active==2"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(1) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
        }
        get professionallicenseID(){
            return cy.get(':nth-child(1) > :nth-child(3) > .fg-line > .global__txtbox')
        }
        get professionalEffective(){
            return cy.get('#efdateu0')
        }
        get professionalExpire(){
            return cy.get('#exdate0')
        }
        get professionalVerify(){
            return cy.get('#verdate0')
        }
    
     
     
    
        get certificateverification(){
            return cy.get('div[ng-show="active==2"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(2) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
        }
        get certificateverificationID(){
            return cy.get(':nth-child(2) > :nth-child(3) > .fg-line > .global__txtbox')
        }
        get certificateverificationEffective(){
            return cy.get('#efdateu1')
        }
        get certificateverificationExpire(){
            return cy.get('#exdate1')
        }
        get certificateverificationVerify(){
            return cy.get('#verdate1')
        }
    
        get CPR(){
            return cy.get('div[ng-show="active==2"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(3) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
        }
        get CPRID(){
            return cy.get(':nth-child(3) > :nth-child(3) > .fg-line > .global__txtbox')
        }
        get CPREffective(){
            return cy.get('#efdateu2')
        }
        get CPRExpire(){
            return cy.get('#exdate2')
        }
        get CPRVerify(){
            return cy.get('#verdate2')
        }
    
        get driverlicense(){
            return cy.get('div[ng-show="active==2"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(4) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
        }

        get driverlicenseID(){
            return cy.get(':nth-child(4) > :nth-child(3) > .fg-line > .global__txtbox')
        }
        get driverlicenseEffective(){
            return cy.get('#efdateu3')
        }
        get driverlicenseExpire(){
            return cy.get('#exdate3')
        }
        get driverlicenseVerify(){
            return cy.get('#verdate3')
        }
    
    
        get carinsurance(){
            return cy.get('div[ng-show="active==2"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(5) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
        }

        get carinsuranceID(){
            return cy.get(':nth-child(5) > :nth-child(3) > .fg-line > .global__txtbox')
        }
        get carinsuranceEffective(){
            return cy.get('#efdateu4')
        }
        get carinsuranceExpire(){
            return cy.get('#exdate4')
        }
        get carinsuranceVerify(){
            return cy.get('#verdate4')
        }

    
        get professionalliability(){
            return cy.get('div[ng-show="active==2"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(6) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
        }

        get professionalliabilityID(){
            return cy.get(':nth-child(6) > :nth-child(3) > .fg-line > .global__txtbox')
        }
        get professionalliabilityEffective(){
            return cy.get('#efdateu5')
        }
        get professionalliabilityExpire(){
            return cy.get('#exdate5')
        }
        get professionalliabilityVerify(){
            return cy.get('#verdate5')
        }
    
    
        get skillschecklist(){
            return cy.get('div[ng-show="active==2"] > .ng-dirty.ng-valid-required > .table-responsive > .table > tbody > :nth-child(7) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
        }

        get skillschecklistEffective(){
            return cy.get('#efdateu6')
        }
        get skillschecklistExpire(){
            return cy.get('#exdate6')
        }
        get skillschecklistVerify(){
            return cy.get('#verdate6')
        }

    
        get competencyevaluation(){
            return  cy.get('.table-responsive > .table > tbody > :nth-child(8) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
        }

        get competencyevaluationEffective(){
            return cy.get('#efdateu7')
        }
        get competencyevaluationExpire(){
            return cy.get('#exdate7')
        }
        get competencyevaluationVerify(){
            return cy.get('#verdate7')
        }
    
        get annualperformance(){
            return cy.get('.table-responsive > .table > tbody > :nth-child(9) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
        }
 
        get annualperformanceEffective(){
            return cy.get('#efdateu8')
        }
        get annualperformanceExpire(){
            return cy.get('#exdate8')
        }
        get annualperformanceVerify(){
            return cy.get('#verdate8')
        }
    
        get livescan(){
            return cy.get('.table-responsive > .table > tbody > :nth-child(10) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single')
        }
   
        get livescanEffective(){
            return cy.get('#efdateu9')
        }
        get livescanExpire(){
            return cy.get('#exdate9')
        }
        get livescanVerify(){
            return cy.get('#verdate9')
        }

    
  
}