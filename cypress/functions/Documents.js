import DocumentSelector from "../models/DocumentSelector";
const document = new DocumentSelector();

function AddDocument(documentData = {}){
    document.edit.click()
    document.upload.click()
    cy.wait(2000)
    document.directoryClick.click()

    documentData.directory != "" 
    ? document.directoryEnter.type(`${documentData.directory}{enter}`) 
    : document.directoryEnter.type('Default{enter}')

    document.fileClick.click()
    cy.get('input[type=file]').selectFile(documentData.file, { force: true })

    document.cancel.click()
}

export { AddDocument }