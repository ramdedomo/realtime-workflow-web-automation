Feature: Physician

    Scenario: Adding and Editing a new Physician

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Medical Resources'
        Then a selected main nav should be expand

        When I click the sub nav 'Physicians'
        Then a selected sub nav should be display 

        # When I add new physician
        # Then added new physician should appear

        When I edit physician 'Joel Miller'
        Then the edited physician should be updated