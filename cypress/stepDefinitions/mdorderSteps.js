/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { ExploreMD } from '../functions/MDOrderManager';

    const mdFilter = {
        patient: 'patient',
        clinician: 'clinician',
        physician: 'physician',
        type: 'ROC',
        group: 'group',
        aged: 'aged',
        start: '',
        end: '',
    }

    When('i search patient {string}', (patient) => {
        ExploreMD(mdFilter, patient)    // filter md and search a patient
    })

    Then('the patient should appear', () => {

    })
