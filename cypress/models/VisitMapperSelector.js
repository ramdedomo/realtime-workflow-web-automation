export default class VisitMapperSelector{
    get forward(){
        return cy.get('[ng-click="schedvisits.$nextDate(schedvisits.datetoday)"] > .fa')
    }
    get backward(){
        return cy.get('[ng-click="schedvisits.$prevDate(schedvisits.datetoday)"] > .fa')
    }
}