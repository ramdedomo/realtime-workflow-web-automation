Feature: Healthcare Vendors

    Scenario: Adding and Editing a new Healthcare Vendor

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Medical Resources'
        Then a selected main nav should be expand

        When I click the sub nav 'Healthcare Vendors'
        Then a selected sub nav should be display 

        When I add new healthcare vendor
        Then added new healthcare vendor should appear 

        # When I edit healthcare vendor 'Healthcare Automation'
        # Then the edited healthcare vendor should be updated