export default {
    personal: {
        firstname: "Tess",
        lastname: "Cooper",
        middleinitial: "S",
        gender: {
            maleCheck: true,
            femaleCheck: false,
        },
        birthdate: "04/27/1960",
        race: "Asian",
        martial: "Married",
        discipline: "Physical",
        title: "Physical",
        hire: "Direct",
        position: "Registered",
        ssn: "143534543",
        clinicianid: "",
        company: "",
        address1: "1030 Southwood Dr",
        address2: "",
        city: "San Luis Obispo",
        state: "CA",
        zip: "93401",
        phone: '8705360046',
        mobile: '09348372819',
        fax: '3454-454-454',
        email: 'tess@email.com',
        hiredate: '04/27/2023',
        terminationdate: '',
        languageprimary: "English",
        languagesecondary: "Filipino",
        verbalprimary: "Inter",
        verbalsecondary: "Inter",
        readingprimary: "Inter",
        readingsecondary: "Inter",
        writingprimary: "Inter",
        writingsecondary: "Inter",
    },
    professional: {
        professionallicense: {
            option: "yes",
            professionallicenseID: "234234234",
            professionalEffective: "04/28/2023",
            professionalExpire: "04/28/2024",
            professionalVerify: "04/28/2023"
        },
        certificateverification: {
            option: "no",
            certificateverificationID: "",
            certificateverificationEffective: "",
            certificateverificationExpire: "",
            certificateverificationVerify: ""
        },
        CPR: {
            option: "yes",
            CPRID: "293847239",
            CPREffective: "04/27/2023",
            CPRExpire: "04/27/2024",
            CPRVerify: "04/27/2023"
        },
        driverlicense: {
            option: "yes",
            driverlicenseID: "293847239",
            driverlicenseEffective: "04/27/2023",
            driverlicenseExpire: "04/27/2024",
            driverlicenseVerify: "04/27/2023"
        },
        carinsurance: {
            option: "yes",
            carinsuranceID: "293847239",
            carinsuranceEffective: "04/27/2023",
            carinsuranceExpire: "04/27/2024",
            carinsuranceVerify: "04/27/2023"
        },
        professionalliability: {
            option: "yes",
            professionalliabilityID: "293847239",
            professionalliabilityEffective: "04/27/2023",
            professionalliabilityExpire: "04/27/2024",
            professionalliabilityVerify: "04/27/2023"
        },
        skillschecklist: {
            option: "yes",
            skillschecklistEffective: "04/27/2023",
            skillschecklistExpire: "04/27/2024",
            skillschecklistVerify: "04/27/2023"
        },
        competencyevaluation: {
            option: "yes",
            competencyevaluationEffective: "04/27/2023",
            competencyevaluationExpire: "04/27/2024",
            competencyevaluationVerify: "04/27/2023"
        },
        annualperformance: {
            option: "yes",
            annualperformanceEffective: "04/27/2023",
            annualperformanceExpire: "04/27/2024",
            annualperformanceVerify: "04/27/2023"
        },
        livescan: {
            option: "yes",
            livescanEffective: "04/27/2023",
            livescanExpire: "04/27/2024",
            livescanVerify: "04/27/2023"
        },
    },
    health: {
        physicalexamination: {
            option: "yes",
            physicalexaminationEffective: "04/27/2023",
            physicalexaminationExpire: "04/27/2024",
            physicalexaminationVerify: "04/27/2023"
        },
        PPD: {
            option: "yes",
            PPDEffective: "04/27/2023",
            PPDExpire: "04/27/2024",
            PPDVerify: "04/27/2023"
        },
        Chest: {
            option: "yes",
            ChestEffective: "04/27/2023",
            ChestExpire: "04/27/2024",
            ChestVerify: "04/27/2023"
        },
        tuberculosis: {
            option: "yes",
            tuberculosisEffective: "04/27/2023",
            tuberculosisExpire: "04/27/2024",
            tuberculosisVerify: "04/27/2023"
        },
        medicalhistory: {
            option: "yes",
            medicalhistoryEffective: "04/27/2023",
            medicalhistoryExpire: "04/27/2024",
            medicalhistoryVerify: "04/27/2023"
        },
        healthquestionnaire: {
            option: "yes",
            healthquestionnaireEffective: "04/27/2023",
            healthquestionnaireExpire: "04/27/2024",
            healthquestionnaireVerify: "04/27/2023"
        },
        hepatitis: {
            option: "yes",
            hepatitisEffective: "04/27/2023",
            hepatitisExpire: "04/27/2024",
            hepatitisVerify: "04/27/2023"
        },
    },
}