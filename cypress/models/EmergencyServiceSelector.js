export default class EmergencyServiceSelector{
    get categoryClick(){
        return cy.get('.chosen-single')
    }
    get categoryEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(2) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }
    get addCategoryClick(){
        return cy.get('.btn__primary_icon-bordered')
    }
    get addCategoryEnter(){
        return cy.get('#title')
    }
    get addCategorySave(){
        return cy.get('.mhc__btn-affirmative')
    }
    get name(){
        return cy.get(':nth-child(3) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get phone(){
        return cy.get(':nth-child(4) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get otherphone(){
        return cy.get(':nth-child(5) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get save(){
        return cy.get('.btn__success')
    }
}