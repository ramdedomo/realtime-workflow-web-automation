/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { AddInsurance, EditInsurance } from '../functions/InsuranceCompany';

    const addInsuranceData = {
        name: "Insurance Automation",
        abbr: "IA",
        contactperson: "Test Contact Person",
        address: "212 Martella St",
        city: "Salinas",
        state: "CA",
        zip: "93901",
        phone: "09374839201",
        fax: "831-757-3640",
        payertype: "Medicaid",
        invoicetype: "UB",
        typeofbill: "032x",
        chargetype: "Visit",
        parentinsurance: "None",
        entermediary: "CGS",
        providercode: "45634",
        providerno: "34456",
        insurancewilluse: "ICD-10",
        patientcare: {
            followmedicareCheck: true,
            exportableOASISCheck: true,
            comprehensiveEvaluationCheck: false
        }
    }

    const editInsuranceData = {
        contactperson: "Test Contact Person Updated",
    }

    When('I add new insurance company', () => {
        AddInsurance(addInsuranceData, false)   // add insurance data, the second parameter is to save or not
    })

    Then('added new insurance company should appear', () => {

    })

    When('I edit insurance company {string}', (company) => {
        EditInsurance(editInsuranceData, company, false)  // edit insurance data, the second parameter is what company should be edited and the last parameter is to save or not
    })

    Then('the edited insurance company should be updated', () => {

    })
