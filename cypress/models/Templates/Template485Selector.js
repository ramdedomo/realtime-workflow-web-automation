export default class Template485Selector{
    get ICD(){
        return cy.get(':nth-child(1) > .oasis__answer > .global__txtbox')
    }

    get newbodysystemClick(){
        return cy.get(':nth-child(2) > .oasis__answer > .fg-line > .btn')
    }
    get newbodysystemDomain(){
        return cy.get('.m-0 > .container > .block-header > fieldset > ng-form.ng-pristine > .table > :nth-child(1) > :nth-child(1) > .oasis__answer > .global__txtbox')
    }
    get newbodysystemGeneral(){
        return cy.get('.genassestmentitem > table > tbody > tr > td > .form-control')
    }
    get newbodysystemGeneralAdd(){
        return cy.get(':nth-child(3) > .oasis__answer > .row > .col-sm-8 > .btn > .zmdi')
    }
    //
    get bodysystemClick(){
        return cy.get(':nth-child(2) > .oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get bodysystemEnter(){
        return cy.get('#content > data > div.container.ng-scope > ui-view > div > div.block-header.bg__wt.max__width_wrapper.p-15 > fieldset > ng-form > table > tbody > tr:nth-child(2) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }


    get newcategoryClick(){
        return cy.get(':nth-child(3) > .oasis__answer > .fg-line > .btn')
    }
    get newcategoryClassification(){
        return cy.get('.m-0 > .container > .block-header > fieldset > ng-form.ng-pristine > .table > :nth-child(1) > :nth-child(1) > .oasis__answer > .global__txtbox')
    }
    get newcategoryGeneral(){
        return cy.get('.genassestmentitem > table > tbody > tr > td > .form-control')
    }
    get newcategoryGeneralAdd(){
        return cy.get(':nth-child(3) > .oasis__answer > .row > .col-sm-8 > .btn > .zmdi')
    }
    //
    get categoryClick(){
        return cy.get(':nth-child(3) > .oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get categoryEnter(){
        return cy.get('#content > data > div.container.ng-scope > ui-view > div > div.block-header.bg__wt.max__width_wrapper.p-15 > fieldset > ng-form > table > tbody > tr:nth-child(3) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }

    get modalsaveNew(){
        return cy.get('.text-right > .mhc__btn-affirmative')
    }
    get modalcloseNew(){
        return cy.get('.mhc__btn-neutral')
    }
 
    get diagnosis(){
        return cy.get(':nth-child(4) > .oasis__answer > .global__txtbox')
    }
    get goal(){
        return cy.get(':nth-child(6) > .oasis__answer > .row > .col-sm-8 > .form-control')
    }

    get sntoperform(){
        return cy.get(':nth-child(8) > .oasis__answer > .row > .col-sm-8 > .teachitem > table > tbody > tr > td > .form-control')
    }
    get sntoperformAdd(){
        return cy.get(':nth-child(8) > .oasis__answer > .row > .col-sm-8 > .btn > .zmdi')
    }
    get sntoperformOn(){
        return cy.get(':nth-child(7) > .global__table-question > div.ng-scope > .btn')
    }
   

    get sntoteach(){
        return cy.get(':nth-child(10) > .oasis__answer > .row > .col-sm-8 > .teachitem > table > tbody > tr > td > .form-control')
    }
    get sntoteachAdd(){
        return cy.get(':nth-child(10) > .oasis__answer > .row > .col-sm-8 > .btn > .zmdi')
    }
    get sntoteachOn(){
        return cy.get(':nth-child(9) > .global__table-question > div.ng-scope > .btn')
    }

    get sntoreport(){
        return cy.get(':nth-child(14) > .oasis__answer > .row > .col-sm-8 > .teachitem > table > tbody > tr > td > .form-control')
    }
    get sntoreportAdd(){
        return cy.get(':nth-child(14) > .oasis__answer > .row > .col-sm-8 > .btn > .zmdi')
    }
    get sntoreportOn(){
        return cy.get(':nth-child(13) > .global__table-question > div.ng-scope > .btn')
    }
    
}