/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { AddUser, EditUser, EditAccount, ResetPassword } from '../functions/User';
import addUserData from '../functions/data/addUserData';

    When('I add new user', () => {
        AddUser(addUserData, true) // add user
    })

    Then('added new user should appear', () => {
   
    })
