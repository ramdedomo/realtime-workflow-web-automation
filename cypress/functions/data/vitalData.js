export default {
    temperature: {
        fahrenheit: "100",
        location: {
            temperature_oral: true,
            temperature_scan: false,
            temperature_tympanic: false,
        }
    },
    pulse: {
        beatspermin: '110',
        method: {
            pulse_radial: true,
            pulse_apical: false,
            pulse_brachial: false,
        }
    },
    respiration: '24',
    armbpleft: {
        systo: '120',
        diasto: '80',
        position: {
            armbpleft_sitting: true,
            armbpleft_lying: false,
            armbpleft_standing: false,
        }
    },
    armbpright: {
        systo: '120',
        diasto: '80',
        position: {
            armbpright_sitting: true,
            armbpright_lying: false,
            armbpright_standing: false,
        }
    },
    legbpleft: {
        systo: '120',
        diasto: '80',
        position: {
            legbpleft_sitting: true,
            legbpleft_lying: false,
            legbpleft_standing: false,
        }
    },
    legbpright: {
        systo: '120',
        diasto: '80',
        position: {
            legbpright_sitting: true,
            legbpright_lying: false,
            legbpright_standing: false,
        }
    },
    o2saturation: "10",
    o2saturation_: "10",
    o2saturation_at: "5",
    bloodsugar: {
        bloodsugar: '100',
        option: {
            bloodsugar_FBS: true,
            bloodsugar_RBS: false,
            bloodsugar_2hr: false,
        }
    },
    weight: {
        weight: '176.3',
        option: {
            weight_actual: true,
            weight_stated: false,
            weight_unable: false,
        }
    },
    height: {
        height: '72',
        option: {
            height_actual: true,
            height_stated: false,
        }
    },
    vitalsbeyond: {
        vitalsbeyond: "test",
        notified: {
            vitalsbeyond_md: true,
            vitalsbeyond_cm: false,
        }
    },
    evidence: {
        evidence: true,
        evidence_yes_decribe: "Test",
        notified: {
            evidence_md: true,
            evidence_cm: false,
        }
    },
    newchange: {
        newchange: true,
        newchange_yes_decribe: "test",
    },
    vitalsigns_observation: "Test",
    vitalsigns_intervention: "Test",
    //VS parameters
    vsparameters: {
        temperature_gt: "102",
        temperature_lt: "96",
        pulse_gt: "150",
        pulse_lt: "50",
        respiration_gt: "24",
        respiration_lt: "10",
        o2saturation_lt: "95",
        systolic_gt: "200",
        systolic_lt: "90",
        diastolic_gt: "90",
        diastolic_lt: "50",
        fasting_gt: "200",
        fasting_lt: "60",
        random_gt: "300",
        random_lt: "60",
    },
    //
    J0510: '0',
    J0520: '',
    J0530: '',
    B0200: '0',
    B01000: '0',
    B1300: '0',
    //
    eyes: {
        wnl: false,
        reading: false,
        option: {
            eyes_cataract_l: true,
            eyes_cataract_r: true,
            //
            eyes_glaucoma_l: true,
            eyes_glaucoma_r: true,
            //
            eyes_redness_l: true,
            eyes_redness_r: true,
            //
            eyes_pain_l: true,
            eyes_pain_r: true,
            //
            eyes_itching_l: true,
            eyes_itching_r: true,
            //
            eyes_ptosis_l: true,
            eyes_ptosis_r: true,
            //
            eyes_sciera_l: true,
            eyes_sciera_r: true,
            //
            eyes_edema_l: true,
            eyes_edema_r: true,
            //
            eyes_blured_l: true,
            eyes_blured_r: true,
            //
            eyes_blind_l: true,
            eyes_blind_r: true,
            //
            eyes_exudate_l: true,
            eyes_exudate_r: true,
            //
            eyes_tearing_l: true,
            eyes_tearing_r: true,
            //
            eyes_macular_l: true,
            eyes_macular_r: true,
            //
            eyes_retinopathy_l: true,
            eyes_retinopathy_r: true,
            //
            eyes_other: {
                eyes_other: "Test Other",
                eyes_other_l: true,
                eyes_other_r: true,
            }
        }
    },
    ears: {
        wnl: false,
        other: "Test",
        option: {
            ears_vertigo: true,
            //
            ears_HOH_l: true,
            ears_HOH_r: true,
            //
            ears_aid_l: true,
            ears_aid_r: true,
            //
            ears_deaf_l: true,
            ears_deaf_r: true,
            //
            ears_tinnitus_l: true,
            ears_tinnitus_r: true,
            //
            ears_drainage_l: true,
            ears_drainage_r: true,
            //
            ears_pain_l: true,
            ears_pain_r: true,
        }
    },
    mouth: {
        wnl: false,
        dentures: true,
        dentures_option: {
            mouth_denture_upper: true,
            mouth_denture_lower: false,
            mouth_denture_partial: false,
        },
        option: {
            mouth_poordentition: true,
            mouth_gingivitis: true,
            mouth_toothache: true,
            mouth_lossoftaste: true,
            mouth_lesions: {
                mouth_lesions: true,
                mouth_lesions_describe: "Test"
            },
            mouth_mass: {
                mouth_mass: true,
                mouth_mass_describe: "Test"
            },
            mouth_difficulty: true,
            mouth_other: "Test Other",
        }
    },
    nose: {
        wnl: false,
        option: {
            nose_congestion: true,
            nose_rhinitis: true,
            nose_sinusprob: true,
            nose_lossofsmell: true,
            nose_epistaxis: true,
            nose_noisy: true,
            nose_lesions: {
                nose_lesions: true,
                nose_lesions_describe: "Test"
            },
            nose_lesions_describe: true,
            nose_other: "Test",
        }
    },
    throat: {
        wnl: false,
        option: {
            throat_sore: true,
            throat_dysphagia: true,
            throat_hoarness: true,
            throat_lesions: {
                throat_lesions: true,
                throat_lesions_describe: "Test"
            },
            throat_other: "Test"
        }
    },
    speech: {
        wnl: false,
        option: {
            speech_slow: true, 
            speech_slurred: true,
            speech_aphasia: true,
            speech_mechanical: true,
            speech_other: "Test"
        }
    },
    //
    touch: {
        wnl: false,
        option: {
            touch_paresthesia: true, 
            touch_hyperesthesia: true, 
            touch_peripheral: true, 
            touch_other: "Test"
        }
    },
    sensory_observation: "Test",
    sensory_intervention: "Test",
    //
    neurological: {
        wnl: false,
        pupils_perrla: false, 
        options: {
            sizeunequal_lgr: true, 
            sizeunequal_llr: true, 
            //
            nonreactive_left: true, 
            nonreactive_right: true, 
            //
            mental_oriented: true, 
            mental_oriented_time: true, 
            mental_oriented_place: true, 
            mental_oriented_person: true, 
            mental_oriented_purpose: true, 
            //
            mental_disoriented: true, 
            mental_lethargic: true, 
            mental_depressed: true, 
            mental_forgetful: true, 
            mental_agitated: true, 
            mental_comatose: true, 
            //
            sleep_adequate: false, 
            sleep_inadequate: true, 
            sleep_inadequate_describe: "Test",
            sleep_MD: true, 
            //
            hadngrips_strong_left: true, 
            hadngrips_strong_right: true, 
            //
            hadngrips_weak_left: true, 
            hadngrips_weak_right: true, 
        },
        othersigns: {
            othersigns_weakness: {
                othersigns_weakness: true, 
                //
                option: {
                    weakness_upperextremity_left: true, 
                    weakness_upperextremity_right: true, 
                    //
                    weakness_lowerextremity_left: true, 
                    weakness_lowerextremity_right: true, 
                    //
                    weakness_hemiparesis_left: true, 
                    weakness_hemiparesis_right: true, 
                }
            },
            othersigns_paralysis: {
                othersigns_paralysis: true, 
                //
                option: {
                    paralysis_hemiplegia_left: true, 
                    paralysis_hemiplegia_right: true, 
                    //
                    paralysis_paraplegia: true, 
                    paralysis_quadriplegia: true, 
                }
            },
            othersigns_tremors: {
                othersigns_tremors: true, 
                //
                option: {
                    tremors_upperextremity_left: true, 
                    tremors_upperextremity_right: true, 
                    tremors_upperextremity_fine: true,
                    tremors_upperextremity_coarset: true,
                    //
                    tremors_lowerextremity_left: true, 
                    tremors_lowerextremity_right: true, 
                    tremors_lowerextremity_fine: true,
                    tremors_lowerextremity_coarset: true,
                }
            },
            othersigns_seizure: {
                othersigns_seizure: true, 
                //
                option: {
                    seizure_grand: true, 
                    seizure_petit: true, 
                    seizure_partial: true, 
                    seizure_lastseizure: "02/21/2023", 
                    seizure_duration: "10"
                }

            },
            othersigns_other: "Test"
        }
    },
    neurological_observation: "test",
    neurological_intervention: "test"
}