export default class PreAdmissionSelector{
    get referraldate(){
        return cy.get('#refDate')
    }
    get referraltime(){
        return cy.get('.time-mask')
    }
    get autoassign(){
        return cy.get(':nth-child(2) > [colspan="2"] > table > tbody > tr > .global__table-question > .checkbox > label > .ng-pristine')
    }
    get manualassign(){
        return cy.get('#takenmrn')
    }
    get SOCdate(){
        return cy.get('#pre_admission_date')
    }
    get lastname(){
        return cy.get('#last_name')
    }
    get firstname(){
        return cy.get('#first_name')
    }
    get middleinitial(){
        return cy.get('#mi')
    }
    get suffix(){
        return cy.get('#suffix')
    }
    get birthdate(){
        return cy.get('#birthdate')
    }
    get male(){
        return cy.get(':nth-child(10) > .oasis__answer > .custom-input-radio > :nth-child(1) > .ng-pristine')
    }
    get female(){
        return cy.get(':nth-child(10) > .oasis__answer > .custom-input-radio > :nth-child(2) > .ng-pristine')
    }

    get maritalClick(){
        return cy.get('#marital_status_chosen > .chosen-single')
    }
    get maritalEnter(){
        return cy.get('#marital_status_chosen > div > div > input[type=text]')
    }

    get ethnicityClick(){
        return cy.get('#ethnicities_chosen > .chosen-choices')
    }
    get ethnicityEnter(){
        return cy.get('#ethnicities_chosen > ul > li > input')
    }

    get languagespokenClick(){
        return cy.get('.ui-select-search')
    }
    get English(){
        return cy.get('#ui-select-choices-row-0-0 > span')
    }
    get Spanish(){
        return cy.get('#ui-select-choices-row-0-1 > span')
    }
    get Mandarin(){
        return cy.get('#ui-select-choices-row-0-2 > span')
    }
    get Cantonese(){
        return cy.get('#ui-select-choices-row-0-3 > span')
    }
    get Vietnamese(){
        return cy.get('#ui-select-choices-row-0-4 > span')
    }
    get Russian(){
        return cy.get('#ui-select-choices-row-0-5 > span')
    }

    get ssn(){
        return cy.get('#ssNumber')
    }
    get street(){
        return cy.get('#main_line1')
    }
    get additionalstreet(){
        return cy.get('#main_line2')
    }
    get majorstreet(){
        return cy.get('#main_street')
    }
    get city(){
        return cy.get('#main_city')
    }

    get stateClick(){
        return cy.get('#main_state_chosen > .chosen-single')
    }
    get stateEnter(){
        return cy.get('#main_state_chosen > div > div > input[type=text]')
    }

    get zip(){
        return cy.get('#main_zipcode')
    }
    get phone1(){
        return cy.get(':nth-child(22) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get phone2(){
        return cy.get(':nth-child(23) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get email(){
        return cy.get('#main_email')
    }
    get sameas(){
        return cy.get(':nth-child(25) > [colspan="2"] > .btn__dark-blue_icon-bordered')
    }

    get livesalone(){
        return cy.get(':nth-child(35) > .oasis__answer > .custom-input-radio > :nth-child(1)')
    }
    get withcaregiver(){
        return cy.get(':nth-child(35) > .oasis__answer > .custom-input-radio > :nth-child(2)')
    }
    get liveswithcongregate(){
        return cy.get(':nth-child(35) > .oasis__answer > .custom-input-radio > :nth-child(3)')
    }

    get roundtheclock(){
        return cy.get(':nth-child(36) > .oasis__answer > .custom-input-radio > :nth-child(1)')
    }
    get regdaytime(){
        return cy.get(':nth-child(36) > .oasis__answer > .custom-input-radio > :nth-child(2)')
    }
    get regnighttime(){
        return cy.get(':nth-child(36) > .oasis__answer > .custom-input-radio > :nth-child(3)')
    }
    get occasionalshort(){
        return cy.get('.custom-input-radio > :nth-child(4)')
    }
    get noassistance(){
        return cy.get('.custom-input-radio > :nth-child(5)')
    }

    get caregiver(){
        return cy.get('#ls_caregiver')
    }
    get caregiverphone(){
        return cy.get('#ls_caregiver_phone')
    }


    get emergencyname(){
        return cy.get('#ec_name')
    }
    get emergencyrelationship(){
        return cy.get('#ec_relationship')
    }
    get emergencyphone1(){
        return cy.get('#ec_phone')
    }
    get emergencyphone2(){
        return cy.get('#ec_other_phone')
    }

    get attendingPhysicianClick(){
        return cy.get('#physician_attending_chosen > .chosen-single')
    }
    get attendingPhysicianEnter(){
        return cy.get('#physician_attending_chosen > div > div > input[type=text]')
    }

    get sameasPhysician(){
        return cy.get('[ng-click="form.physician.primary.sameAs()"]')
    }

    get primaryPhysicianClick(){
        return cy.get('#physician_primary_chosen > .chosen-single')
    }
    get primaryPhysicianEnter(){
        return cy.get('#physician_primary_chosen > div > div > input[type=text]')
    }

    get otherPhysicianClick(){
        return cy.get('[style="height: unset;"] > .select > .chosen-container > .chosen-choices')
    }
    get otherPhysicianEnter(){
        return cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > ng-form > fieldset > table > tbody > tr:nth-child(46) > td:nth-child(2) > div > div > ul > li > input')
    }

    get primaryInsuranceClick(){
        return cy.get('#primary_insurance_chosen > .chosen-single')
    }
    get primaryInsuranceEnter(){
        return cy.get('#primary_insurance_chosen > div > div > input[type=text]')
    }
    get primaryInsurancePolicy(){
        return cy.get('#primary_insurance_policy')
    }
    get primaryInsuranceAuth(){
        return cy.get('#primary_insurance_authorization')
    }
 

    get secondaryInsuranceClick(){
        return cy.get('#secondary_insurance_chosen > .chosen-single')
    }
    get secondaryInsuranceEnter(){
        return cy.get('#secondary_insurance_chosen > div > div > input[type=text]')
    }
    get secondaryInsurancePolicy(){
        return cy.get('#secondary_insurance_policy')
    }
    get secondaryInsuranceAuth(){
        return cy.get('#secondary_insurance_authorization')
    }


    get typeofadmissionClick(){
        return cy.get('#admissiontype_chosen > .chosen-single')
    }
    get typeofadmissionEnter(){
        return cy.get('#admissiontype_chosen > div > div > input[type=text]')
    }

    get pointoforiginClick(){
        return cy.get('#point_of_origin_chosen > .chosen-single')
    }
    get pointoforiginEnter(){
        return cy.get('#point_of_origin_chosen > div > div > input[type=text]')
    }


    get typeofreferralClick(){
        return cy.get('#referral_type_chosen > .chosen-single')
    }
    get typeofreferralEnter(){
        return cy.get('#referral_type_chosen > div > div > input[type=text]')
    }

    get referralsourcesClick(){
        return cy.get('#referral_source_id_chosen > .chosen-single')
    }
    get referralsourcesEnter(){
        return cy.get('#referral_source_id_chosen > div > div > input[type=text]')
    }

    get referralphone(){
        return cy.get('#referral_phone')
    }

    get hospitalClick(){
        return cy.get('#hospital_id_chosen > .chosen-single')
    }
    get hospitalEnter(){
        return cy.get('#hospital_id_chosen > div > div > input[type=text]')
    }

    get hospitaladmit(){
        return cy.get('#admit_date')
    }
    get hospitaldischarge(){
        return cy.get('#discharge_date')
    }

    get diagnosissurgery(){
        return cy.get('#diagnosis_surgery')
    }
    get allergiesnka(){
        return cy.get('#diagnosis_nka')
    }
    get allergiestags(){
        return cy.get('#diagnosis_allergies > .host > .tags')
    }
    

    get rn(){
        return cy.get(':nth-child(68) > .oasis__answer > .fg-line > :nth-child(1) > .ng-pristine')
    }
    get pt(){
        return cy.get('.fg-line > :nth-child(3) > .ng-pristine')
    }
    get st(){
        return cy.get('.fg-line > :nth-child(5) > .ng-pristine')
    }
    get ot(){
        return cy.get(':nth-child(7) > .ng-pristine')
    }



    get rnClick(){
        return cy.get('#cs_rn_chosen > .chosen-single')
    }
    get rnEnter(){
        return cy.get('#cs_rn_chosen > div > div > input[type=text]')
    }

    get ptClick(){
        return cy.get('#cs_pt_chosen > .chosen-single')
    }
    get ptEnter(){
        return cy.get('#cs_pt_chosen > div > div > input[type=text]')
    }

    get stClick(){
        return cy.get('#cs_st_chosen > .chosen-single')
    }
    get stEnter(){
        return cy.get('#cs_st_chosen > div > div > input[type=text]')
    }

    get otClick(){
        return cy.get('#cs_ot_chosen > .chosen-single')
    }
    get otEnter(){
        return cy.get('#cs_ot_chosen > div > div > input[type=text]')
    }

    get cmClick(){
        return cy.get('#cs_cm_chosen > .chosen-single')
    }
    get cmEnter(){
        return cy.get('#cs_cm_chosen > div > div > input[type=text]')
    }

    get save(){
        return cy.get('.btn__success')
    }
}