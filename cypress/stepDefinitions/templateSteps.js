/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { Add485Template, AddClassificationTemplate, AddMDOTemplate, AddWoundTemplate } from '../functions/Templates'

    const add485TemplateData = {
        ICD: "3274982374",
        bodysystem: {
            newbodysystem: {
                add: true,
                domain: 'test domain',
                general: 'test general',
            },
            bodysystem: "Test1"
        },
        category: {
            newcategory: {
                add: true,
                classification: 'test classification',
                general: 'test general'
            },
            category: "Testers"
        },
        diagnosis: "test diagnosis",
        goal: "test goal",
        sntoperform: {
            rearrange: true,
            //note: magkaiba ang selector pag dalawa lang ang array
            string: [
                'test1',
                'test2',
                'test3',
                'test4',
            ]
        },
        sntoteach: {
            rearrange: true,
            //note: magkaiba ang selector pag dalawa lang ang array
            string: [
                'test1',
                'test2',
                'test3',
                'test4',
            ]
        },
        sntoreport: {
            rearrange: true,
            //note: magkaiba ang selector pag dalawa lang ang array
            string: [
                'test1',
                'test2',
                'test3',
                'test4',
            ]
        }
    }
    const addClassificationData = {
        category: '485',
        classification: 'Test Classification',
        generalassesment: ['Test1','Test2','Test3']
    }
    const addWoundData = {
        field: 'Frequency',
        entry: 'Test Entry'
    }
    const addMDOData = {
        title: 'test title',
        description: 'test description'
    }

    When('I add new 485 template', () => {
        Add485Template(add485TemplateData, false) // add 485Template
    })

    Then('added new 485 template should appear', () => {

    })

    When('I add new classification template', () => {
        AddClassificationTemplate(addClassificationData, false) // add Classification
    })

    Then('added new classification template should appear', () => {

    })

    When('I add new wound treatment template', () => {
        AddWoundTemplate(addWoundData, true)// add wound treatment
    })

    Then('added new wound treatment template should appear', () => {

    })

    When('I add new MDO template', () => {
        AddMDOTemplate(addMDOData, false) // add MDO template
    })

    Then('added new MDO template should appear', () => {

    })


