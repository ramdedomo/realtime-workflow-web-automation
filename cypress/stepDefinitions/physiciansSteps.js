/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { AddPhysician, EditPhysician } from '../functions/Physician';

    const addPhyscianData = {
        fname: "Joel", 
        mname: "Williams", 
        lname: "Miller",
        address: "882 Maria Way Vis", 
        city: "Placerville", 
        state: "CA", 
        zip: "95667", 
        phone: "09238472812", 
        fax: "453-567-3924", 
        cellphone: "09238472812", 
        contactPerson: "Test Contact Person", 
        specialty: "specialty test", 
        licenseNo: "2342357abc", 
        expirationDate: "04/25/2023", 
        email: "jwilliams@email.com"
    }

    const editPhysicianData = {
        contactPerson: "Test Contact Person", 
    }

    When('I add new physician', () => {
        AddPhysician(addPhyscianData, false)  // add physician data, the second parameter is to save or not
    });
    
    Then('added new physician should appear', () => {

    });

    When('I edit physician {string}', (person) => {
        EditPhysician(editPhysicianData, person, false)  // edit physician data, the second parameter is who should be edited and the last parameter is to save or not
    });    
    
    Then('the edited physician should be updated', () => {

    });