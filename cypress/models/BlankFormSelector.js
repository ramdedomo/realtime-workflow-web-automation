export default class BlankFormSelector{
    get edit(){
        return cy.get('.btn__warning')
    }
    get cancel(){
        return cy.get('.pull-right > .btn__warning')
    }
    get upload(){
        return cy.get('.btn__primary')
    }
    get directoryClick(){
        return cy.get('.chosen-single')
    }
    get directoryEnter(){
        return cy.get('body > div.modal.fade.ng-isolate-scope.patient-records-upload.in > div > div > div > div > div.form-group > div > div > div > div > div > input[type=text]')
    }
    get fileClick(){
        return cy.get('.col-md-12 > .btn')
    }
}