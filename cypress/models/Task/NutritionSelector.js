export default class NutritionSelector{
    get M1600(){

    }
    get M1610(){
        cy.get(':nth-child(2) > :nth-child(1) > .molocked > .oasis__enter-code > .form-group > #code')
    }
    get genitourinarywnl(){
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(1) > .oasis__primary > .m-l-15 > .checkbox > .ng-pristine')
    }
    get genitourinary_urineclarity_clear(){
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(2) > :nth-child(2) > .checkbox > .ng-scope')
    }
    get genitourinary_urineclarity_cloudy(){
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(3) > .p-b-5.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get genitourinary_urineclarity_turbid(){
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(2) > .b-r-n.b-l-n > .checkbox > .ng-scope')
    }
    get genitourinary_urineclarity_sediments(){
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(3) > .b-r-n.b-l-n > .checkbox > .ng-scope')
    }
    get genitourinary_urineclarity_other(){
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(2) > .b-l-n.ng-isolate-scope > .global__txtbox')
    }

    get genitourinary_urinecolor_straw(){
        cy.get(':nth-child(4) > :nth-child(2) > .radio > .ng-pristine')
    }
    get genitourinary_urinecolor_yellow(){
        cy.get(':nth-child(5) > :nth-child(1) > .radio > .ng-pristine')
    }
    get genitourinary_urinecolor_amber(){
        cy.get(':nth-child(4) > :nth-child(3) > .radio > .ng-pristine')
    }
    get genitourinary_urinecolor_brown(){
        cy.get(':nth-child(5) > :nth-child(2) > .radio > .ng-pristine')
    }
    get genitourinary_urinecolor_red(){
        cy.get(':nth-child(4) > :nth-child(4) > .radio > .ng-pristine')
    }
    get genitourinary_urinecolor_other(){
        cy.get(':nth-child(5) > :nth-child(3) > .global__txtbox')
    }

    get genitourinary_urineodor_no(){
        cy.get(':nth-child(6) > :nth-child(2) > .radio > .ng-pristine')
    }
    get genitourinary_urineodor_yes(){
        cy.get(':nth-child(6) > .b-r-n.b-l-n > .radio > .ng-pristine')
    }
    get genitourinary_urineodor_describe(){
        cy.get('[colspan="5"] > .global__txtbox')
    }

    get abnormal_urinary_yes(){
        cy.get(':nth-child(7) > .b-l-n > :nth-child(2) > .ng-valid')
    }
    get abnormal_urinary_no(){
        cy.get(':nth-child(7) > .b-l-n > :nth-child(1) > .ng-valid')
    }
    get abnormal_urinary_frequency(){
        cy.get('tbody > :nth-child(1) > :nth-child(1) > .checkbox > .ng-scope')
    }
    get abnormal_urinary_dysuria(){
        cy.get(':nth-child(2) > [style="width: 150px;"] > .checkbox > .ng-scope')
    }
    get abnormal_urinary_retention(){
        cy.get('.inner__table-nb > tbody > :nth-child(3) > :nth-child(1) > .checkbox > .ng-scope')
    }
    get abnormal_urinary_hematuria(){
        cy.get(':nth-child(4) > [style="width: 150px;"] > .checkbox > .ng-scope')
    }
    get abnormal_urinary_incontinence(){
        cy.get('[style="width: 23%;"] > .checkbox > .ng-scope')
    }
    get abnormal_urinary_stress(){
        cy.get('.inner__table-nb > tbody > :nth-child(2) > :nth-child(2) > .checkbox > .ng-scope')
    }
    get abnormal_urinary_urgency(){
        cy.get('.inner__table-nb > tbody > :nth-child(3) > :nth-child(2) > .checkbox > .ng-scope')
    }
    get abnormal_urinary_polyuria(){
        cy.get(':nth-child(4) > :nth-child(2) > .checkbox > .ng-scope')
    }
    get abnormal_urinary_oliguria(){
        cy.get('.inner__table-nb > tbody > :nth-child(1) > :nth-child(3) > .checkbox > .ng-scope')
    }
    get abnormal_urinary_nocturia(){
        cy.get('[style="width: 180px;"] > .checkbox > .ng-scope')
    }
    get abnormal_urinary_other(){
        cy.get('.m-l-50 > .fg-line > .global__txtbox')
    }

    get special_procedure_no(){
        cy.get(':nth-child(9) > .b-l-n > :nth-child(1) > .ng-pristine')
    }
    get special_procedure_yes(){
        cy.get(':nth-child(9) > .b-l-n > :nth-child(2) > .ng-pristine')
    }

    get special_indwelling(){
        cy.get('[style="width: 23%;"] > div > .radio > .ng-valid')
    }
    get indwelling_catheter_type_urethral(){
        cy.get(':nth-child(1) > #SOOELIMINATION0017')
    }
    get indwelling_catheter_type_suprapubic(){
        cy.get('.m-l-40 > #SOOELIMINATION0017')
    }

    get indwelling_catheter_size_fr(){
        cy.get('.catheter__row > :nth-child(3) > .radio > .ng-pristine')
    }
    get indwelling_catheter_size_14(){
        cy.get('.catheter__row > :nth-child(4) > .radio > .ng-pristine')
    }
    get indwelling_catheter_size_16(){
        cy.get('.catheter__row > :nth-child(5) > .radio > .ng-pristine')
    }
    get indwelling_catheter_size_18(){
        cy.get('.catheter__row > :nth-child(6) > .radio > .ng-pristine')
    }
    get indwelling_catheter_size_20(){
        cy.get('.catheter__row > :nth-child(7) > .radio > .ng-pristine')
    }
    get indwelling_catheter_size_22(){
        cy.get('.catheter__row > :nth-child(8) > .radio > .ng-pristine')
    }
    get indwelling_catheter_size_24(){
        cy.get('.catheter__row > :nth-child(9) > .radio > .ng-pristine')
    }

    get indwelling_balloon_ml(){
        cy.get('.balloon_inflation__row > :nth-child(3) > .radio > .ng-pristine')
    }
    get indwelling_balloon_5(){
        cy.get('.balloon_inflation__row > :nth-child(4) > .radio > .ng-pristine')
    }
    get indwelling_balloon_10(){
        cy.get('.balloon_inflation__row > :nth-child(5) > .radio > .ng-pristine')
    }
    get indwelling_balloon_15(){
        cy.get('.balloon_inflation__row > :nth-child(6) > .radio > .ng-pristine')
    }
    get indwelling_balloon_20(){
        cy.get('.balloon_inflation__row > :nth-child(7) > .radio > .ng-pristine')
    }
    get indwelling_balloon_25(){
        cy.get('.balloon_inflation__row > :nth-child(8) > .radio > .ng-pristine')
    }
    get indwelling_balloon_30(){
        cy.get('.balloon_inflation__row > :nth-child(9) > .radio > .ng-pristine')
    }


    get indwelling_catheter_lumens_2(){
        cy.get(':nth-child(5) > [colspan="9"] > :nth-child(1) > .ng-pristine')
    }
    get indwelling_catheter_lumens_3(){
        cy.get(':nth-child(5) > [colspan="9"] > :nth-child(2) > .ng-pristine')
    }

    get indwelling_catheter_change_days(){
        cy.get(':nth-child(6) > [colspan="9"] > .global__txtbox')
    }
    get indwelling_catheter_change_done(){
        cy.get(':nth-child(6) > [colspan="9"] > .display-ib > .p-l-18 > .ng-pristine')
    }

    get indwelling_drainage_beside(){
        cy.get('[colspan="9"] > :nth-child(1) > .ng-scope')
    }
    get indwelling_drainage_leg(){
        cy.get('[colspan="9"] > :nth-child(2) > .ng-scope')
    }

    get indwelling_irrigation_frequency_days(){
        cy.get(':nth-child(8) > [colspan="9"] > .global__txtbox')
    }
    get indwelling_irrigation_frequency_asneeded(){
        cy.get(':nth-child(8) > [colspan="9"] > .radio > .ng-pristine')
    }
    get indwelling_irrigation_frequency_done(){
        cy.get(':nth-child(8) > [colspan="9"] > .display-ib > .p-l-18 > .ng-pristine')
    }

    get indwelling_irrigation_solution_amount(){
        cy.get('[name="SOOELIMINATION0055"]')
    }
    get indwelling_irrigation_solution_ml(){
        cy.get('[name="SOOELIMINATION0056"]')
    }

    get indwelling_observation(){
        cy.get(':nth-child(10) > [colspan="10"] > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get indwelling_intervetion(){
        cy.get(':nth-child(11) > [colspan="10"] > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
        
    get special_intermittent(){
        cy.get(':nth-child(2) > :nth-child(1) > div > .radio > .ng-valid')
    }
    get intermittent_catheter_size_fr(){
       
    }
    get intermittent_catheter_size_14(){
        cy.get('tbody > :nth-child(2) > :nth-child(2) > :nth-child(3) > .ng-pristine')
    }
    get intermittent_catheter_size_16(){
        cy.get('.m-l-10 > .global__txtbox')
    }
    get intermittent_frequency_times(){
        cy.get('[name="SOOELIMINATION0153"]')
    }
    get intermittent_frequency_day(){
        cy.get(':nth-child(3) > :nth-child(2) > :nth-child(3) > .ng-pristine')
    }
    get intermittent_frequency_week(){
        cy.get(':nth-child(3) > :nth-child(2) > :nth-child(4) > .ng-pristine')
    }
    get intermittent_frequency_other(){
        cy.get('[name="SOOELIMINATION0183"]')
    }
    get intermittent_doneby_patient(){
        cy.get('[chk="data.SOOELIMINATION0207"] > .ng-pristine')
    }
    get intermittent_doneby_caregiver(){
        cy.get('[chk="data.SOOELIMINATION0208"] > .ng-pristine')
    }
    get intermittent_doneby_sn(){
        cy.get('[chk="data.SOOELIMINATION0209"] > .ng-pristine')
    }
    get intermittent_observation(){
        cy.get('[ng-if="!(subsetPreloader && tab.preloader) && data.SOOELIMINATION0218"] > tbody > :nth-child(5) > :nth-child(2) > .global__txtbox')
    }
    get intermittent_intervention(){
        cy.get(':nth-child(6) > :nth-child(2) > .global__txtbox')
    }


    get special_nephrostomy(){
        cy.get(':nth-child(3) > :nth-child(1) > div > .radio > .ng-valid')
    }
    get nephrostomy_dressing_days(){
        cy.get('[ng-if="!(subsetPreloader && tab.preloader) && data.SOOELIMINATION0219"] > :nth-child(1) > :nth-child(2) > :nth-child(2) > .global__txtbox')
    }
    get nephrostomy_dressing_asneeded(){
        cy.get('[ng-if="!(subsetPreloader && tab.preloader) && data.SOOELIMINATION0219"] > :nth-child(1) > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine')
    }
    get nephrostomy_dressing_done(){
        cy.get(':nth-child(2) > :nth-child(2) > .display-ib > .p-l-18 > .ng-pristine')
    }
    get nephrostomy_bag_days(){
        cy.get(':nth-child(3) > .b-t-n > .global__txtbox')
    }
    get nephrostomy_bag_asneeded(){
        cy.get(':nth-child(3) > .b-t-n > .radio > .ng-pristine')
    }
    get nephrostomy_bag_done(){
        cy.get('.b-t-n > .display-ib > .p-l-18 > .ng-pristine')
    }
    get nephrostomy_irrigation_frequency_days(){
        cy.get('[ng-if="!(subsetPreloader && tab.preloader) && data.SOOELIMINATION0219"] > :nth-child(1) > :nth-child(4) > :nth-child(2) > .global__txtbox')
    }
    get nephrostomy_irrigation_frequency_asneeded(){
        cy.get('[ng-if="!(subsetPreloader && tab.preloader) && data.SOOELIMINATION0219"] > :nth-child(1) > :nth-child(4) > :nth-child(2) > .radio > .ng-pristine')
    }
    get nephrostomy_irrigation_frequency_done(){
        cy.get(':nth-child(4) > :nth-child(2) > .display-ib > .p-l-18 > .ng-pristine')
    }
    get nephrostomy_irrigation_solution_amount(){
        cy.get('[name="SOOELIMINATION0178"]')
    }
    get nephrostomy_irrigation_solution_ml(){
        cy.get('[name="SOOELIMINATION0179"]')
    }
    get nephrostomy_observation(){
        cy.get(':nth-child(6) > [colspan="3"] > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get nephrostomy_intervention(){
        cy.get('.oasis__answer > .inner__table-wb > tbody > :nth-child(2) > .b-l-n > .global__txtbox')
    }



    get special_urostomy(){
        cy.get(':nth-child(1) > :nth-child(2) > div > .radio > .ng-valid')
    }
    get urostomy_days(){
        cy.get('[ng-if="!(subsetPreloader && tab.preloader) && data.SOOELIMINATION0220"] > :nth-child(1) > :nth-child(2) > td > .global__txtbox')
    }
    get urostomy__observation(){
        cy.get('.oasis__answer > .inner__table-wb > tbody > :nth-child(1) > .b-l-n > .global__txtbox')
    }
    get urostomy__intervention(){
        cy.get('.oasis__answer > .inner__table-wb > tbody > :nth-child(2) > .b-l-n > .global__txtbox')
    }

    get special_hemodialysis(){
        cy.get(':nth-child(2) > :nth-child(2) > div > .radio > .ng-valid')
    }
    get hemodialysis_AV_shunt(){
        
    }
    get hemodialysis_AV_location(){
        
    }
    get hemodialysis_AV_permacath(){
        
    }
    get hemodialysis_AV_other(){
        
    }
    get hemodialysis_bruit_yes(){
        
    }
    get hemodialysis_bruit_no(){
        
    }
    get hemodialysis_thrill_yes(){
        
    }
    get hemodialysis_thrill_no(){
        
    }
    get hemodialysis_dialysis_mon(){
        
    }
    get hemodialysis_dialysis_tue(){
        
    }
    get hemodialysis_dialysis_wed(){
        
    }
    get hemodialysis_dialysis_thu(){
        
    }
    get hemodialysis_dialysis_fri(){
        
    }
    get hemodialysis_dialysis_sat(){
        
    }
    get hemodialysis_dialysis_sun(){
        
    }

    get hemodialysis_center(){
        
    }
    get hemodialysis_phone(){
        
    }

    get special_peritoneal(){
        cy.get(':nth-child(3) > :nth-child(2) > div > .radio > .ng-valid')
    }

    get special_external(){
        cy.get(':nth-child(3) > div > .radio > .ng-untouched')
    }

    get special_other(){
        cy.get(':nth-child(2) > :nth-child(3) > :nth-child(2) > .fg-line > .global__txtbox')
    }

    get special_observation(){
        cy.get('.oasis__answer > .inner__table-wb > tbody > :nth-child(1) > .b-l-n > .global__txtbox')
    }
    get special_intervention(){
        cy.get('.oasis__answer > .inner__table-wb > tbody > :nth-child(2) > .b-l-n > .global__txtbox')
    }



 
}