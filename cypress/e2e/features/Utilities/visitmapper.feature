Feature: Visit Mapper

    Scenario: Visit Mapper

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Utilities'
        Then a selected main nav should be expand

        When I click the sub nav 'Visit Mapper'
        Then a selected sub nav should be display 

        #backward, forward
        #1, 2, 3 ...
        When i navigate the date 'backward' 5 times
        Then it should be navigate no of times


