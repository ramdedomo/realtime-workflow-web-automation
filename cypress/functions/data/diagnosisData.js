export default {
    description: [
        //first is primary diagnosis
        {
            diagnosis: "F40.220",
            level: 0
        },
        //other diagnosis
        {
            diagnosis: "F40.230",
            level: 0
        },
        {
            diagnosis: "F40.231",
            level: 0
        },
        {
            diagnosis: "F40.232",
            level: 0
        },
        {
            diagnosis: "F40.233",
            level: 0
        },
        {
            diagnosis: "F40.242",
            level: 0
        },
    ],
    M1028: {
        //multiple select
        M1028_1: true,
        M1028_2: false,
        M1028_3: false,
    },
    M1033: {
        //multiple select
        M1033_1: true,
        M1033_2: true,
        M1033_3: true,
        M1033_4: true,
        M1033_5: true,
        M1033_6: false,
        M1033_7: false,
        M1033_8: false,
        M1033_9: {
            M1033_9: true,
        },
        M1033_10: false,
    },
    PHRL: {
        //single select
        PHRL_high: true,
        PHRL_moderate: true,
        PHRL_low: true,
    },
    sensory: {
        sensory: true,
        sensorydata:{
            sensory_yes_vision: true,
            sensory_yes_hearing: true,
            sensory_yes_other: "Test Other"
        }
    },
    integumentary:{
        integumentary: true,
        integumentarydata:{
            integumentary_yes_lesions: true,
            integumentary_yes_wound: true,
            integumentary_yes_other: "Test Other"
        }
    },
    endocrine:{
        endocrine: true,
        endocrinedata:{
            endocrine_yes_diabetes: true,
            endocrine_yes_thyroid: true,
            endocrine_yes_other: "Test Other"
        }
    },
    respiratory:{
        respiratory: true,
        respiratorydata:{
            respiratory_yes_COPD: true,
            respiratory_yes_asthma: true,
            respiratory_yes_pnuemonia: true,
            respiratory_yes_emphysema: true,
            respiratory_yes_other: "Test Other"
        }
    },
    cardiovascular:{
        cardiovascular: true,
        cardiovasculardata:{
            cardiovascular_yes_CHF: true,
            cardiovascular_yes_HTN: true,
            cardiovascular_yes_arrhythmia: true,
            cardiovascular_yes_cardiomyopathy: true,
            cardiovascular_yes_CAD: true,
            cardiovascular_yes_PVD: true,
            cardiovascular_yes_other: "Test Other"
        }
    },
    gastrointestinal:{
        gastrointestinal: true,
        gastrointestinaldata:{
            gastrointestinal_yes_constipation: true,
            gastrointestinal_yes_crohn: true,
            gastrointestinal_yes_fecal: true,
            gastrointestinal_yes_diverticulitis: true,
            gastrointestinal_yes_liver: true,
            gastrointestinal_yes_IBS: true,
            gastrointestinal_yes_other: "Test Other"
        }
    },
    genitourinary:{
        genitourinary: true,
        genitourinarydata:{
            genitourinary_yes_urinaryincontinence: true,
            genitourinary_yes_chronic: true,
            genitourinary_yes_urinaryretention: true,
            genitourinary_yes_dialysis: true,
            genitourinary_yes_recent: true,
            genitourinary_yes_BPH: true,
            genitourinary_yes_other: "Test Other"
        }
    },
    neurological:{
        neurological: true,
        neurologicaldata:{
            neurological_yes_headaches: true,
            neurological_yes_dizziness: true,
            neurological_yes_parkinson: true,
            neurological_yes_CVA: true,
            neurological_yes_TIA: true,
            neurological_yes_alzheimer: true,
            neurological_yes_multiple: true,
            neurological_yes_seizure: true,
            neurological_yes_paralysis: true,
            neurological_yes_other: "Test Other"
        }
    },
    musculoskeletal:{
        musculoskeletal: true,
        musculoskeletaldata:{
            musculoskeletal_yes_rheumatoid: true,
            musculoskeletal_yes_osteoarthritis: true,
            musculoskeletal_yes_fall: true,
            musculoskeletal_yes_gait: true,
            musculoskeletal_yes_fractures: true,
            musculoskeletal_yes_joint: true,
            musculoskeletal_yes_spinal: true,
            musculoskeletal_yes_muscular: true,
            musculoskeletal_yes_other: "Test Other"
        }
    },
    circulatory:{
        circulatory: true,
        circulatorydata:{
            circulatory_yes_anemia: true,
            circulatory_yes_abnormal: true,
            circulatory_yes_other: "Test Other"
        }
    },
    other:{
        other: true,
        otherdata:{
            other_yes_cancer: true,
            other_yes_infectious: true,
            other_yes_tobacco: true,
            other_yes_substance: true,
            other_yes_trauma: true,
            other_yes_mental: true,
            other_yes_other: "Test Other"
        }
    },
    surgical: {
        surgical: true,
        surgical_yes_surgeries: "Test Surgiries"
    },
    hospitalization: {
        hospitalizations_none: false,
        hospitalizations: "Test Surgiries"
    },
    exam: [
        {
            name: "Test Exam 1",
            date: "05/05/2023",
            result: "Passed"
        },
        {
            name: "Test Exam 2",
            date: "05/06/2023",
            result: "Passed"
        },
        {
            name: "Test Exam 3",
            date: "05/07/2023",
            result: "Passed"
        },
    ],
    //no allergies?
    allergies: false,
    datecontacted: "05/06/2023",
    datevisit: "05/07/2023",
    datereason: "Test Reason",
    influenza: {
        influenza: true,
        influenza_from: {
            influenza_yes_agency: true,
            influenza_yes_pcp: true,
            influenza_yes_facility: true,
            influenza_yes_pharmacy: true,
        },
        reason: "Test Reason",
        date: "05/06/2021",
        offered: true,
        offered_from: {
            influenza_offered_agency: true,
            influenza_offered_clinic: true,
            influenza_offered_pharmacy: true,
        }
    },
    pneumonia: {
        pneumonia: true,
        pneumonia_from: {
            pneumonia_yes_agency: true,
            pneumonia_yes_pcp: true,
            pneumonia_yes_facility: true,
            pneumonia_yes_pharmacy: true,
        },
        reason: "Test Reason",
        date: "10/06/2021",
        offered: true,
        offered_from: {
            pneumonia_offered_agency: true,
            pneumonia_offered_clinic: true,
            pneumonia_offered_pharmacy: true,
        }
    }
}