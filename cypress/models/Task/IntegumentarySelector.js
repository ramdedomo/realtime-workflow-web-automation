export default class IntegumentarySelector{
    get integumentarywnl(){
        return cy.get('.checkbox > .p-r-5')
    }
    
    get integumentary_skin_pink(){
        return cy.get(':nth-child(2) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(1) > [style="width: 17%;"] > div > .radio > .ng-valid')
    }
    get integumentary_skin_flushed(){
        return cy.get(':nth-child(2) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(2) > :nth-child(1) > .ng-isolate-scope > .radio > .ng-valid')
    }
    get integumentary_skin_redness(){
        return cy.get(':nth-child(3) > [opt="data.SOOINT0005"] > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get integumentary_skin_pale(){
        return cy.get('.p-t-5 > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get integumentary_skin_cyanotic(){
        return cy.get(':nth-child(2) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(2) > :nth-child(2) > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get integumentary_skin_other(){
        return cy.get('#SOOINT0060')
    }


    get integumentary_temp_warm(){
        return cy.get(':nth-child(3) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(1) > [style="width: 17%;"] > div > .radio > .ng-pristine')
    }
    get integumentary_temp_cool(){
        return cy.get(':nth-child(3) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(2) > :nth-child(1) > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get integumentary_temp_hot(){
        return cy.get(':nth-child(3) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(1) > :nth-child(2) > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get integumentary_temp_cold(){
        return cy.get(':nth-child(3) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(2) > :nth-child(2) > .ng-isolate-scope > .radio > .ng-pristine')
    }

    get integumentary_moisture_dry(){
        return cy.get('[style="width: 17%;"] > :nth-child(1) > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get integumentary_moisture_dehydrated(){
        return cy.get(':nth-child(4) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(1) > :nth-child(2) > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get integumentary_moisture_moist(){
        return cy.get(':nth-child(4) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(2) > :nth-child(1) > div > .radio > .ng-pristine')
    }
    get integumentary_moisture_diaphoretic(){
        return cy.get(':nth-child(4) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(2) > :nth-child(2) > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get integumentary_moisture_clammy(){
        return cy.get(':nth-child(4) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(3) > .p-5 > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get integumentary_moisture_other(){
        return cy.get('#SOOINT0074')
    }


    get integumentary_turgor_normal(){
        return cy.get(':nth-child(5) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(1) > [style="width: 17%;"] > div > .radio > .ng-pristine')
    }
    get integumentary_turgor_fair(){
        return cy.get(':nth-child(5) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(2) > .p-5 > .ng-isolate-scope > .radio > .ng-pristine')
    }
    get integumentary_turgor_poor(){
        return cy.get(':nth-child(5) > .oasis__answer > .inner__table-wb > :nth-child(1) > :nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > :nth-child(1) > :nth-child(2) > .ng-isolate-scope > .radio > .ng-pristine')
    }

    get integrity_skinintact(){
        return cy.get(':nth-child(2) > .m-0 > .checkbox > .ng-valid')
    }
    get integrity_lesions(){
        return cy.get(':nth-child(3) > .m-0 > .checkbox > .ng-valid')
    }
    get integrity_lesions_add(){
        return cy.get(':nth-child(3) > .m-0 > .btn')
    }

    get integrity_wound(){
        return cy.get(':nth-child(4) > .m-0 > .checkbox > .ng-pristine')
    }
    get integrity_wound_add(){
        return cy.get(':nth-child(4) > .m-0 > .btn > .zmdi')
    }
    get integrity_other(){
        return cy.get('#SOOINT0079')
    }

     get observation(){
        return cy.get('#SOOINT0078')
     }
     get intervention(){
        return cy.get('#SOOINT0080')
     }

     get perception(){
        return cy.get(':nth-child(3) > .p-5.text-center > .text-center > .oasis__code_input-xs')
     }
     get moisture(){
        return cy.get(':nth-child(4) > .p-5.text-center > .text-center > .oasis__code_input-xs')
     }
     get activity(){
        return cy.get(':nth-child(5) > .p-5.text-center > .text-center > .oasis__code_input-xs')
     }
     get mobility(){
        return cy.get(':nth-child(6) > .p-5.text-center > .text-center > .oasis__code_input-xs')
     }
     get nutrition(){
        return cy.get(':nth-child(7) > .p-5.text-center > .text-center > .oasis__code_input-xs')
     }
     get friction(){
        return cy.get(':nth-child(8) > .p-5.text-center > .text-center > .oasis__code_input-xs')
     }

     get M1306(){
        return cy.get('[style=""] > :nth-child(1) > .molocked > .oasis__enter-code > .form-group > #code')
     }

     get M1311_A1(){
        return cy.get('#M1311_NBR_PRSULC_STG2_A1')
     }
     get M1311_B1(){
        return cy.get('#M1311_NBR_PRSULC_STG3_B1')
     }
     get M1311_C1(){
        return cy.get('#M1311_NBR_PRSULC_STG4_C1')
     }
     get M1311_D1(){
        return cy.get('#M1311_NSTG_DRSG_D1')
     }
     get M1311_E1(){
        return cy.get('#M1311_NSTG_CVRG_E1')
     }
     get M1311_F1(){
        return cy.get('#M1311_NSTG_DEEP_TSUE_F1')
     }


     get M1322(){
        return cy.get(':nth-child(3) > :nth-child(1) > .molocked > .oasis__enter-code > .form-group > #code')
     }
     get M1324(){
        return cy.get(':nth-child(4) > :nth-child(1) > .molocked > .oasis__enter-code > .form-group > #code')
     }
     get M1330(){
        return cy.get(':nth-child(5) > :nth-child(1) > .molocked > .oasis__enter-code > .form-group > #code')
     }
     get M1332(){
        return cy.get(':nth-child(6) > :nth-child(1) > .molocked > .oasis__enter-code > .form-group > #code')
     }
     get M1334(){
        return cy.get(':nth-child(7) > :nth-child(1) > .molocked > .oasis__enter-code > .form-group > #code')
     }
     get M1340(){
        return cy.get(':nth-child(8) > :nth-child(1) > .molocked > .oasis__enter-code > .form-group > #code')
     }
     get M1342(){
        return cy.get(':nth-child(9) > :nth-child(1) > .molocked > .oasis__enter-code > .form-group > #code')
     }


}