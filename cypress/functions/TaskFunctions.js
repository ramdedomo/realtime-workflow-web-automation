import AdministrativeSelector from "../models/Task/AdministrativeSelector"
import DiagnosisSelector from "../models/Task/DiagnosisSelector"
import VitalSignsSelector from "../models/Task/VitalSignsSelector"
import CognitiveSelector from "../models/Task/CognitiveSelector"
import IntegumentarySelector from "../models/Task/IntegumentarySelector"
import RespiratorySelector from "../models/Task/RespiratorySelector"

function TaskAdministrative(administrativeData, issave){
    const administrative = new AdministrativeSelector()
    
    cy.get(':nth-child(1) > .oasis-section-tabs').click()
    cy.wait(2000)

    const A1005 = administrativeData.A1005
    for (const property in A1005) {
        if (A1005.hasOwnProperty(property)) {
            if(A1005[property]){
                administrative[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    const A1010 = administrativeData.A1010
    for (const property in A1010) {
        if (A1010.hasOwnProperty(property)) {
            if(A1010[property]){
                administrative[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    const M0150 = administrativeData.M0150
    for (const property in M0150) {
        if (M0150.hasOwnProperty(property)) {
            if(M0150[property]){
                administrative[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    //language
    administrative.A1110.type(administrativeData.A1110.A1110).should('have.value', administrativeData.A1110.A1110)
    //need interpreter?
    const interpreter = administrativeData.A1110.options
    for (const property in interpreter) {
        if (interpreter.hasOwnProperty(property)) {
            if(interpreter[property]){
                administrative[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    //assesment completed
    administrative.M0090.type(administrativeData.M0090).should('have.value', administrativeData.M0090)

    //date of physician SOC
    administrativeData.M0102.M0102_na 
    ? administrative.M0102_na.click() 
    : administrative.M0102.type(administrativeData.M0102.M0102).should('have.value', administrativeData.M0102.M0102)

    //referral date
    administrative.M0104.type(administrativeData.M0104).should('have.value', administrativeData.M0104)

    //episode timing
    const M0110 = administrativeData.M0110
    for (const property in M0110) {
        if (M0110.hasOwnProperty(property)) {
            if(M0110[property]){
                administrative[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    //transportation
    const A1250 = administrativeData.A1250
    for (const property in A1250) {
        if (A1250.hasOwnProperty(property)) {
            if(A1250[property]){
                administrative[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    //past 14 days
    const M1000 = administrativeData.M1000
    for (const property in M1000) {
        if (M1000.hasOwnProperty(property)) {
            if(M1000[property]){
                administrative[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    //date of physician SOC
    administrativeData.M1005.M1005_uk 
    ? administrative.M1005_uk.click().should('have.class','ng-not-empty')
    : administrative.M1005.type(administrativeData.M1005.M1005).should('have.value', administrativeData.M1005.M1005)

    if(issave){
        cy.get('.btn__success').click()
    }else{
        cy.get('.btn__warning').click()
        cy.wait(1000)
        cy.get('.swal2-confirm').click()
    }
}

function TaskDiagnosis(diagnosisData, issave){
    const diagnosis = new DiagnosisSelector()
    
    cy.get(':nth-child(2) > .oasis-section-tabs').click()
    cy.wait(2000)

    const description = diagnosisData.description
    for (let index = 1; index <= description.length; index++) {
        cy.xpath(`/html/body/section/section/data/section/div[2]/div/div/div/div/div/div/div/form/div/fieldset/div[5]/fieldset/div/fieldset/table[1]/tbody/tr[2]/td/table/tbody[2]/tr[${index}]/td/table/tbody/tr/td[3]/table/tbody/tr/td[2]/oasisv5-icd-opt/div/div[1]/input`).type(`${description[index-1].diagnosis}`, {delay: 500}).should('have.value', description[index-1].diagnosis)
        cy.xpath(`/html/body/section/section/data/section/div[2]/div/div/div/div/div/div/div/form/div/fieldset/div[5]/fieldset/div/fieldset/table[1]/tbody/tr[2]/td/table/tbody[2]/tr[${index}]/td/table/tbody/tr/td[3]/table/tbody/tr/td[3]/fieldset/div/label[${description[index-1].level+1}]/input`).click().should('have.class','ng-not-empty');
    }

    const M1028 = diagnosisData.M1028
    for (const property in M1028) {
        if (M1028.hasOwnProperty(property)) {
            if(M1028[property]){
                diagnosis[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    const M1033 = diagnosisData.M1033
    for (const property in M1033) {
        if (M1033.hasOwnProperty(property)) {
            if(typeof M1033[property] === 'object'){
                if(M1033[property][property]){
                    diagnosis[property].click().should('have.class','ng-not-empty');
                    diagnosis[`${property}_other`].type("Test").should('have.value', "Test")
                }
            }else{
                if(M1033[property]){
                    diagnosis[property].click().should('have.class','ng-not-empty');
                } 
            }
        }
    }

    const sensory = diagnosisData.sensory
    if(sensory.sensory){
        diagnosis.sensory_yes.click().should('have.class','ng-not-empty');

        const sensorydata = sensory.sensorydata
        for (const property in sensorydata) {
            if (sensorydata.hasOwnProperty(property)) {
                if(typeof sensorydata[property] == "string"){
                    diagnosis[property].type(sensorydata[property]).should('have.value', sensorydata[property])
                }else{
                    if(sensorydata[property]){
                        diagnosis[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }else{
        diagnosis.sensory_no.click().should('have.class','ng-not-empty');
    }

    const integumentary = diagnosisData.integumentary
    if(integumentary.integumentary){
        diagnosis.integumentary_yes.click().should('have.class','ng-not-empty');

        const integumentarydata = integumentary.integumentarydata
        for (const property in integumentarydata) {
            if (integumentarydata.hasOwnProperty(property)) {
                if(typeof integumentarydata[property] == "string"){
                    diagnosis[property].type(integumentarydata[property]).should('have.value', integumentarydata[property])
                }else{
                    if(integumentarydata[property]){
                        diagnosis[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }else{
        diagnosis.integumentary_no.click().should('have.class','ng-not-empty');
    }

    const endocrine = diagnosisData.endocrine
    if(endocrine.endocrine){
        diagnosis.endocrine_yes.click().should('have.class','ng-not-empty');

        const endocrinedata = endocrine.endocrinedata
        for (const property in endocrinedata) {
            if (endocrinedata.hasOwnProperty(property)) {
                if(typeof endocrinedata[property] == "string"){
                    diagnosis[property].type(endocrinedata[property]).should('have.value', endocrinedata[property])
                }else{
                    if(endocrinedata[property]){
                        diagnosis[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }else{
        diagnosis.endocrine_no.click().should('have.class','ng-not-empty');
    }
    
//    .should('have.class', 'ng-untouched')
//    .should('have.class', 'ng-pristine')
//    .should('have.class', 'ng-valid');

    const respiratory = diagnosisData.respiratory
    if(respiratory.respiratory){
        diagnosis.respiratory_yes.click().should('have.class','ng-not-empty');

        const respiratorydata = respiratory.respiratorydata
        for (const property in respiratorydata) {
            if (respiratorydata.hasOwnProperty(property)) {
                if(typeof respiratorydata[property] == "string"){
                    diagnosis[property].type(respiratorydata[property]).should('have.value', respiratorydata[property])
                }else{
                    if(respiratorydata[property]){
                        diagnosis[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }else{
        diagnosis.respiratory_no.click().should('have.class','ng-not-empty');
    }

    const cardiovascular = diagnosisData.cardiovascular
    if(cardiovascular.cardiovascular){
        diagnosis.cardiovascular_yes.click().should('have.class','ng-not-empty');

        const cardiovasculardata = cardiovascular.cardiovasculardata
        for (const property in cardiovasculardata) {
            if (cardiovasculardata.hasOwnProperty(property)) {
                if(typeof cardiovasculardata[property] == "string"){
                    diagnosis[property].type(cardiovasculardata[property]).should('have.value', cardiovasculardata[property])
                }else{
                    if(cardiovasculardata[property]){
                        diagnosis[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }else{
        diagnosis.cardiovascular_no.click().should('have.class','ng-not-empty');
    }

    const gastrointestinal = diagnosisData.gastrointestinal
    if(gastrointestinal.gastrointestinal){
        diagnosis.gastrointestinal_yes.click().should('have.class','ng-not-empty');

        const gastrointestinaldata = gastrointestinal.gastrointestinaldata
        for (const property in gastrointestinaldata) {
            if (gastrointestinaldata.hasOwnProperty(property)) {
                if(typeof gastrointestinaldata[property] == "string"){
                    diagnosis[property].type(gastrointestinaldata[property]).should('have.value', gastrointestinaldata[property])
                }else{
                    if(gastrointestinaldata[property]){
                        diagnosis[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }else{
        diagnosis.gastrointestinal_no.click().should('have.class','ng-not-empty');
    }

    const genitourinary = diagnosisData.genitourinary
    if(genitourinary.genitourinary){
        diagnosis.genitourinary_yes.click().should('have.class','ng-not-empty');

        const genitourinarydata = genitourinary.genitourinarydata
        for (const property in genitourinarydata) {
            if (genitourinarydata.hasOwnProperty(property)) {
                if(typeof genitourinarydata[property] == "string"){
                    diagnosis[property].type(genitourinarydata[property]).should('have.value', genitourinarydata[property])
                }else{
                    if(genitourinarydata[property]){
                        diagnosis[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }else{
        diagnosis.genitourinary_no.click().should('have.class','ng-not-empty');
    }

    const neurological = diagnosisData.neurological
    if(neurological.neurological){
        diagnosis.neurological_yes.click().should('have.class','ng-not-empty');

        const neurologicaldata = neurological.neurologicaldata
        for (const property in neurologicaldata) {
            if (neurologicaldata.hasOwnProperty(property)) {
                if(typeof neurologicaldata[property] == "string"){
                    diagnosis[property].type(neurologicaldata[property]).should('have.value', neurologicaldata[property])
                }else{
                    if(neurologicaldata[property]){
                        diagnosis[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }else{
        diagnosis.neurological_no.click()
    }

    const musculoskeletal = diagnosisData.musculoskeletal
    if(musculoskeletal.musculoskeletal){
        diagnosis.musculoskeletal_yes.click().should('have.class','ng-not-empty');

        const musculoskeletaldata = musculoskeletal.musculoskeletaldata
        for (const property in musculoskeletaldata) {
            if (musculoskeletaldata.hasOwnProperty(property)) {
                if(typeof musculoskeletaldata[property] == "string"){
                    diagnosis[property].type(musculoskeletaldata[property]).should('have.value', musculoskeletaldata[property])
                }else{
                    if(musculoskeletaldata[property]){
                        diagnosis[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }else{
        diagnosis.musculoskeletal_no.click().should('have.class','ng-not-empty');
    }

    const circulatory = diagnosisData.circulatory
    if(circulatory.circulatory){
        diagnosis.circulatory_yes.click().should('have.class','ng-not-empty');

        const circulatorydata = circulatory.circulatorydata
        for (const property in circulatorydata) {
            if (circulatorydata.hasOwnProperty(property)) {
                if(typeof circulatorydata[property] == "string"){
                    diagnosis[property].type(circulatorydata[property]).should('have.value', circulatorydata[property])
                }else{
                    if(circulatorydata[property]){
                        diagnosis[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }else{
        diagnosis.circulatory_no.click().should('have.class','ng-not-empty');
    }

    const other = diagnosisData.other
    if(other.other){
        diagnosis.other_yes.click().should('have.class','ng-not-empty');

        const otherdata = circulatory.otherdata
        for (const property in otherdata) {
            if (otherdata.hasOwnProperty(property)) {
                if(typeof otherdata[property] == "string"){
                    diagnosis[property].type(otherdata[property]).should('have.value', otherdata[property])
                }else{
                    if(otherdata[property]){
                        diagnosis[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }else{
        diagnosis.other_no.click().should('have.class','ng-not-empty');
    }

    const surgical = diagnosisData.surgical
    if(surgical.surgical){
        diagnosis.surgical_yes.click().should('have.class','ng-not-empty');
        diagnosis.surgical_yes_surgeries.type(surgical.surgical_yes_surgeries).should('have.value', surgical.surgical_yes_surgeries)
    }else{
        diagnosis.surgical_no.click().should('have.class','ng-not-empty');
    }

    const hospitalization = diagnosisData.hospitalization
    if(hospitalization.hospitalizations_none){
        diagnosis.hospitalizations_none.click().should('have.class','ng-not-empty');
    }else{
        diagnosis.hospitalizations.type(hospitalization.hospitalizations).should('have.value', hospitalization.hospitalizations)
    }

    const exam = diagnosisData.exam
    for (let index = 0; index < exam.length-1; index++) {
        diagnosis.examAdd.click()
    }
    for (let index = 1; index <= exam.length; index++) {
        cy.get(`:nth-child(${index+2}) > td > [style="width: 36%; margin-top: 2px;"] > .global__txtbox`).type(exam[index-1].name).should('have.value', exam[index-1].name)
        cy.get(`:nth-child(${index+2}) > td > [style="width: 10%;"] > .global__txtbox`).type(exam[index-1].date).should('have.value', exam[index-1].date)
        cy.get(`:nth-child(${index+2}) > td > [style="width: 33%;"] > .global__txtbox`).type(exam[index-1].result).should('have.value',exam[index-1].result)
    }

    diagnosisData.allergies && diagnosis.allergies_none.click().should('have.class','ng-not-empty');
    diagnosis.datecontacted.type(diagnosisData.datecontacted).should('have.value', diagnosisData.datecontacted)
    diagnosis.datevisit.type(diagnosisData.datevisit).should('have.value', diagnosisData.datevisit)
    diagnosis.datereason.type(diagnosisData.datereason).should('have.value', diagnosisData.datereason)

    const influenza = diagnosisData.influenza
    const influenza_from = influenza.influenza_from

    //recieved influenza?
    if(influenza.influenza){
        diagnosis.influenza_yes.click().should('have.class','ng-not-empty');
        diagnosis.influenza_yes_date.type(influenza.date).should('have.value', influenza.date)

        for (const property in influenza_from) {
            if (influenza_from.hasOwnProperty(property)) {
                if(influenza_from[property]){
                    diagnosis[property].click().should('have.class','ng-not-empty');
                }   
            }
        }
    }else{
        diagnosis.influenza_no.click().should('have.class','ng-not-empty');
        diagnosis.influenza_no_reason.type(influenza.reason).should('have.value', influenza.reason)
    }
    //offered influenza?
    if(influenza.offered){
        diagnosis.influenza_offered_agree.click().should('have.class','ng-not-empty');
        
        const offered_from = influenza.offered_from
        for (const property in offered_from) {
            if (offered_from.hasOwnProperty(property)) {
                if(offered_from[property]){
                    diagnosis[property].click().should('have.class','ng-not-empty');
                }   
            }
        }

    }else{
        diagnosis.influenza_offered_disagree.click().should('have.class','ng-not-empty');
    }

 
    const pneumonia = diagnosisData.pneumonia
    const pneumonia_from = pneumonia.pneumonia_from

   //recieved pneumonia?
    if(pneumonia.pneumonia){
        diagnosis.pneumonia_yes.click().should('have.class','ng-not-empty');
        diagnosis.pneumonia_yes_date.type(pneumonia.date).should('have.value', pneumonia.date)

        for (const property in pneumonia_from) {
            if (pneumonia_from.hasOwnProperty(property)) {
                if(pneumonia_from[property]){
                    diagnosis[property].click().should('have.class','ng-not-empty');
                }   
            }
        }

    }else{
        diagnosis.pneumonia_no.click().should('have.class','ng-not-empty');
        diagnosis.pneumonia_no_reason.type(pneumonia.reason).should('have.value', pneumonia.reason)
    }
    
    //offered pneumonia?
    if(pneumonia.offered){
        diagnosis.pneumonia_offered_agree.click().should('have.class','ng-not-empty');
        
        const offered_from = pneumonia.offered_from
        for (const property in offered_from) {
            if (offered_from.hasOwnProperty(property)) {
                if(offered_from[property]){
                    diagnosis[property].click().should('have.class','ng-not-empty');
                }   
            }
        }

    }else{
        diagnosis.pneumonia_offered_disagree.click().should('have.class','ng-not-empty');
    }

    if(issave){
        cy.get('.btn__success').click()
    }else{
        cy.get('.btn__warning').click()
        cy.wait(1000)
        cy.get('.swal2-confirm').click()
    }
}

function TaskVital(vitalData, issave){
    const vital = new VitalSignsSelector()
    
    cy.get(':nth-child(3) > .oasis-section-tabs').click()
    cy.wait(2000)

    const temperature = vitalData.temperature
    vital.temperature.clear()
    vital.temperature.type(temperature.fahrenheit).should('have.value', temperature.fahrenheit)
    const temperature_location = temperature.location
    for (const property in temperature_location) {
        if (temperature_location.hasOwnProperty(property)) {
            if(temperature_location[property]){
                vital[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    const pulse = vitalData.pulse
    vital.pulse.clear()
    vital.pulse.type(pulse.beatspermin).should('have.value', pulse.beatspermin)
    const vital_method = vital.method
    for (const property in vital_method) {
        if (vital_method.hasOwnProperty(property)) {
            if(vital_method[property]){
                vital[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    vital.respiration.clear()
    vital.respiration.type(vitalData.respiration).should('have.value', vitalData.respiration)

    const armbpleft = vitalData.armbpleft
    vital.armbpleft_systo.clear()
    vital.armbpleft_diasto.clear()
    vital.armbpleft_systo.type(armbpleft.systo).should('have.value', armbpleft.systo)
    vital.armbpleft_diasto.type(armbpleft.diasto).should('have.value', armbpleft.diasto)
    const armbpleft_position = armbpleft.position
    for (const property in armbpleft_position) {
        if (armbpleft_position.hasOwnProperty(property)) {
            if(armbpleft_position[property]){
                vital[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    const armbpright = vitalData.armbpright
    vital.armbpright_systo.clear()
    vital.armbpright_diasto.clear()
    vital.armbpright_systo.type(armbpright.systo).should('have.value', armbpright.systo)
    vital.armbpright_diasto.type(armbpright.diasto).should('have.value', armbpright.diasto)
    const armbpright_position = armbpright.position
    for (const property in armbpright_position) {
        if (armbpright_position.hasOwnProperty(property)) {
            if(armbpright_position[property]){
                vital[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    const legbpleft = vitalData.legbpleft
    vital.legbpleft_systo.clear()
    vital.legbpleft_diasto.clear()
    vital.legbpleft_systo.type(legbpleft.systo).should('have.value', legbpleft.systo)
    vital.legbpleft_diasto.type(legbpleft.diasto).should('have.value', legbpleft.diasto)
    const legbpleft_position = legbpleft.position
    for (const property in legbpleft_position) {
        if (legbpleft_position.hasOwnProperty(property)) {
            if(legbpleft_position[property]){
                vital[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    const legbpright = vitalData.legbpright
    vital.legbpright_systo.clear()
    vital.legbpright_diasto.clear()
    vital.legbpright_systo.type(legbpright.systo).should('have.value', legbpright.systo)
    vital.legbpright_diasto.type(legbpright.diasto).should('have.value',legbpright.diasto)
    const legbpright_position = legbpright.position
    for (const property in legbpright_position) {
        if (legbpright_position.hasOwnProperty(property)) {
            if(legbpright_position[property]){
                vital[property].click().should('have.class','ng-not-empty');
            }   
        }
    }
    
    vital.o2saturation.clear()
    vital.o2saturation.type(vitalData.o2saturation).should('have.value',vitalData.o2saturation)

    vital.o2saturation_.clear()
    vital.o2saturation_.type(vitalData.o2saturation_).should('have.value',vitalData.o2saturation_)

    vital.o2saturation_at.clear()
    vital.o2saturation_at.type(vitalData.o2saturation_at).should('have.value',vitalData.o2saturation_at)
    
    // const bloodsugar = vitalData.bloodsugar
    // vital.bloodsugar.clear()
    // vital.bloodsugar.type(bloodsugar.bloodsugar)
    // const bloodsugar_option = bloodsugar.option
    // for (const property in bloodsugar_option) {
    //     if (bloodsugar_option.hasOwnProperty(property)) {
    //         if(bloodsugar_option[property]){
    //             vital[property].click()
    //         }   
    //     }
    // }

    const weight = vitalData.weight
    vital.weight.clear()
    vital.weight.type(weight.weight).should('have.value', weight.weight)
    const weight_option = weight.option
    for (const property in weight_option) {
        if (weight_option.hasOwnProperty(property)) {
            if(weight_option[property]){
                vital[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    const height = vitalData.height
    vital.height.clear()
    vital.height.type(height.height).should('have.value', height.height)
    const height_option = height.option
    for (const property in height_option) {
        if (height_option.hasOwnProperty(property)) {
            if(height_option[property]){
                vital[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    const vitalsbeyond = vitalData.vitalsbeyond
    // vital.vitalsbeyond.clear()
    // vital.vitalsbeyond.type(vitalsbeyond.vitalsbeyond).should('have.value',vitalsbeyond.vitalsbeyond)
    const vitalsbeyond_notified = vitalsbeyond.notified
    for (const property in vitalsbeyond_notified) {
        if (vitalsbeyond_notified.hasOwnProperty(property)) {
            if(vitalsbeyond_notified[property]){
                vital[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    const evidence = vitalData.evidence
    if(evidence.evidence){
        vital.evidence_yes.click().should('have.class','ng-not-empty');
        vital.evidence_yes_decribe.type(evidence.evidence_yes_decribe).should('have.value', evidence.evidence_yes_decribe)
    }else{
        vital.evidence_no.click().should('have.class','ng-not-empty');
    }

    const evidence_notified = evidence.notified
    for (const property in evidence_notified) {
        if (evidence_notified.hasOwnProperty(property)) {
            if(evidence_notified[property]){
                vital[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    const newchange = vitalData.newchange
    if(newchange.newchange){
        vital.newchange_yes.click().should('have.class','ng-not-empty');
        vital.newchange_yes_decribe.type(newchange.newchange_yes_decribe).should('have.value', newchange.newchange_yes_decribe)
    }else{
        vital.newchange_no.click().should('have.class','ng-not-empty');
    }

    vital.vitalsigns_observation.type(vitalData.vitalsigns_observation).should('have.value', vitalData.vitalsigns_observation)
    vital.vitalsigns_intervention.type(vitalData.vitalsigns_intervention).should('have.value', vitalData.vitalsigns_intervention)

    const vsparameters = vitalData.vsparameters
    for (const property in vsparameters) {
        if (vsparameters.hasOwnProperty(property)) {
            if(vsparameters[property]){
                vital[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    vitalData.J0510 && vital.J0510.clear()
    vitalData.J0510 && vital.J0510.type(vitalData.J0510).should('have.value', vitalData.J0510)
    vitalData.J052 && vital.J0520.clear()
    vitalData.J052 && vital.J0520.type(vitalData.J0520).should('have.value', vitalData.J0520)
    vitalData.J0530 && vital.J0530.clear()
    vitalData.J0530 && vital.J0530.type(vitalData.J0530).should('have.value', vitalData.J0530)
    vitalData.B0200 && vital.B0200.clear()
    vitalData.B0200 && vital.B0200.type(vitalData.B0200).should('have.value', vitalData.B0200)
    vitalData.B01000 && vital.B01000.clear()
    vitalData.B01000 && vital.B01000.type(vitalData.B01000).should('have.value', vitalData.B01000)
    vitalData.B1300 && vital.B1300.clear()
    vitalData.B1300 && vital.B1300.type(vitalData.B1300).should('have.value', vitalData.B1300)

    const eyes = vitalData.eyes
    if(eyes.wnl){
        vital.eyes_wnl.click().should('have.class','ng-not-empty');
    }else{
        if(eyes.reading){
            vital.eyes_reading.click().should('have.class','ng-not-empty');
        }

        const eyes_option = eyes.option
        for (const property in eyes_option) {
            if (eyes_option.hasOwnProperty(property)) {
                if(typeof eyes_option[property] === 'object'){
                        const a = eyes_option[property];
                        for (const property in a) {
                            if (a.hasOwnProperty(property)) {
                                if(typeof a[property] === 'string'){
                                    vital[property].clear();
                                    vital[property].type("Test").should('have.value', "Test")
                                }else{
                                    if(vital[property]){
                                        vital[property].click().should('have.class','ng-not-empty');
                                    }
                                }
                            }
                        }
                }else{
                    if(eyes_option[property]){
                        vital[property].check().should('have.class','ng-not-empty');
                    }   
                }
            }
        }
    }

    const ears = vitalData.ears
    if(ears.wnl){
        vital.ears_wnl.click().should('have.class','ng-not-empty');
    }else{
        const ears_option = ears.option
        for (const property in ears_option) {
            if (ears_option.hasOwnProperty(property)) {
                if(ears_option[property]){
                    vital[property].click().should('have.class','ng-not-empty');
                }   
            }
        }
        vital.ears_other.clear();
        vital.ears_other.type(ears.other).should('have.value', ears.other)
    }

    const mouth = vitalData.mouth
    if(mouth.dentures){
        vital.mouth_denture.click().should('have.class','ng-not-empty');
        const mouth_dentures_option = mouth.dentures_option
        for (const property in mouth_dentures_option) {
            if (mouth_dentures_option.hasOwnProperty(property)) {
                if(mouth_dentures_option[property]){
                    vital[property].click().should('have.class','ng-not-empty');
                }   
            }
        }
    }
    if(mouth.wnl){
        vital.mouth_wnl.click().should('have.class','ng-not-empty');
    }else{
        const mouth_option = mouth.option
        for (const property in mouth_option) {
            if (mouth_option.hasOwnProperty(property)) {
                if(mouth_option[property]){
                    if(typeof mouth_option[property] === 'object'){
                        if(mouth_option[property][property]){
                            vital[property].click().should('have.class','ng-not-empty');
                            vital[`${property}_describe`].type(mouth_option[property][`${property}_describe`]).should('have.value', mouth_option[property][`${property}_describe`])
                        }
                    }else{
                        if(typeof mouth_option[property] === 'string'){
                            vital[property].type(mouth_option[property]).should('have.value', mouth_option[property])
                        }else{
                            vital[property].click().should('have.class','ng-not-empty');
                        }
                    }
                }   
            }
        }
    }

    const nose = vitalData.nose
    if(nose.wnl){
        vital.nose_wnl.click().should('have.class','ng-not-empty');
    }else{
        const nose_option = nose.option
        for (const property in nose_option) {
            if (nose_option.hasOwnProperty(property)) {
                if(nose_option[property]){
                    if(typeof nose_option[property] === 'object'){
                        if(nose_option[property][property]){
                            vital[property].click().should('have.class','ng-not-empty');
                            vital[`${property}_describe`].type(nose_option[property][`${property}_describe`]).should('have.value', nose_option[property][`${property}_describe`])
                        }
                    }else{
                        if(typeof nose_option[property] === 'string'){
                            vital[property].type(nose_option[property]).should('have.value', nose_option[property])
                        }else{
                            vital[property].click().should('have.class','ng-not-empty');
                        }
                    }
                }   
            }
        }
    }

    const throat = vitalData.throat
    if(throat.wnl){
        vital.throat_wnl.click().should('have.class','ng-not-empty');
    }else{
        const throat_option = throat.option
        for (const property in throat_option) {
            if (throat_option.hasOwnProperty(property)) {
                if(throat_option[property]){
                    if(typeof throat_option[property] === 'object'){
                        if(throat_option[property][property]){
                            vital[property].click().should('have.class','ng-not-empty');
                            vital[`${property}_describe`].type(throat_option[property][`${property}_describe`]).should('have.value', throat_option[property][`${property}_describe`])
                        }
                    }else{
                        if(typeof throat_option[property] === 'string'){
                            vital[property].type(throat_option[property]).should('have.value', throat_option[property])
                        }else{
                            vital[property].click().should('have.class','ng-not-empty');
                        }
                    }
                }   
            }
        }
    }

    const speech = vitalData.speech
    if(speech.wnl){
        vital.speech_wnl.click().should('have.class','ng-not-empty');
    }else{
        const speech_option = speech.option
        for (const property in speech_option) {
            if (speech_option.hasOwnProperty(property)) {
                if(typeof speech_option[property] === 'string'){
                    vital[property].type(speech_option[property]).should('have.value', speech_option[property])
                }else{
                    if(speech_option[property]){
                        vital[property].click().should('have.class','ng-not-empty');
                    }
                }
            }   
        }
    }

    const touch = vitalData.touch
    if(touch.wnl){
        vital.touch_wnl.click().should('have.class','ng-not-empty');
    }else{
        const touch_option = touch.option
        for (const property in touch_option) {
            if (touch_option.hasOwnProperty(property)) {
                if(typeof touch_option[property] === 'string'){
                    vital[property].type(touch_option[property]).should('have.value', touch_option[property])
                }else{
                    if(touch_option[property]){
                        vital[property].click().should('have.class','ng-not-empty');
                    }
                }
            }   
        }
    }

    vital.sensory_observation.type(vitalData.sensory_observation).should('have.value', vitalData.sensory_observation)
    vital.sensory_intervention.type(vitalData.sensory_intervention).should('have.value', vitalData.sensory_intervention)

    const neurological = vitalData.neurological
    if(neurological.wnl){
        vital.neurological_wnl.click().should('have.class','ng-not-empty');
    }else{
        if(neurological.pupils_perrla){
            vital.pupils_perrla.click().should('have.class','ng-not-empty');
        }

        const neurological_option = neurological.options
        for (const property in neurological_option) {
            if (neurological_option.hasOwnProperty(property)) {
                if(typeof neurological_option[property] === 'string'){
                    vital[property].type(neurological_option[property]).should('have.value', neurological_option[property])
                }else{
                    if(neurological_option[property]){
                        vital[property].click().should('have.class','ng-not-empty');
                    }
                }
            }   
        }

    }

    const neurological_othersigns_weakness = vitalData.neurological.othersigns.othersigns_weakness
    const neurological_othersigns_paralysis = vitalData.neurological.othersigns.othersigns_paralysis
    const neurological_othersigns_tremors = vitalData.neurological.othersigns.othersigns_tremors
    const neurological_othersigns_seizure = vitalData.neurological.othersigns.othersigns_seizure
    const neurological_other = vitalData.neurological.othersigns.othersigns_other

    if(neurological_othersigns_weakness.othersigns_weakness){
        vital.othersigns_weakness.click().should('have.class','ng-not-empty');
        const weakness_option = neurological_othersigns_weakness.option

        for (const property in weakness_option) {
            if (weakness_option.hasOwnProperty(property)) {
                if(typeof weakness_option[property] === 'string'){
                    vital[property].type(weakness_option[property]).should('have.value', weakness_option[property])
                }else{
                    if(weakness_option[property]){
                        vital[property].click().should('have.class','ng-not-empty');
                    }
                }
            }   
        }

    }
    if(neurological_othersigns_paralysis.othersigns_paralysis){
        vital.othersigns_paralysis.click().should('have.class','ng-not-empty');
        const paralysis_option = neurological_othersigns_paralysis.option

        for (const property in paralysis_option) {
            if (paralysis_option.hasOwnProperty(property)) {
                if(typeof paralysis_option[property] === 'string'){
                    vital[property].type(paralysis_option[property]).should('have.value', paralysis_option[property])
                }else{
                    if(paralysis_option[property]){
                        vital[property].click().should('have.class','ng-not-empty');
                    }
                }
            }   
        }

    }
    if(neurological_othersigns_tremors.othersigns_tremors){
        vital.othersigns_tremors.click().should('have.class','ng-not-empty');
        const tremors_option = neurological_othersigns_tremors.option

        for (const property in tremors_option) {
            if (tremors_option.hasOwnProperty(property)) {
                if(typeof tremors_option[property] === 'string'){
                    vital[property].type(tremors_option[property]).should('have.value', tremors_option[property])
                }else{
                    if(tremors_option[property]){
                        vital[property].click().should('have.class','ng-not-empty');
                    }
                }
            }   
        }

    }
    if(neurological_othersigns_seizure.othersigns_seizure){
        vital.othersigns_seizure.click().should('have.class','ng-not-empty');
        const seizure_option = neurological_othersigns_seizure.option

        for (const property in seizure_option) {
            if (seizure_option.hasOwnProperty(property)) {
                if(typeof seizure_option[property] === 'string'){
                    vital[property].type(seizure_option[property]).should('have.value', seizure_option[property])
                }else{
                    if(seizure_option[property]){
                        vital[property].click().should('have.class','ng-not-empty');
                    }
                }
            }   
        }
    }

    if(issave){
        cy.get('.btn__success').click()
    }else{
        cy.get('.btn__warning').click()
        cy.wait(1000)
        cy.get('.swal2-confirm').click()
    }

}

function TaskCognitive(cognitiveData, issave){
    const cognitive = new CognitiveSelector()
    
    cy.get(':nth-child(4) > .oasis-section-tabs').click()
    cy.wait(2000)

    cognitive.C0100.type(cognitiveData.codes.C0100).should('have.value', cognitiveData.codes.C0100)
    cognitive.C0200.type(cognitiveData.codes.C0200).should('have.value', cognitiveData.codes.C0200)
    cognitive.C0300A.type(cognitiveData.codes.C0300A).should('have.value', cognitiveData.codes.C0300A)
    cognitive.C0300B.type(cognitiveData.codes.C0300C).should('have.value', cognitiveData.codes.C0300C)
    cognitive.C0400A.type(cognitiveData.codes.C0400A).should('have.value', cognitiveData.codes.C0400A)
    cognitive.C0400B.type(cognitiveData.codes.C0400B).should('have.value', cognitiveData.codes.C0400B)
    cognitive.C0400C.type(cognitiveData.codes.C0400C).should('have.value', cognitiveData.codes.C0400C)
    cognitive.C05001.type(cognitiveData.codes.C05001).should('have.value', cognitiveData.codes.C05001)
    cognitive.C05002.type(cognitiveData.codes.C05002).should('have.value', cognitiveData.codes.C05002)
    cognitive.C1310A.type(cognitiveData.codes.C1310A).should('have.value', cognitiveData.codes.C1310A)
    cognitive.C1310B.type(cognitiveData.codes.C1310B).should('have.value', cognitiveData.codes.C1310B)
    cognitive.C1310C.type(cognitiveData.codes.C1310C).should('have.value', cognitiveData.codes.C1310C)
    cognitive.C1310D.type(cognitiveData.codes.C1310D).should('have.value', cognitiveData.codes.C1310D)
    cognitive.M1700.type(cognitiveData.codes.M1700).should('have.value', cognitiveData.codes.M1700)
    cognitive.M1710.type(cognitiveData.codes.M1710).should('have.value', cognitiveData.codes.M1710)
    cognitive.M1720.type(cognitiveData.codes.M1720).should('have.value', cognitiveData.codes.M1720)

    const D0150 = cognitiveData.D0150
    for (const property in D0150) {
        if (D0150.hasOwnProperty(property)) {
            if(D0150[property]){
                cy.wait(200)
                cognitive[property].check().should('have.class','ng-not-empty');
            }   
        }
    }

    cognitive.D0700.type(cognitiveData.D0700).should('have.value', cognitiveData.D0700)

    const M1740 = cognitiveData.M1740
    for (const property in M1740) {
        if (M1740.hasOwnProperty(property)) {
            if(M1740[property]){
                cognitive[property].click().should('have.class','ng-not-empty');
            }   
        }
    }

    cognitive.M1745.type(cognitiveData.M1745).should('have.value', cognitiveData.M1745)
    
    if(issave){
        cy.get('.btn__success').click()
    }else{
        cy.get('.btn__warning').click()
        cy.wait(1000)
        cy.get('.swal2-confirm').click()
    }
}

function TaskIntegumentary(integumentaryData, issave){
    const integumentary = new IntegumentarySelector()
    
    cy.get(':nth-child(5) > .oasis-section-tabs').click()
    cy.wait(2000)

    const integumentarystatus = integumentaryData.integumentary
    if(integumentarystatus.wnl){
        integumentary.integumentarywnl.click()
    }else{
        const integumentarystatus_options = integumentarystatus.options
        for (const property in integumentarystatus_options) {
            if (integumentarystatus_options.hasOwnProperty(property)) {
                if(typeof integumentarystatus_options[property] == "string"){
                    integumentary[property].type(integumentarystatus_options[property]).should('have.value', integumentarystatus_options[property])
                }else{
                    if(integumentarystatus_options[property]){
                        integumentary[property].click().should('have.class','ng-not-empty');
                    }   
                }
            
            }
        }
    }

    integumentaryData.skinintact && integumentary.integrity_skinintact.click().should('have.class','ng-not-empty');

    const lesion = integumentaryData.lesion
    if(lesion.lesion){
        integumentary.integrity_lesions.click()
        const lesion_option = lesion.options
        for (let index = 0; index < lesion_option.length; index++) {
            integumentary.integrity_lesions_add.click()
        }
        for (let index = 0; index < lesion_option.length; index++) {
            cy.xpath(`//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[1]/tbody/tr[${index+7}]/td/table/tbody/tr/td[3]/div/div/div/a`).click()
            cy.xpath(`//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[1]/tbody/tr[${index+7}]/td/table/tbody/tr/td[3]/div/div/div/div/div/input`).type(lesion_option[index].name).should('have.value', lesion_option[index].name)
            cy.get('.highlighted').click()
            cy.xpath(`//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[1]/tbody/tr[${index+7}]/td/table/tbody/tr/td[5]/input`).type(lesion_option[index].location).should('have.value', lesion_option[index].location)
            cy.xpath(`//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[1]/tbody/tr[${index+7}]/td/table/tbody/tr/td[7]/input`).type(lesion_option[index].comment).should('have.value', lesion_option[index].comment)
              
        }   
    }

    const wound = integumentaryData.wound
    if(wound.wound){
        integumentary.integrity_wound.click()
        const wound_option = wound.options
        for (let index = 0; index < wound_option.length; index++) {
            integumentary.integrity_wound_add.click()
        }
        for (let index = 0; index < wound_option.length; index++) {
            cy.xpath(`//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[1]/tbody/tr[${index+7}]/td/table/tbody/tr/td[3]/div/div[1]/input`).type(`${wound_option[index].name}`).should('have.value', `${wound_option[index].name}`)
            cy.xpath(`//*[@id="parent"]/div/div/div/form/div/fieldset/div[5]/fieldset/div/table[1]/tbody/tr[${index+7}]/td/table/tbody/tr/td[5]/input`).type(wound_option[index].comment).should('have.value', wound_option[index].comment)
        }   
    }

    integumentary.observation.type(integumentaryData.observation).should('have.value', integumentaryData.observation)
    integumentary.intervention.type(integumentaryData.intervention).should('have.value', integumentaryData.intervention)

    integumentary.perception.type(integumentaryData.perception).should('have.value', integumentaryData.perception)
    integumentary.moisture.type(integumentaryData.moisture).should('have.value', integumentaryData.moisture)
    integumentary.activity.type(integumentaryData.activity).should('have.value', integumentaryData.activity)
    integumentary.mobility.type(integumentaryData.mobility).should('have.value', integumentaryData.mobility)
    integumentary.nutrition.type(integumentaryData.nutrition).should('have.value', integumentaryData.nutrition)
    integumentary.friction.type(integumentaryData.friction).should('have.value', integumentaryData.friction)
    
    integumentary.M1306.type(integumentaryData.M1306).should('have.value', integumentaryData.M1306)
 
    integumentary.M1311_A1.type(integumentaryData.M1311_A1).should('have.value', integumentaryData.M1311_A1)
    integumentary.M1311_B1.type(integumentaryData.M1311_B1).should('have.value', integumentaryData.M1311_B1)
    integumentary.M1311_C1.type(integumentaryData.M1311_C1).should('have.value', integumentaryData.M1311_C1)
    integumentary.M1311_D1.type(integumentaryData.M1311_D1).should('have.value', integumentaryData.M1311_D1)
    integumentary.M1311_E1.type(integumentaryData.M1311_E1).should('have.value', integumentaryData.M1311_E1)
    integumentary.M1311_F1.type(integumentaryData.M1311_F1).should('have.value', integumentaryData.M1311_F1)

    integumentary.M1322.type(integumentaryData.M1322).should('have.value', integumentaryData.M1322)
    integumentary.M1324.type(integumentaryData.M1324).should('have.value', integumentaryData.M1324)
    //integumentary.M1330.type(integumentaryData.M1330).should('have.value', integumentaryData.M1330)
    integumentary.M1332.type(integumentaryData.M1332).should('have.value', integumentaryData.M1332)
    integumentary.M1334.type(integumentaryData.M1334).should('have.value', integumentaryData.M1334)
    integumentary.M1340.type(integumentaryData.M1340).should('have.value', integumentaryData.M1340)
    integumentary.M1342.type(integumentaryData.M1342).should('have.value', integumentaryData.M1342)

}

function TaskRespiratory(respiratoryData, issave){
    const respiratory = new RespiratorySelector()

    cy.get(':nth-child(6) > .oasis-section-tabs').click()
    cy.wait(2000)

    respiratory.M1400.type(respiratoryData.M1400).should('have.value', respiratoryData.M1400)

    const respiratory_status = respiratoryData.respiratory
    if(respiratory_status.wnl){
        respiratory.respiratorywnl.click().should('have.class','ng-not-empty');
    }else{
        const respiratory_status_options = respiratory_status.options
        for (const property in respiratory_status_options) {
            if (respiratory_status_options.hasOwnProperty(property)) {
                if(respiratory_status_options[property]){
                    respiratory[property].check().should('have.class','ng-not-empty');
                }   
            }
        }
    }

    const abnormal = respiratoryData.abnormal
    if(abnormal.abnormal){
        respiratory.respiratory_abnormal_yes.click().should('have.class','ng-not-empty');
        const abnormal_options = abnormal.options
        for (const property in abnormal_options) {
            if (abnormal_options.hasOwnProperty(property)) {
                if(typeof abnormal_options[property] === 'string'){
                    respiratory[property].type(abnormal_options[property]).should('have.value', abnormal_options[property])
                }else{
                    if(abnormal_options[property]){
                        respiratory[property].click().should('have.class','ng-not-empty');
                    }
                }
            }   
        }
    }else{
        respiratory.respiratory_abnormal_no.click().should('have.class','ng-not-empty');
    }

    const cough = respiratoryData.cough
    if(cough.cough){
        respiratory.respiratory_cough_yes.click().should('have.class','ng-not-empty');
        const cough_options = cough.options
        for (const property in cough_options) {
            if (cough_options.hasOwnProperty(property)) {
                if(cough_options[property]){
                    respiratory[property].click().should('have.class','ng-not-empty');
                }
            }   
        }
    }else{
        respiratory.respiratory_cough_no.click().should('have.class','ng-not-empty');
    }

    const specialprod = respiratoryData.specialprod
    if(specialprod.specialprod){
        respiratory.respiratory_special_yes.click().should('have.class','ng-not-empty');
        
        const o2therapy = specialprod.o2therapy
        if(o2therapy.o2therapy){
            respiratory.respiratory_special_o2.click()
            cy.wait(1000)
            const o2therapy_options = o2therapy.options
            for (const property in o2therapy_options) {
                if (o2therapy_options.hasOwnProperty(property)) {
                    if(typeof o2therapy_options[property] === 'string'){
                        respiratory[property].type(o2therapy_options[property]).should('have.value', o2therapy_options[property])
                    }else{
                        if(o2therapy_options[property]){
                            respiratory[property].click().should('have.class','ng-not-empty');
                        }
                    }
                }   
            }
        }

        const tracheostomy = specialprod.tracheostomy
        if(tracheostomy.tracheostomy){
            respiratory.respiratory_special_trancheostomy.click()
            cy.wait(1000)
            const tracheostomy_options = tracheostomy.options
            for (const property in tracheostomy_options) {
                if (tracheostomy_options.hasOwnProperty(property)) {
                    if(typeof tracheostomy_options[property] === 'string'){
                        respiratory[property].type(tracheostomy_options[property]).should('have.value', tracheostomy_options[property])
                    }else{
                        if(tracheostomy_options[property]){
                            respiratory[property].click().should('have.class','ng-not-empty');
                        }
                    }
                }   
            }
        }

        const BiPAP = specialprod.BiPAP
        if(BiPAP.BiPAP){
            respiratory.respiratory_special_BiPAP.click()
            cy.wait(1000)
            const BiPAP_options = BiPAP.options
            for (const property in BiPAP_options) {
                if (BiPAP_options.hasOwnProperty(property)) {
                    if(typeof BiPAP_options[property] === 'string'){
                        respiratory[property].type(BiPAP_options[property]).should('have.value', BiPAP_options[property])
                    }else{
                        if(BiPAP_options[property]){
                            respiratory[property].click().should('have.class','ng-not-empty');
                        }
                    }
                }   
            }
        }

        const suctioning = specialprod.suctioning
        if(suctioning.suctioning){
            respiratory.respiratory_special_suctioning.click()
            cy.wait(1000)
            const suctioning_options = suctioning.options
            for (const property in suctioning_options) {
                if (suctioning_options.hasOwnProperty(property)) {
                    if(typeof suctioning_options[property] === 'string'){
                        respiratory[property].type(suctioning_options[property]).should('have.value', suctioning_options[property])
                    }else{
                        if(suctioning_options[property]){
                            respiratory[property].click().should('have.class','ng-not-empty');
                        }
                    }
                }   
            }
        }

        const ventilator = specialprod.ventilator
        if(ventilator.ventilator){
            respiratory.respiratory_special_ventilator.click()
            cy.wait(1000)
            const ventilator_options = ventilator.options
            for (const property in ventilator_options) {
                if (ventilator_options.hasOwnProperty(property)) {
                    if(typeof ventilator_options[property] === 'string'){
                        respiratory[property].type(ventilator_options[property]).should('have.value', ventilator_options[property])
                    }else{
                        if(ventilator_options[property]){
                            respiratory[property].click().should('have.class','ng-not-empty');
                        }
                    }
                }   
            }
        }

        const pleurX = specialprod.pleurX
        if(pleurX.pleurX){
            respiratory.respiratory_special_pleurx.click()
            cy.wait(1000)
            const pleurX_options = pleurX.options
            for (const property in pleurX_options) {
                if (pleurX_options.hasOwnProperty(property)) {
                    if(typeof pleurX_options[property] === 'string'){
                        respiratory[property].type(pleurX_options[property]).should('have.value', pleurX_options[property])
                    }else{
                        if(pleurX_options[property]){
                            respiratory[property].click().should('have.class','ng-not-empty');
                        }
                    }
                }   
            }
        }

        respiratory.respiratory_special_other.type(respiratoryData.specialprod.other)


    }else{
        respiratory.respiratory_special_no.click().should('have.class','ng-not-empty');
    }

    respiratory.respiratory_observation.type(respiratoryData.respiratory_observation).should('have.value', respiratoryData.respiratory_observation)
    respiratory.respiratory_intervention.type(respiratoryData.respiratory_intervention).should('have.value', respiratoryData.respiratory_intervention)

    const cardiovascular = respiratoryData.cardiovascular
    if(cardiovascular.wnl){
        respiratory.cardiovascularwnl.click()
    }else{
        const cardiovascular_options = cardiovascular.options
        for (const property in cardiovascular_options) {
            if (cardiovascular_options.hasOwnProperty(property)) {
                if(typeof cardiovascular_options[property] === 'string'){
                    respiratory[property].type(cardiovascular_options[property]).should('have.value', cardiovascular_options[property])
                }else{
                    if(cardiovascular_options[property]){
                        respiratory[property].click().should('have.class','ng-not-empty');
                    }
                }
            }   
        }
    }

    const abnormal_pulses = respiratoryData.abnormal_pulses
    for (const property in abnormal_pulses) {
        if (abnormal_pulses.hasOwnProperty(property)) {
            if(typeof abnormal_pulses[property] === 'string'){
                respiratory[property].type(abnormal_pulses[property]).should('have.value', abnormal_pulses[property])
            }else{
                if(abnormal_pulses[property]){
                    respiratory[property].click().should('have.class','ng-not-empty');
                }
            }
        }   
    }

    const peripheral = respiratoryData.peripheral
    for (const property in peripheral) {
        if (peripheral.hasOwnProperty(property)) {
            if(typeof peripheral[property] === 'string'){
                respiratory[property].type(peripheral[property]).should('have.value', peripheral[property])
            }else{
                if(peripheral[property]){
                    respiratory[property].click().should('have.class','ng-not-empty');
                }
            }
        }   
    }

    const chest = respiratoryData.chest
    for (const property in chest) {
        if (chest.hasOwnProperty(property)) {
            if(typeof chest[property] === 'string'){
                respiratory[property].type(chest[property]).should('have.value', chest[property])
            }else{
                if(chest[property]){
                    respiratory[property].click().should('have.class','ng-not-empty');
                }
            }
        }   
    }
    
}

function TaskNutrition(){
    cy.get(':nth-child(7) > .oasis-section-tabs').click()
}

function TaskMusculo(){
    cy.get(':nth-child(8) > .oasis-section-tabs').click()
}

function TaskMedication(){
    cy.get(':nth-child(9) > .oasis-section-tabs').click()
}

function TaskCare(){
    cy.get(':nth-child(10) > .oasis-section-tabs').click()
}

export { 
    TaskAdministrative,
    TaskDiagnosis,
    TaskVital,
    TaskCognitive,
    TaskIntegumentary,
    TaskRespiratory,
    TaskNutrition,
    TaskMusculo,
    TaskMedication,
    TaskCare
  }