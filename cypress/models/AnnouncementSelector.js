export default class AnnouncementSelector{
    get start(){
        return cy.get('#datefrom')
    }
    get end(){
        return cy.get('#dateto')
    }
    get description(){
        return cy.get(':nth-child(3) > .form-control')
    }
    get post(){
        return cy.get('.m-t-15 > .btn')
    }
    get delete(){
        return cy.get('[ng-click="announctrl.announcement.delete(item.annoucementid)"] > .zmdi')
    }
    get deleteConfirm(){
        return cy.get('.swal2-confirm')
    }
    get deleteCancel(){
        return cy.get('.swal2-cancel')
    }
    get edit(){
        return cy.get('[ng-click="items.showedit=true;items.annoucementid=item.annoucementid;announctrl.announcement.fetchannounce(item.annoucementid);"] > .zmdi')
    }
}