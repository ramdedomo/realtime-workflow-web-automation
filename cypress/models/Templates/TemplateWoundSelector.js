export default class TemplateWoundSelector{
    get fieldClick(){
        return cy.get('.chosen-single')
    }
    get fieldEnter(){
        return cy.get('#content > data > div.container.ng-scope > ui-view > div > div.block-header.bg__wt.max__width_wrapper.p-15 > fieldset > ng-form > table > tbody > tr:nth-child(1) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }
    get entry(){
        return cy.get('.global__txtbox')
    }
}