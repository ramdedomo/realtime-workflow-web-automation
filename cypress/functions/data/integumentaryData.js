export default {
    integumentary: {
        wnl: false,
        options: {
            integumentary_skin_pink: false,
            integumentary_skin_flushed: true,
            integumentary_skin_redness: false,
            integumentary_skin_pale: false,
            integumentary_skin_cyanotic: false,
            integumentary_skin_other: "Test Other",
            //
            integumentary_temp_warm: true,
            integumentary_temp_cool: false,
            integumentary_temp_hot: false,
            integumentary_temp_cold: false,
            //
            integumentary_moisture_dry: false,
            integumentary_moisture_dehydrated: false,
            integumentary_moisture_moist: true,
            integumentary_moisture_diaphoretic: false,
            integumentary_moisture_clammy: false,
            integumentary_moisture_other: "Test Other",
            //
            integumentary_turgor_normal: false,
            integumentary_turgor_fair: false,
            integumentary_turgor_poor: true,
        }
    },
    skinintact: true,
    lesion: {
        lesion: true,
        options: [
            //2 or more
            {
                name: "Rashes",
                location: "Test Location",
                comment: "Test Comment"
            },
            {
                name: "Dermatitis",
                location: "Test Location",
                comment: "Test Comment"
            },
            {
                name: "Blisters",
                location: "Test Location",
                comment: "Test Comment"
            }
        ]
    },
    wound: {
        wound: true,
        options: [
            {
                name: "Right Head - Skull",
                comment: "Test Comment",
            },
            {
                name: "Right Head - Nose",
                comment: "Test Comment",
            },
            {
                name: "Right Head - Mouth",
                comment: "Test Comment",
            }
        ]
    },
    observation: "Test Observation",
    intervention: "Test Intervention",
    perception: '2',
    moisture: '2',
    activity: '2',
    mobility: '2',
    nutrition: '2',
    friction: '2',
    M1306: '1',
    M1311_A1: '2',
    M1311_B1: '2',
    M1311_C1: '2',
    M1311_D1: '2',
    M1311_E1: '2',
    M1311_F1: '2',
    M1322: '1',
    M1324: '1',
    M1330: '1',
    M1332: '1',
    M1334: '1',
    M1340: '1',
    M1342: '1',
}