import BlankFormSelector from "../models/BlankFormSelector";
const blankform = new BlankFormSelector();

function AddBlankForm(blankformData = {}){
    blankform.edit.click()
    blankform.upload.click()
    cy.wait(2000)
    blankform.directoryClick.click()

    blankformData.directory != "" 
    ? blankform.directoryEnter.type(`${blankformData.directory}{enter}`) 
    : blankform.directoryEnter.type('Default{enter}')

    blankform.fileClick.click()
    cy.get('input[type=file]').selectFile(blankformData.file, { force: true })

    blankform.cancel.click()
}

export { AddBlankForm }