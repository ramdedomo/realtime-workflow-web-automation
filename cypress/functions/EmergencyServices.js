import EmergencyServiceSelector from "../models/EmergencyServiceSelector";
const emergency =  new EmergencyServiceSelector();

function AddEmergency(emergencyData = {}, issave = false){
    cy.get('.globallist__header_content > :nth-child(1) > .btn').click()
    cy.wait(2000)

    //adding new category
    // emergency.addCategoryClick.click()
    // cy.wait(2000)
    // emergency.addCategoryEnter.type(emergencyData.category)
    // emergency.addCategorySave.click()

    //seleting a category
    emergency.categoryClick.click()
    emergency.categoryEnter.type(emergencyData.category)
    cy.get('.highlighted').click()

    emergency.name.type(emergencyData.name)
    emergency.phone.type(emergencyData.phone)
    emergency.otherphone.type(emergencyData.otherphone)

    if(issave){
        emergency.save.click()
    }
}

function EditEmergency(emergencyData = {}, service, issave = false){
    cy.get('.searchbar__content > .ng-pristine').type(service)
    cy.wait(1000)
    cy.get('.btn > .zmdi').click()
    cy.wait(2000)

    //check what to edit
    for (const property in emergencyData) {
        if (emergencyData.hasOwnProperty(property)) {    
            emergency[property].clear()
            emergency[property].type(emergencyData[property])
            cy.log(`emergencyData has ${property} property`);
        }
    }

    if(issave){
        healthcare.save.click()
    }
}

export { AddEmergency, EditEmergency }