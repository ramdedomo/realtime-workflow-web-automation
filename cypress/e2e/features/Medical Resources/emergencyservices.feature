Feature: Emergency Services

    Scenario: Adding and Editing a new Emergency Services

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Medical Resources'
        Then a selected main nav should be expand

        When I click the sub nav 'Emergency Services'
        Then a selected sub nav should be display 

        When I add new emergency service
        Then added new emergency service should appear 

        # When I edit emergency service 'Automation Emergency'
        # Then the edited emergency service should be updated