Feature: Referral Sources

    Scenario: Adding and Editing a new Referral Source

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Medical Resources'
        Then a selected main nav should be expand

        When I click the sub nav 'Referral Sources'
        Then a selected sub nav should be display 

        When I add new referral sources
        Then added new referral sources should appear 

        # When I edit referral sources 'Ellie'
        # Then the edited referral sources should be updated