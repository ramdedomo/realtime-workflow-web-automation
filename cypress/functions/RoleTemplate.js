import RoleTemplateSelector from "../models/RoleTemplateSelector";

function AddRole(roleData = {}, issave = false){
    cy.get('.global__txtbox').type(roleData.TemplateName)

    RoleTemplateSelector.map(data => (
        roleData[data.main].selectall // check if select all it true
        ? cy.get(data.selector).click() // if true, select all
        : data.sub.map(subdata => (
            roleData[data.main].sub[subdata.name] && cy.get(subdata.selector).click() // else select sub checkbox
        ))
    ))

    if(issave){
        cy.get('.btn__success').click()
    }
}

function EditRole(roleData = {}, template, issave = false){
    
    cy.get('.searchbar__content > .ng-pristine').type(template)
    cy.wait(2000)
    cy.get('.btn > .zmdi').click()
    cy.wait(2000)

    cy.get('.global__txtbox').clear()
    cy.get('.global__txtbox').type(roleData.TemplateName)

    RoleTemplateSelector.map(data => (
        !roleData[data.main].selectall // check if select all it true
        ? cy.get(data.selector).click() // if true, select all
        : data.sub.map(subdata => (
            !roleData[data.main].sub[subdata.name] && cy.get(subdata.selector).click() // else select sub checkbox
        ))
    ))

    if(issave){
        cy.get('.btn__success').click()
    }
}

export { AddRole, EditRole }