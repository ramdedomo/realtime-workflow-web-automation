export default class VitalSignsSelector{
    get temperature(){
        return cy.get(':nth-child(2) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get temperature_oral(){
        return cy.get(':nth-child(2) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > [style="margin-right: 14px;"] > .ng-pristine')
    }
    get temperature_scan(){
        return cy.get(':nth-child(2) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(2) > .ng-pristine')
    }
    get temperature_tympanic(){
        return cy.get('[style="margin-left: 12px;"] > .ng-pristine')
    }
    get pulse(){
        return cy.get(':nth-child(3) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get pulse_radial(){
        return cy.get(':nth-child(3) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(1) > .ng-pristine')
    }
    get pulse_apical(){
        return cy.get(':nth-child(3) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(2) > .ng-pristine')
    }
    get pulse_brachial(){
        return cy.get('[style="margin-left: 6px;"] > .ng-pristine')
    }
    get respiration(){
        return cy.get(':nth-child(4) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }

    get armbpleft_systo(){
        return cy.get(':nth-child(5) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .text-right')
    }
    get armbpleft_diasto(){
        return cy.get('[name="SOOVS0059"]')
    }
    get armbpleft_sitting(){
        return cy.get(':nth-child(5) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(1) > .ng-pristine')
    }
    get armbpleft_lying(){
        return cy.get(':nth-child(5) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(2) > .ng-pristine')
    }
    get armbpleft_standing(){
        return cy.get(':nth-child(5) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(3) > .ng-pristine')
    }


    get armbpright_systo(){
        return cy.get(':nth-child(6) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .text-right')
    }
    get armbpright_diasto(){
        return cy.get('[name="SOOVS0058"]')
    }
    get armbpright_sitting(){
        return cy.get('.ng-isolate-scope > .m-0 > :nth-child(1) > .ng-pristine')
    }
    get armbpright_lying(){
        return cy.get('.ng-isolate-scope > .m-0 > :nth-child(2) > .ng-pristine')
    }
    get armbpright_standing(){
        return cy.get('.ng-isolate-scope > .m-0 > :nth-child(3) > .ng-pristine')
    }


    get legbpleft_systo(){
        return cy.get(':nth-child(7) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .text-right')
    }
    get legbpleft_diasto(){
        return cy.get('[name="SOOVS0061"]')
    }
    get legbpleft_sitting(){
        return cy.get(':nth-child(7) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(1) > .ng-pristine')
    }
    get legbpleft_lying(){
        return cy.get(':nth-child(7) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(2) > .ng-pristine')
    }
    get legbpleft_standing(){
        return cy.get(':nth-child(7) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(3) > .ng-pristine')
    }


    get legbpright_systo(){
        return cy.get(':nth-child(8) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .text-right')
    }
    get legbpright_diasto(){
        return cy.get('[name="SOOVS0060"]')
    }
    get legbpright_sitting(){
        return cy.get(':nth-child(8) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(1) > .ng-pristine')
    }
    get legbpright_lying(){
        return cy.get(':nth-child(8) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(2) > .ng-pristine')
    }
    get legbpright_standing(){
        return cy.get(':nth-child(8) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(3) > .ng-pristine')
    }


    get o2saturation(){
        return cy.get(':nth-child(9) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get o2saturation_(){
        return cy.get(':nth-child(10) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get o2saturation_at(){
        return cy.get('.inner__table-nb > tbody > tr > :nth-child(3) > .global__txtbox')
    }

    get bloodsugar(){
        return cy.get(':nth-child(11) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get bloodsugar_RBS(){
        return cy.get(':nth-child(11) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > [style="margin-right: 14px;"] > .ng-pristine')
    }
    get bloodsugar_FBS(){
        return cy.get('[style="margin-right: 13px;"] > .ng-pristine')
    }
    get bloodsugar_2hr(){
        return cy.get(':nth-child(11) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(3) > .ng-pristine')
    }

    get weight(){
        return cy.get('.p-l-5 > .global__txtbox')
    }
    get weight_actual(){
        return cy.get(':nth-child(12) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(1) > .ng-pristine')
    }
    get weight_stated(){
        return cy.get(':nth-child(12) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(2) > .ng-pristine')
    }
    get weight_unable(){
        return cy.get(':nth-child(12) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(3) > .ng-pristine')
    }

    get height(){
        return cy.get(':nth-child(13) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get height_actual(){
        return cy.get(':nth-child(13) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(1) > .ng-pristine')
    }
    get height_stated(){
        return cy.get(':nth-child(13) > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(3) > .m-0 > :nth-child(2) > .ng-pristine')
    }

    get vitalsbeyond(){
        return cy.get(':nth-child(14) > .oasis__answer > .inner__table-wb > tbody > tr > .p-5 > .ng-pristine')
    }
    get vitalsbeyond_md(){
        return cy.get('[chk="data.SOOVS0066"] > .ng-pristine')
    }
    get vitalsbeyond_cm(){
        return cy.get('[chk="data.SOOVS0067"] > .ng-pristine')
    }

    get evidence_yes(){
        return cy.get('.p-5 > .m-t-2 > :nth-child(2) > .ng-pristine')
    }
    get evidence_yes_decribe(){
        return cy.get('[style="width: 60%;"] > .global__txtbox')
    }
    get evidence_no(){
        return cy.get('.p-5 > .m-t-2 > :nth-child(1) > .ng-pristine')
    }
    get evidence_md(){
        return cy.get('[chk="data.SOOVS0031"] > .ng-pristine')
    }
    get evidence_cm(){
        return cy.get('[chk="data.SOOVS0032"] > .ng-pristine')
    }

    get newchange_yes(){
        return cy.get('[colspan="2"] > .m-t-2 > :nth-child(2) > .ng-pristine')
    }
    get newchange_yes_decribe(){
        return cy.get('[style="width: 84%;"] > .global__txtbox')
    }
    get newchange_no(){
        return cy.get('[colspan="2"] > .m-t-2 > :nth-child(1) > .ng-pristine')
    }
    get vitalsigns_observation(){
        return cy.get(':nth-child(16) > .oasis__answer > .inner__table-nb > tbody > tr > [style="width: 91%;"] > .global__txtbox')
    }
    get vitalsigns_intervention(){
        return cy.get(':nth-child(17) > .oasis__answer > .inner__table-nb > tbody > tr > [style="width: 91%;"] > .global__txtbox')
    }
   
    

    get temperature_gt(){
        return cy.get(':nth-child(2) > td.text-right > .global__txtbox')
    }
    get temperature_lt(){
        return cy.get(':nth-child(2) > :nth-child(4) > .global__txtbox')
    }
    get pulse_gt(){
        return cy.get(':nth-child(3) > :nth-child(2) > .global__txtbox')
    }
    get pulse_lt(){
        return cy.get(':nth-child(3) > :nth-child(4) > .global__txtbox')
    }
    get respiration_gt(){
        return cy.get(':nth-child(4) > :nth-child(2) > .global__txtbox')
    }
    get respiration_lt(){
        return cy.get(':nth-child(4) > :nth-child(4) > .global__txtbox')
    }
    get o2saturation_lt(){
        return cy.get(':nth-child(5) > :nth-child(3) > .global__txtbox')
    }
    get systolic_gt(){
        return cy.get('.table > tbody > :nth-child(2) > :nth-child(7) > .global__txtbox')
    }
    get systolic_lt(){
        return cy.get(':nth-child(2) > :nth-child(9) > .global__txtbox')
    }
    get diastolic_gt(){
        return cy.get(':nth-child(3) > :nth-child(7) > .global__txtbox')
    }
    get diastolic_lt(){
        return cy.get(':nth-child(3) > :nth-child(9) > .global__txtbox')
    }
    get fasting_gt(){
        return cy.get(':nth-child(4) > :nth-child(7) > .global__txtbox')
    }
    get fasting_lt(){
        return cy.get(':nth-child(4) > :nth-child(9) > .global__txtbox')
    }
    get random_gt(){
        return cy.get(':nth-child(6) > .global__txtbox')
    }
    get random_lt(){
        return cy.get(':nth-child(8) > .global__txtbox')
    }

    get J0510(){
        return cy.get(':nth-child(3) > :nth-child(1) > :nth-child(2) > .oasis__enter-code > .form-group > #code')
    }
    get J0520(){
        return cy.get(':nth-child(4) > :nth-child(1) > :nth-child(2) > .oasis__enter-code > .form-group > #code')
    }
    get J0530(){
        return cy.get(':nth-child(5) > :nth-child(1) > :nth-child(2) > .oasis__enter-code > .form-group > #code')
    }
    get B0200(){
        return cy.get(':nth-child(6) > :nth-child(1) > :nth-child(2) > .oasis__enter-code > .form-group > #code')
    }
    get B01000(){
        return cy.get(':nth-child(7) > :nth-child(1) > :nth-child(2) > .oasis__enter-code > .form-group > #code')
    }
    get B1300(){
        return cy.get(':nth-child(8) > :nth-child(1) > :nth-child(2) > .oasis__enter-code > .form-group > #code')
    }


    get eyes_wnl(){
        return cy.get(':nth-child(2) > .oasis__supp-l > .inner__table-nb > tbody > tr > [style="width: 20%"] > .checkbox > .ng-valid')
    }
    get eyes_reading(){
        return cy.get(':nth-child(2) > .oasis__supp-l > .inner__table-nb > tbody > tr > :nth-child(3) > .checkbox > .ng-valid')
    }
    get eyes_cataract_l(){
        return cy.get('.inner__table-wb > tbody > :nth-child(2) > :nth-child(2) > .checkbox > .ng-valid')
    }
    get eyes_cataract_r(){
        return cy.get(':nth-child(2) > :nth-child(3) > .checkbox > .ng-valid')
    }
    get eyes_glaucoma_l(){
        return cy.get('.inner__table-wb > tbody > :nth-child(3) > :nth-child(2) > .checkbox > .ng-pristine')
    }
    get eyes_glaucoma_r(){
        return cy.get(':nth-child(3) > :nth-child(3) > .checkbox > .ng-pristine')
    }
    get eyes_redness_l(){
        return cy.get('.inner__table-wb > tbody > :nth-child(4) > :nth-child(2) > .checkbox > .ng-pristine')
    }
    get eyes_redness_r(){
        return cy.get(':nth-child(4) > :nth-child(3) > .checkbox > .ng-pristine')
    }
    get eyes_pain_l(){
        return cy.get(':nth-child(5) > :nth-child(2) > .checkbox > .ng-pristine')
    }
    get eyes_pain_r(){
        return cy.get(':nth-child(5) > :nth-child(3) > .checkbox > .ng-pristine')
    }
    get eyes_itching_l(){
        return cy.get(':nth-child(6) > :nth-child(2) > .checkbox > .ng-pristine')
    }
    get eyes_itching_r(){
        return cy.get(':nth-child(6) > :nth-child(3) > .checkbox > .ng-pristine')
    }
    get eyes_ptosis_l(){
        return cy.get(':nth-child(2) > :nth-child(5) > .checkbox > .ng-pristine')
    }
    get eyes_ptosis_r(){
        return cy.get(':nth-child(2) > :nth-child(6) > .checkbox > .ng-pristine')
    }
    get eyes_sciera_l(){
        return cy.get(':nth-child(3) > :nth-child(5) > .checkbox > .ng-pristine')
    }
    get eyes_sciera_r(){
        return cy.get(':nth-child(3) > :nth-child(6) > .checkbox > .ng-pristine')
    }
    get eyes_edema_l(){
        return cy.get(':nth-child(4) > :nth-child(5) > .checkbox > .ng-pristine')
    }
    get eyes_edema_r(){
        return cy.get(':nth-child(4) > :nth-child(6) > .checkbox > .ng-pristine')
    }
    get eyes_blured_l(){
        return cy.get(':nth-child(5) > :nth-child(5) > .checkbox > .ng-pristine')
    }
    get eyes_blured_r(){
        return cy.get(':nth-child(5) > :nth-child(6) > .checkbox > .ng-pristine')
    }
    get eyes_blind_l(){
        return cy.get(':nth-child(6) > :nth-child(5) > .checkbox > .ng-pristine')
    }
    get eyes_blind_r(){
        return cy.get(':nth-child(6) > :nth-child(6) > .checkbox > .ng-pristine')
    }
    get eyes_exudate_l(){
        return cy.get(':nth-child(2) > :nth-child(8) > .checkbox > .ng-pristine')
    }
    get eyes_exudate_r(){
        return cy.get(':nth-child(2) > :nth-child(9) > .checkbox > .ng-pristine')
    }
    get eyes_tearing_l(){
        return cy.get(':nth-child(3) > :nth-child(8) > .checkbox > .ng-pristine')
    }
    get eyes_tearing_r(){
        return cy.get(':nth-child(3) > :nth-child(9) > .checkbox > .ng-pristine')
    }
    get eyes_macular_l(){
        return cy.get(':nth-child(4) > :nth-child(8) > .checkbox > .ng-pristine')
    }
    get eyes_macular_r(){
        return cy.get(':nth-child(4) > :nth-child(9) > .checkbox > .ng-pristine')
    }
    get eyes_retinopathy_l(){
        return cy.get(':nth-child(5) > :nth-child(8) > .checkbox > .ng-pristine')
    }
    get eyes_retinopathy_r(){
        return cy.get(':nth-child(5) > :nth-child(9) > .checkbox > .ng-pristine')
    }
    get eyes_other(){
        return cy.get(':nth-child(6) > :nth-child(7) > .global__txtbox')
    }
    get eyes_other_l(){
        return cy.get(':nth-child(6) > :nth-child(8) > .checkbox > .ng-pristine')
    }
    get eyes_other_r(){
        return cy.get(':nth-child(6) > :nth-child(9) > .checkbox > .ng-pristine')
    }


    get ears_wnl(){
        return cy.get(':nth-child(4) > .oasis__supp-l > .inner__table-nb > tbody > tr > [style="width: 20%"] > .checkbox > .ng-valid')
    }
    get ears_HOH_l(){
        return cy.get(':nth-child(2) > :nth-child(2) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_HOH_r(){
        return cy.get(':nth-child(2) > :nth-child(3) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_aid_l(){
        return cy.get(':nth-child(3) > :nth-child(2) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_aid_r(){
        return cy.get(':nth-child(3) > :nth-child(3) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_deaf_l(){
        return cy.get(':nth-child(4) > :nth-child(2) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_deaf_r(){
        return cy.get(':nth-child(4) > :nth-child(3) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_tinnitus_l(){
        return cy.get(':nth-child(2) > :nth-child(5) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_tinnitus_r(){
        return cy.get(':nth-child(2) > :nth-child(6) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_drainage_l(){
        return cy.get(':nth-child(3) > :nth-child(5) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_drainage_r(){
        return cy.get(':nth-child(3) > :nth-child(6) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_pain_l(){
        return cy.get(':nth-child(4) > :nth-child(5) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_pain_r(){
        return cy.get(':nth-child(4) > :nth-child(6) > .m-tb-0 > .checkbox > .ng-pristine')
    }
    get ears_other(){
        return cy.get('.inner__table-wb > tbody > :nth-child(2) > :nth-child(7) > .global__txtbox')
    }
    get ears_vertigo(){
        return cy.get('.lopt-left > .m-tb-0 > .checkbox > .ng-pristine')
    }

    get mouth_wnl(){
        return cy.get('[style="width: 20%"] > .m-0 > .checkbox > .ng-valid')
    }
    get mouth_denture(){
        return cy.get('.m-r-15 > .ng-scope')
    }
    get mouth_denture_upper(){
        return cy.get(':nth-child(3) > span.ng-isolate-scope > :nth-child(1) > .ng-scope')
    }
    get mouth_denture_lower(){
        return cy.get(':nth-child(3) > span.ng-isolate-scope > :nth-child(2) > .ng-scope')
    }
    get mouth_denture_partial(){
        return cy.get('.m-r-5 > .ng-scope')
    }
    get mouth_poordentition(){
        return cy.get('[ng-hide="data.SOOSENSORY0059"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(1) > :nth-child(1) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get mouth_gingivitis(){
        return cy.get('[c=""] > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get mouth_toothache(){
        return cy.get('[ng-hide="data.SOOSENSORY0059"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(3) > :nth-child(1) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get mouth_lossoftaste(){
        return cy.get('[ng-hide="data.SOOSENSORY0059"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(1) > :nth-child(2) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get mouth_lesions(){
        return cy.get('[ng-hide="data.SOOSENSORY0059"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(2) > :nth-child(2) > :nth-child(1) > .lopt-left > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get mouth_lesions_describe(){
        return cy.get('[ng-hide="data.SOOSENSORY0059"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(2) > :nth-child(2) > :nth-child(1) > .cont-opt > .global__txtbox')
    }
    get mouth_mass(){
        return cy.get('[style="width: 38%;"] > :nth-child(1) > .lopt-left > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get mouth_mass_describe(){
        return cy.get('[style="width: 38%;"] > :nth-child(1) > .cont-opt > .global__txtbox')
    }
    get mouth_difficulty(){
        return cy.get('.p-l-5 > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get mouth_other(){
        return cy.get('[ng-hide="data.SOOSENSORY0059"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(2) > .p-l-5 > :nth-child(1) > .cont-opt > .global__txtbox')
    }

    get nose_wnl(){
        return cy.get(':nth-child(8) > .oasis__supp-l > .inner__table-nb > tbody > tr > [style="width: 20%"] > .checkbox > .ng-valid')
    }
    get nose_congestion(){
        return cy.get('[ng-hide="data.SOOSENSORY0055"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(1) > :nth-child(1) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get nose_rhinitis(){
        return cy.get('[ng-hide="data.SOOSENSORY0055"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(2) > :nth-child(1) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get nose_sinusprob(){
        return cy.get('[ng-hide="data.SOOSENSORY0055"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(3) > :nth-child(1) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get nose_lossofsmell(){
        return cy.get('[ng-hide="data.SOOSENSORY0055"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(1) > :nth-child(2) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get nose_epistaxis(){
        return cy.get('[ng-hide="data.SOOSENSORY0055"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(2) > :nth-child(2) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get nose_noisy(){
        return cy.get(':nth-child(3) > :nth-child(2) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get nose_lesions(){
        return cy.get(':nth-child(3) > :nth-child(1) > .lopt-left > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get nose_lesions_describe(){
        return cy.get('[ng-hide="data.SOOSENSORY0055"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(1) > :nth-child(3) > :nth-child(1) > .cont-opt > .global__txtbox')
    }
    get nose_other(){
        return cy.get(':nth-child(2) > :nth-child(3) > :nth-child(1) > .cont-opt > div > .global__txtbox')
    }

    get throat_wnl(){
        return cy.get(':nth-child(10) > .oasis__supp-l > .inner__table-nb > tbody > tr > [style="width: 20%"] > .checkbox > .ng-valid')
    }
    get throat_sore(){
        return cy.get('[ng-hide="data.SOOSENSORY0063"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(1) > [style="width: 20%;"] > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get throat_dysphagia(){
        return cy.get('[ng-hide="data.SOOSENSORY0063"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(2) > :nth-child(1) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get throat_hoarness(){
        return cy.get('[style="width: 38%;"] > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get throat_lesions(){
        return cy.get('[ng-hide="data.SOOSENSORY0063"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(2) > :nth-child(2) > :nth-child(1) > .lopt-left > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get throat_lesions_describe(){
        return cy.get('fieldset > .global__txtbox')
    }
    get throat_other(){
        return cy.get('.p-l-5 > :nth-child(1) > .cont-opt > div > .global__txtbox')
    }
    
    get speech_wnl(){
        return cy.get(':nth-child(12) > .oasis__supp-l > .inner__table-nb > tbody > tr > [style="width: 20%"] > .checkbox > .ng-valid')
    }
    get speech_slow(){
        return cy.get('[ng-hide="data.SOOSENSORY0067"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(1) > [style="width: 20%;"] > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get speech_slurred(){
        return cy.get('[ng-hide="data.SOOSENSORY0067"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(2) > :nth-child(1) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get speech_aphasia(){
        return cy.get('[ng-hide="data.SOOSENSORY0067"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(1) > :nth-child(2) > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get speech_mechanical(){
        return cy.get('[colspan="2"] > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get speech_other(){
        return cy.get('[ng-hide="data.SOOSENSORY0067"] > .oasis__answer > .inner__table-nb > tbody > :nth-child(1) > :nth-child(3) > :nth-child(1) > .cont-opt > .global__txtbox')
    }

    get touch_wnl(){
        return cy.get(':nth-child(14) > .oasis__supp-l > .inner__table-nb > tbody > tr > [style="width: 20%"] > .checkbox > .ng-valid')
    }
    get touch_paresthesia(){
        return cy.get(':nth-child(1) > :nth-child(1) > :nth-child(1) > .lopt-left > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get touch_hyperesthesia(){
        return cy.get(':nth-child(2) > :nth-child(1) > :nth-child(1) > .lopt-left > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get touch_peripheral(){
        return cy.get('.p-l-5 > :nth-child(1) > .lopt-left > div.ng-isolate-scope > .checkbox > .ng-scope')
    }
    get touch_other(){
        return cy.get('.ng-isolate-scope > .cont-opt > .global__txtbox')
    }

    get sensory_observation(){
        return cy.get(':nth-child(9) > :nth-child(1) > :nth-child(16) > :nth-child(1) > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }
    get sensory_intervention(){
        return cy.get(':nth-child(9) > :nth-child(1) > :nth-child(17) > :nth-child(1) > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox')
    }

    get neurological_wnl(){
        return cy.get('.m-l-15 > .checkbox > .ng-pristine')
    }

    get pupils_perrla(){
        return cy.get(':nth-child(10) > :nth-child(1) > :nth-child(2) > :nth-child(2) > .checkbox > .ng-pristine')
    }

    get sizeunequal_lgr(){
        return cy.get('[style="margin-right: 59px !important;"] > .ng-pristine')
    }
    get sizeunequal_llr(){
        return cy.get('.b-b-n.ng-isolate-scope > :nth-child(2) > .ng-pristine')
    }

    get nonreactive_left(){
        return cy.get('[chk="data.SOONEURO0091"] > .ng-pristine')
    }
    get nonreactive_right(){
        return cy.get('[chk="data.SOONEURO0090"] > .ng-pristine')
    }

    get mental_oriented(){
        return cy.get('div > [chk="data.SOONEURO0093"] > .ng-scope')
    }
    get mental_oriented_time(){
        return cy.get('div > span.ng-isolate-scope > :nth-child(1) > .ng-scope')
    }
    get mental_oriented_place(){
        return cy.get('div > span.ng-isolate-scope > :nth-child(2) > .ng-scope')
    }
    get mental_oriented_person(){
        return cy.get('div > span.ng-isolate-scope > :nth-child(3) > .ng-scope')
    }
    get mental_oriented_purpose(){
        return cy.get('span.ng-isolate-scope > :nth-child(4) > .ng-scope')
    }
    get mental_disoriented(){
        return cy.get('[style="width: 21%;"] > .checkbox > .ng-scope')
    }
    get mental_lethargic(){
        return cy.get(':nth-child(3) > .p-b-5 > .checkbox > .ng-scope')
    }
    get mental_depressed(){
        return cy.get(':nth-child(4) > :nth-child(1) > .checkbox > .ng-scope')
    }
    get mental_forgetful(){
        return cy.get(':nth-child(2) > :nth-child(2) > .checkbox > .ng-scope')
    }
    get mental_agitated(){
        return cy.get('tbody > :nth-child(3) > :nth-child(2) > .checkbox > .ng-scope')
    }
    get mental_comatose(){
        return cy.get(':nth-child(4) > .p-b-5 > .checkbox > .ng-scope')
    }

    get sleep_adequate(){
        return cy.get('td > .m-r-10 > .ng-pristine')
    }
    get sleep_inadequate(){
        return cy.get(':nth-child(6) > :nth-child(2) > .inner__table-nb > tbody > tr > td > :nth-child(2) > .radio > .ng-untouched')
    }
    get sleep_inadequate_describe(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td > span:nth-child(3) > input')
    }
    get sleep_MD(){
        return cy.get(':nth-child(4) > .checkbox > .ng-pristine')
    }

    get hadngrips_strong_left(){
        return cy.get('[chk="data.SOONEURO0139"] > .ng-valid')
    }
    get hadngrips_strong_right(){
        return cy.get('[chk="data.SOONEURO0140"] > .ng-valid')
    }
    get hadngrips_weak_left(){
        return cy.get('[chk="data.SOONEURO0105"] > .ng-valid')
    }
    get hadngrips_weak_right(){
        return cy.get('[chk="data.SOONEURO0106"] > .ng-valid')
    }

    get othersigns_weakness(){
        return cy.get(':nth-child(1) > :nth-child(9) > :nth-child(2) > :nth-child(1) > .ng-scope')
    }

    get weakness_upperextremity_left(){
        return cy.get('[chk="data.SOONEURO0018"] > .ng-pristine')
    }
    get weakness_upperextremity_right(){
        return cy.get('[chk="data.SOONEURO0017"] > .ng-pristine')
    }
    get weakness_lowerextremity_left(){
        return cy.get('[chk="data.SOONEURO0021"] > .ng-pristine')
    }
    get weakness_lowerextremity_right(){
        return cy.get('[chk="data.SOONEURO0020"] > .ng-pristine')
    }
    get weakness_hemiparesis_left(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td > label:nth-child(2) > input')
    }
    get weakness_hemiparesis_right(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td > label:nth-child(3) > input')
    }

 
    get othersigns_paralysis(){
        return cy.get(':nth-child(1) > :nth-child(9) > :nth-child(2) > :nth-child(2) > .ng-scope')
    }
    get paralysis_hemiplegia_left(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(1) > input')
    }
    get paralysis_hemiplegia_right(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(2) > input')
    }
    get paralysis_paraplegia(){
        return cy.get(':nth-child(2) > .p-b-5 > .radio > .ng-valid')
    }
    get paralysis_quadriplegia(){
        return cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td > label > input')
    }
    
    get othersigns_tremors(){
        return cy.get(':nth-child(1) > :nth-child(9) > :nth-child(2) > :nth-child(3) > .ng-scope')
    }
    get tremors_upperextremity_left(){
        return cy.get('[chk="data.SOONEURO0024"] > .ng-pristine')
    }
    get tremors_upperextremity_right(){
        return cy.get('[chk="data.SOONEURO0023"] > .ng-pristine')
    }
    get tremors_upperextremity_fine(){
        return cy.get(':nth-child(1) > :nth-child(2) > .d-ib > :nth-child(1) > .ng-pristine')
    }
    get tremors_upperextremity_coarset(){
        return cy.get(':nth-child(1) > :nth-child(2) > .d-ib > .m-l-5 > .ng-pristine')
    }

    get tremors_lowerextremity_left(){
        return cy.get('[chk="data.SOONEURO0027"] > .ng-pristine')
    }
    get tremors_lowerextremity_right(){
        return cy.get('[chk="data.SOONEURO0026"] > .ng-pristine')
    }
    get tremors_lowerextremity_fine(){
        return cy.get(':nth-child(2) > :nth-child(2) > .d-ib > :nth-child(1) > .ng-pristine')
    }
    get tremors_lowerextremity_coarset(){
        return cy.get(':nth-child(2) > :nth-child(2) > .d-ib > .m-l-5 > .ng-pristine')
    }

    get othersigns_seizure(){
        return cy.get(':nth-child(1) > :nth-child(9) > :nth-child(2) > :nth-child(4) > .ng-scope')
    }
    get seizure_grand(){
        return cy.get('[style="padding: 5px 5px 0px;"] > .inner__table-nb > tbody > :nth-child(1) > .p-b-5 > :nth-child(1) > .ng-pristine')
    }
    get seizure_petit(){
        return cy.get('[style="padding: 5px 5px 0px;"] > .inner__table-nb > tbody > :nth-child(1) > .p-b-5 > :nth-child(2) > .ng-pristine')
    }
    get seizure_partial(){
        return cy.get('[style="padding: 5px 5px 0px;"] > .inner__table-nb > tbody > :nth-child(1) > .p-b-5 > :nth-child(3) > .ng-pristine')
    }
    get seizure_lastseizure(){
        return cy.get(':nth-child(2) > td > .ng-isolate-scope > .global__txtbox')
    }
    get seizure_duration(){
        return cy.get('.m-l-10 > .global__txtbox')
    }

    get othersigns_other(){
        return cy.get('[style="margin: 0;"] > .global__txtbox')
    }

    get neurological_observation(){
        return cy.get(':nth-child(14) > :nth-child(2) > .global__txtbox')
    }
    get neurological_intervention(){
        return cy.get(':nth-child(15) > :nth-child(2) > .global__txtbox')
    }
}
