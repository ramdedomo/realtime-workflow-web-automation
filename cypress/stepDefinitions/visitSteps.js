/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { NavigateMapper } from '../functions/VisitMapper';

    When('i navigate the date {string} {int} times', (navigation, tap) => {
        NavigateMapper(navigation, tap) // tap backward or forward visit mapper base on given number tap
    })

    Then('it should be navigate no of times', () => {

    })
