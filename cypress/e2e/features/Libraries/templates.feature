Feature: Templates

    Scenario: Adding a Template

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Libraries'
        Then a selected main nav should be expand

        When I click the sub nav 'Templates'
        Then a selected sub nav should be display 

        When I add new 485 template
        Then added new 485 template should appear

        # When I add new classification template
        # Then added new classification template should appear

        # When I add new wound treatment template
        # Then added new wound treatment template should appear

        # When I add new MDO template
        # Then added new MDO template should appear


 