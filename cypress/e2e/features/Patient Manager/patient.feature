Feature: Patient Manager

    Scenario: Adding and Editing a new Patient

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Patient Manager'
        Then a selected main nav should be expand

        # add
        # When I click the sub nav 'Add Patient'
        # Then a selected sub nav should be display 

        # When I add a new Patient
        # Then a new patient should be added

        # edit
        When I click the sub nav 'Patients'
        Then a selected sub nav should be display 

        # edit personal info
        # When I edit patient 'Sturgis, William' info
        # Then the patient info should be updated

        # edit documents
        When I edit patient 'Sturgis, William' documents
        Then the patient documents should be updated