import InsuranceCompanySelector from "../models/InsuranceCompanySelector"
const insurance = new InsuranceCompanySelector()

function AddInsurance(insuranceData = {}, issave = false){
    cy.get('.globallist__header_content > :nth-child(1) > .btn').click()

    //wait elements to load
    cy.wait(2000)

    insurance.name.type(insuranceData.name)
    insurance.abbr.type(insuranceData.abbr)
    insurance.contactperson.type(insuranceData.contactperson)
    insurance.address.type(insuranceData.address)
    insurance.city.type(insuranceData.city)
    
    insurance.stateClick.click();
    insurance.stateEnter.type(insuranceData.state)
    cy.get('.highlighted').click();

    insurance.zip.type(insuranceData.zip)
    insurance.phone.type(insuranceData.phone)
    insurance.fax.type(insuranceData.fax)

    insurance.payertypeClick.click();
    insurance.payertypeEnter.type(insuranceData.payertype);
    cy.get('.highlighted').click();

    insurance.invoicetypeClick.click();
    insurance.invoicetypeEnter.type(insuranceData.invoicetype);
    cy.get('.highlighted').click();

    insurance.typeofbillClick.click();
    insurance.typeofbillEnter.type(insuranceData.typeofbill);
    cy.get('.highlighted').click();

    insurance.chargetypeClick.click();
    insurance.chargetypeEnter.type(insuranceData.chargetype);
    cy.get('.highlighted').click();
    
    insurance.parentinsuranceClick.click();
    insurance.parentinsuranceEnter.type(insuranceData.parentinsurance);
    cy.get('.highlighted').click();
    
    insurance.entermediaryClick.click();
    insurance.entermediaryEnter.type(insuranceData.entermediary);
    cy.get('.highlighted').click();

    insurance.providercode.type(insuranceData.providercode)
    insurance.providerno.type(insuranceData.providerno)

    insurance.insurancewilluseClick.click();
    insurance.insurancewilluseEnter.type(insuranceData.insurancewilluse);
    cy.get('.highlighted').click();

    const patientcare = insuranceData.patientcare

    for (const property in patientcare) {
        if (patientcare.hasOwnProperty(property)) {
            if(patientcare[property]){
                insurance[property].click();
            }   
            cy.log(`patientcare has ${property} property`);
        }
    }
    
    if(issave){
        insurance.save.click();
    }
}

function EditInsurance(insuranceData = {},  company, issave = false){
    cy.wait(2000)

    cy.get('.searchbar__content > .ng-valid').type(company)
    cy.wait(2000)
    cy.get('.lv-actions > .btn').click()
    cy.wait(2000)

    //check what to edit
    for (const property in insuranceData) {
        if (insuranceData.hasOwnProperty(property)) {    
            insurance[property].clear()
            insurance[property].type(insuranceData[property])
            cy.log(`insuranceData has ${property} property`);
        }
    }

    if(issave){
        insurance.save.click()
    }

}


export { AddInsurance, EditInsurance }