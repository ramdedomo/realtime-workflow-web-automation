export default [
    { 
        name: "Dashboard", 
        selector: '.activenav > .expanded__item', 
        subNav: []
    },

    { 
        name: "Patient Manager", 
        selector: ':nth-child(2) > .expanded__item', 
        subNav: [
            { 
                name: 'Patients', 
                selector: '.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem' 
            },
            { 
                name: 'Add Patient', 
                selector: '.activenav > .expanded__subnav > :nth-child(2) > .expanded__subitem' 
            },
            { 
                name: 'Authorization', 
                selector: '.activenav > .expanded__subnav > :nth-child(3) > .expanded__subitem' 
            }
        ] 
    },
    
    { 
        name: "Human Resources", 
        selector: ':nth-child(3) > .expanded__item', 
        subNav: [
            { 
                name: 'User List', 
                selector: '.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem' 
            },
            { 
                name: 'Add User', 
                selector: '.activenav > .expanded__subnav > :nth-child(2) > .expanded__subitem' 
            },
            { 
                name: 'Add Role Template', 
                selector: '.activenav > .expanded__subnav > :nth-child(3) > .expanded__subitem' 
            },
            { 
                name: 'Role Template List', 
                selector: '.activenav > .expanded__subnav > :nth-child(4) > .expanded__subitem' 
            }
        ] 
    },

    { 
        name: "Medical Resources", 
        selector: ':nth-child(4) > .expanded__item', 
        subNav: [
            { 
                name: 'Hospitals/Facilities', 
                selector: '.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem' 
            },
            { 
                name: 'Physicians', 
                selector: '.activenav > .expanded__subnav > :nth-child(2) > .expanded__subitem' 
            },
            { 
                name: 'Referral Sources', 
                selector: '.activenav > .expanded__subnav > :nth-child(3) > .expanded__subitem' 
            },
            { 
                name: 'Insurance Companies', 
                selector: '.activenav > .expanded__subnav > :nth-child(4) > .expanded__subitem' 
            },
            { 
                name: 'Healthcare Vendors', 
                selector: '.activenav > .expanded__subnav > :nth-child(5) > .expanded__subitem' 
            },
            { 
                name: 'Emergency Services', 
                selector: '.activenav > .expanded__subnav > :nth-child(6) > .expanded__subitem' 
            },
        ] 
    },

    { 
        name: "Reports", 
        selector: ':nth-child(5) > .expanded__item', 
        subNav: []
    },

    { 
        name: "Utilities", 
        selector: ':nth-child(6) > .expanded__item', 
        subNav: [
            { 
                name: 'MD Order Manager', 
                selector: '.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem' 
            },
            { 
                name: 'Announcements', 
                selector: '.activenav > .expanded__subnav > :nth-child(2) > .expanded__subitem' 
            },
            { 
                name: 'Visit Mapper', 
                selector: '.activenav > .expanded__subnav > :nth-child(3) > .expanded__subitem' 
            }
        ] 
    },

    { 
        name: "Billing Manager 2.0", 
        selector: ':nth-child(7) > .expanded__item', 
        subNav: [
            { 
                name: 'Export OASIS', 
                selector: '.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem' 
            },
            { 
                name: 'Medicare Claims', 
                selector: '.activenav > .expanded__subnav > :nth-child(2) > .expanded__subitem' 
            },
            { 
                name: 'Patient Eligibility', 
                selector: '.activenav > .expanded__subnav > :nth-child(3) > .expanded__subitem' 
            },
            { 
                name: 'Other Claims', 
                selector: '.activenav > .expanded__subnav > :nth-child(4) > .expanded__subitem' 
            }
        ]  
    },

    { 
        name: "Libraries", 
        selector: ':nth-child(8) > .expanded__item', 
        subNav: [
            { 
                name: 'Blank Forms', 
                selector: '.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem' 
            },
            { 
                name: 'Documents', 
                selector: '.activenav > .expanded__subnav > :nth-child(2) > .expanded__subitem' 
            },
            { 
                name: 'Templates', 
                selector: '.activenav > .expanded__subnav > :nth-child(3) > .expanded__subitem' 
            }
        ] 
    },

    { 
        name: "System", 
        selector: ':nth-child(9) > .expanded__item', 
        subNav: [
            { 
                name: 'Settings', 
                selector: '.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem' 
            },
            { 
                name: 'Agency Info', 
                selector: '.activenav > .expanded__subnav > :nth-child(2) > .expanded__subitem' 
            },
            { 
                name: 'System Logs', 
                selector: '.activenav > .expanded__subnav > :nth-child(3) > .expanded__subitem' 
            },
            { 
                name: 'VS Parameters', 
                selector: '.activenav > .expanded__subnav > :nth-child(4) > .expanded__subitem' 
            }
        ]  
    },

    { 
        name: "Mail", 
        selector: ':nth-child(10) > .expanded__item', 
        subNav: []
    },
]