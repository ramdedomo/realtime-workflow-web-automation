import UserSelector_Personal from "../models/HumanResources/UserSelector_Personal";
import UserSelector_Professional from "../models/HumanResources/UserSelector_Professional";
import UserSelector_Health from "../models/HumanResources/UserSelector_Health";
import UserSelector from "../models/HumanResources/UserSelector";

const user = new UserSelector()
const user_personal = new UserSelector_Personal()
const user_professional = new UserSelector_Professional()
const user_heath = new UserSelector_Health()

function AddUser(userData = {}, issave = false){
    cy.get('.globallist__header_content > :nth-child(1) > .btn').click()
    cy.wait(2000)

    const userDataPersonal = userData.personal;
    const userDataProfessional = userData.professional;
    const userDataHealth = userData.health;

    user_personal.firstname.type(userDataPersonal.firstname)
    user_personal.lastname.type(userDataPersonal.lastname)
    user_personal.middleinitial.type(userDataPersonal.middleinitial)
    
    const gender = userDataPersonal.gender
  
    for (const property in gender) {
        if (gender.hasOwnProperty(property)) {
            if(gender[property]){
                user_personal[property].click();
            }   
            cy.log(`gender has ${property} property`);
        }
    }

    user_personal.birthdate.type(userDataPersonal.birthdate)

    user_personal.raceClick.click()
    user_personal.raceEnter.type(userDataPersonal.race)
    cy.get('.highlighted').click();

    user_personal.martialClick.click()
    user_personal.martialEnter.type(userDataPersonal.martial)
    cy.get('.highlighted').click();

    user_personal.disciplineClick.click()
    user_personal.disciplineEnter.type(userDataPersonal.discipline)
    cy.get('.highlighted').click();

    user_personal.titleClick.click()
    user_personal.titleEnter.type(userDataPersonal.title)
    cy.get('.highlighted').click();

    user_personal.hireClick.click()
    user_personal.hireEnter.type(userDataPersonal.hire)
    cy.get('.highlighted').click();

    user_personal.positionClick.click()
    user_personal.positionEnter.type(userDataPersonal.position)
    cy.get('.highlighted').click();

    user_personal.ssn.type(userDataPersonal.ssn)
    user_personal.address1.type(userDataPersonal.address1)
    user_personal.city.type(userDataPersonal.city)

    user_personal.stateClick.click()
    user_personal.stateEnter.type(userDataPersonal.state)
    cy.get('.highlighted').click();

    user_personal.zip.type(userDataPersonal.zip)
    user_personal.phone.type(userDataPersonal.phone)
    user_personal.mobile.type(userDataPersonal.mobile)
    user_personal.fax.type(userDataPersonal.fax)
    user_personal.email.type(userDataPersonal.email)
    user_personal.hiredate.type(userDataPersonal.hiredate)

    user_personal.languageprimaryClick.click()
    user_personal.languageprimaryEnter.type(userDataPersonal.languageprimary)
    cy.get('.highlighted').click();
    user_personal.languagesecondaryClick.click()
    user_personal.languagesecondaryEnter.type(userDataPersonal.languagesecondary)
    cy.get('.highlighted').click();

    user_personal.verbalprimaryClick.click()
    user_personal.verbalprimaryEnter.type(userDataPersonal.verbalprimary)
    cy.get('.highlighted').click();
    user_personal.verbalsecondaryClick.click()
    user_personal.verbalsecondaryEnter.type(userDataPersonal.verbalsecondary)
    cy.get('.highlighted').click();

    user_personal.readingprimaryClick.click()
    user_personal.readingprimaryEnter.type(userDataPersonal.readingprimary)
    cy.get('.highlighted').click();
    user_personal.readingsecondaryClick.click()
    user_personal.readingsecondaryEnter.type(userDataPersonal.readingsecondary)
    cy.get('.highlighted').click();

    user_personal.writingprimaryClick.click()
    user_personal.writingprimaryEnter.type(userDataPersonal.writingprimary)
    cy.get('.highlighted').click();
    user_personal.writingsecondaryClick.click()
    user_personal.writingsecondaryEnter.type(userDataPersonal.writingsecondary)
    cy.get('.highlighted').click();

    //professional tab
    user.professionalcredentials.click()
    cy.wait(2000)

    user_professional.professionallicense.click().type(userDataProfessional.professionallicense.option)
    cy.get('.highlighted').click();
    userDataProfessional.professionallicense.professionallicenseID && user_professional.professionallicenseID.type(userDataProfessional.professionallicense.professionallicenseID)
    userDataProfessional.professionallicense.professionalEffective && user_professional.professionalEffective.type(userDataProfessional.professionallicense.professionalEffective)
    userDataProfessional.professionallicense.professionalExpire && user_professional.professionalExpire.type(userDataProfessional.professionallicense.professionalExpire)
    userDataProfessional.professionallicense.professionalVerify && user_professional.professionalVerify.type(userDataProfessional.professionallicense.professionalVerify)

    user_professional.certificateverification.click().type(userDataProfessional.certificateverification.option)
    cy.get('.highlighted').click();
    userDataProfessional.certificateverification.certificateverificationID && user_professional.certificateverificationID.type(userDataProfessional.certificateverification.certificateverificationID)
    userDataProfessional.certificateverification.certificateverificationEffective && user_professional.certificateverificationEffective.type(userDataProfessional.certificateverification.certificateverificationEffective)
    userDataProfessional.certificateverification.certificateverificationExpire && user_professional.certificateverificationExpire.type(userDataProfessional.certificateverification.certificateverificationExpire)
    userDataProfessional.certificateverification.certificateverificationVerify && user_professional.certificateverificationVerify.type(userDataProfessional.certificateverification.certificateverificationVerify)

    user_professional.CPR.click().type(userDataProfessional.CPR.option)
    cy.get('.highlighted').click();
    userDataProfessional.CPR.CPRID && user_professional.CPRID.type(userDataProfessional.CPR.CPRID)
    userDataProfessional.CPR.CPREffective && user_professional.CPREffective.type(userDataProfessional.CPR.CPREffective)
    userDataProfessional.CPR.CPRExpire && user_professional.CPRExpire.type(userDataProfessional.CPR.CPRExpire)
    userDataProfessional.CPR.CPRVerify && user_professional.CPRVerify.type(userDataProfessional.CPR.CPRVerify)

    user_professional.driverlicense.click().type(userDataProfessional.driverlicense.option)
    cy.get('.highlighted').click();
    userDataProfessional.driverlicense.driverlicenseID && user_professional.driverlicenseID.type(userDataProfessional.driverlicense.driverlicenseID)
    userDataProfessional.driverlicense.driverlicenseEffective && user_professional.driverlicenseEffective.type(userDataProfessional.driverlicense.driverlicenseEffective)
    userDataProfessional.driverlicense.driverlicenseExpire && user_professional.driverlicenseExpire.type(userDataProfessional.driverlicense.driverlicenseExpire)
    userDataProfessional.driverlicense.driverlicenseVerify && user_professional.driverlicenseVerify.type(userDataProfessional.driverlicense.driverlicenseVerify)

    user_professional.carinsurance.click().type(userDataProfessional.carinsurance.option)
    cy.get('.highlighted').click();
    userDataProfessional.carinsurance.carinsuranceID && user_professional.carinsuranceID.type(userDataProfessional.carinsurance.carinsuranceID)
    userDataProfessional.carinsurance.carinsuranceID && user_professional.carinsuranceEffective.type(userDataProfessional.carinsurance.carinsuranceEffective)
    userDataProfessional.carinsurance.carinsuranceID && user_professional.carinsuranceExpire.type(userDataProfessional.carinsurance.carinsuranceExpire)
    userDataProfessional.carinsurance.carinsuranceID && user_professional.carinsuranceVerify.type(userDataProfessional.carinsurance.carinsuranceVerify)

    user_professional.professionalliability.click().type(userDataProfessional.professionalliability.option)
    cy.get('.highlighted').click();
    userDataProfessional.professionalliability.professionalliabilityID && user_professional.professionalliabilityID.type(userDataProfessional.professionalliability.professionalliabilityID)
    userDataProfessional.professionalliability.professionalliabilityID && user_professional.professionalliabilityEffective.type(userDataProfessional.professionalliability.professionalliabilityEffective)
    userDataProfessional.professionalliability.professionalliabilityID && user_professional.professionalliabilityExpire.type(userDataProfessional.professionalliability.professionalliabilityExpire)
    userDataProfessional.professionalliability.professionalliabilityID && user_professional.professionalliabilityVerify.type(userDataProfessional.professionalliability.professionalliabilityVerify)

    user_professional.skillschecklist.click().type(userDataProfessional.skillschecklist.option)
    cy.get('.highlighted').click();
    userDataProfessional.skillschecklist.skillschecklistEffective && user_professional.skillschecklistEffective.type(userDataProfessional.skillschecklist.skillschecklistEffective)
    userDataProfessional.skillschecklist.skillschecklistEffective && user_professional.skillschecklistExpire.type(userDataProfessional.skillschecklist.skillschecklistExpire)
    userDataProfessional.skillschecklist.skillschecklistEffective && user_professional.skillschecklistVerify.type(userDataProfessional.skillschecklist.skillschecklistVerify)

    user_professional.competencyevaluation.click().type(userDataProfessional.competencyevaluation.option)
    cy.get('.highlighted').click();
    userDataProfessional.competencyevaluation.competencyevaluationEffective && user_professional.competencyevaluationEffective.type(userDataProfessional.competencyevaluation.competencyevaluationEffective)
    userDataProfessional.competencyevaluation.competencyevaluationEffective && user_professional.competencyevaluationExpire.type(userDataProfessional.competencyevaluation.competencyevaluationExpire)
    userDataProfessional.competencyevaluation.competencyevaluationEffective && user_professional.competencyevaluationVerify.type(userDataProfessional.competencyevaluation.competencyevaluationVerify)

    user_professional.annualperformance.click().type(userDataProfessional.annualperformance.option)
    cy.get('.highlighted').click();
    userDataProfessional.annualperformance.annualperformanceEffective && user_professional.annualperformanceEffective.type(userDataProfessional.annualperformance.annualperformanceEffective)
    userDataProfessional.annualperformance.annualperformanceEffective && user_professional.annualperformanceExpire.type(userDataProfessional.annualperformance.annualperformanceExpire)
    userDataProfessional.annualperformance.annualperformanceVerify && user_professional.annualperformanceVerify.type(userDataProfessional.annualperformance.annualperformanceVerify)

    user_professional.livescan.click().type(userDataProfessional.livescan.option)
    cy.get('.highlighted').click();
    userDataProfessional.livescan.livescanEffective && user_professional.livescanEffective.type(userDataProfessional.livescan.livescanEffective)
    userDataProfessional.livescan.livescanExpire && user_professional.livescanExpire.type(userDataProfessional.livescan.livescanExpire)
    userDataProfessional.livescan.livescanVerify && user_professional.livescanVerify.type(userDataProfessional.livescan.livescanVerify)

    //health tab
    user.healthcredentials.click()
    cy.wait(2000)

    user_heath.physicalexamination.click().type(userDataHealth.physicalexamination.option)
    cy.get('.highlighted').click();
    userDataHealth.physicalexamination.physicalexaminationEffective && user_heath.physicalexaminationEffective.type(userDataHealth.physicalexamination.physicalexaminationEffective)
    userDataHealth.physicalexamination.physicalexaminationExpire && user_heath.physicalexaminationExpire.type(userDataHealth.physicalexamination.physicalexaminationExpire)
    userDataHealth.physicalexamination.physicalexaminationVerify && user_heath.physicalexaminationVerify.type(userDataHealth.physicalexamination.physicalexaminationVerify)

    user_heath.PPD.click().type(userDataHealth.PPD.option)
    cy.get('.highlighted').click();
    userDataHealth.PPD.PPDEffective && user_heath.PPDEffective.type(userDataHealth.PPD.PPDEffective)
    userDataHealth.PPD.PPDExpire && user_heath.PPDExpire.type(userDataHealth.PPD.PPDExpire)
    userDataHealth.PPD.PPDVerify && user_heath.PPDVerify.type(userDataHealth.PPD.PPDVerify)

    user_heath.Chest.click().type(userDataHealth.Chest.option)
    cy.get('.highlighted').click();
    userDataHealth.Chest.ChestEffective && user_heath.ChestEffective.type(userDataHealth.Chest.ChestEffective)
    userDataHealth.Chest.ChestExpire && user_heath.ChestExpire.type(userDataHealth.Chest.ChestExpire)
    userDataHealth.Chest.ChestVerify && user_heath.ChestVerify.type(userDataHealth.Chest.ChestVerify)

    user_heath.tuberculosis.click().type(userDataHealth.tuberculosis.option)
    cy.get('.highlighted').click();
    userDataHealth.tuberculosis.tuberculosisEffective && user_heath.tuberculosisEffective.type(userDataHealth.tuberculosis.tuberculosisEffective)
    userDataHealth.tuberculosis.tuberculosisExpire && user_heath.tuberculosisExpire.type(userDataHealth.tuberculosis.tuberculosisExpire)
    userDataHealth.tuberculosis.tuberculosisVerify && user_heath.tuberculosisVerify.type(userDataHealth.tuberculosis.tuberculosisVerify)

    user_heath.medicalhistory.click().type(userDataHealth.medicalhistory.option)
    cy.get('.highlighted').click();
    userDataHealth.medicalhistory.medicalhistoryEffective && user_heath.medicalhistoryEffective.type(userDataHealth.medicalhistory.medicalhistoryEffective)
    userDataHealth.medicalhistory.medicalhistoryExpire && user_heath.medicalhistoryExpire.type(userDataHealth.medicalhistory.medicalhistoryExpire)
    userDataHealth.medicalhistory.medicalhistoryVerify && user_heath.medicalhistoryVerify.type(userDataHealth.medicalhistory.medicalhistoryVerify)

    user_heath.healthquestionnaire.click().type(userDataHealth.healthquestionnaire.option)
    cy.get('.highlighted').click();
    userDataHealth.healthquestionnaire.healthquestionnaireEffective && user_heath.healthquestionnaireEffective.type(userDataHealth.healthquestionnaire.healthquestionnaireEffective)
    userDataHealth.healthquestionnaire.healthquestionnaireExpire && user_heath.healthquestionnaireExpire.type(userDataHealth.healthquestionnaire.healthquestionnaireExpire)
    userDataHealth.healthquestionnaire.healthquestionnaireVerify && user_heath.healthquestionnaireVerify.type(userDataHealth.healthquestionnaire.healthquestionnaireVerify)

    user_heath.hepatitis.click().type(userDataHealth.hepatitis.option)
    cy.get('.highlighted').click();
    userDataHealth.hepatitis.hepatitisEffective && user_heath.hepatitisEffective.type(userDataHealth.hepatitis.hepatitisEffective)
    userDataHealth.hepatitis.hepatitisExpire && user_heath.hepatitisExpire.type(userDataHealth.hepatitis.hepatitisExpire)
    userDataHealth.hepatitis.hepatitisVerify && user_heath.hepatitisVerify.type(userDataHealth.hepatitis.hepatitisVerify)

    if(issave){
        user.save.click();
    }

}

function EditUser(userData = {}, username, issave = false){
    cy.get('.searchbar__content > .ng-valid').type(username+'{enter}')
    user.editbutton.click()
}

function ResetPassword(){

}

function EditAccount(){

}

export {AddUser, EditAccount, EditUser, ResetPassword}