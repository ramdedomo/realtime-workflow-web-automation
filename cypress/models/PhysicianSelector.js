export default class PhysicianSelector {
    get firstname(){
        return cy.get(':nth-child(2) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get middlename(){
        return cy.get(':nth-child(3) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get lastname(){
        return cy.get(':nth-child(4) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get address(){
        return cy.get('.oasis__answer > .fg-line > .form-control')
    }

    get city(){
        return cy.get(':nth-child(6) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get stateClick(){
        return cy.get('.chosen-single')
    }

    get stateEnter(){
        return cy.get('#state_chosen > div > div > input[type=text]')
    }

    get zip(){
        return cy.get('#zipCode')
    }

    get phone(){
        return cy.get('.oasis__answer > .global__txtbox')
    }

    get fax(){
        return cy.get(':nth-child(10) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get cellphone(){
        return cy.get(':nth-child(11) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get contactPerson(){
        return cy.get(':nth-child(12) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get specialty(){
        return cy.get(':nth-child(13) > .oasis__answer > div > .form-control')
    }

    get licenseNo(){
        return cy.get(':nth-child(14) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get expirationDate(){
        return cy.get('#expiration')
    }

    get email(){
        return cy.get(':nth-child(17) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get save(){
        return cy.get('.btn__success')
    }
    
}