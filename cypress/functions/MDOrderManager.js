import MDOrderSelector from "../models/MDOrderSelector";
const md = new MDOrderSelector();

function ExploreMD(mdFilter = {}, patient){
    md.filter.click()
    cy.wait(4000)

    md.patientClick.click()
    md.patientEnter.type(mdFilter.patient)
    cy.get('.highlighted').click();

    md.clinicianClick.click()
    md.clinicianEnter.type(mdFilter.clinician)
    cy.get('.highlighted').click();

    md.physicianClick.click()
    md.physicianEnter.type(mdFilter.physician)
    cy.get('.highlighted').click();

    md.typeClick.click()
    md.typeEnter.type(mdFilter.type)
    cy.get('.highlighted').click();

    md.groupClick.click()
    md.groupEnter.type(mdFilter.group)
    cy.get('.highlighted').click();

    md.ageorderClick.click()
    md.ageorderEnter.type(mdFilter.aged)
    cy.get('.highlighted').click();

    md.search.type(patient)
    cy.wait(2000)

    md.print.click()
}

export {ExploreMD}