Feature: Insurance Companies

    Scenario: Adding and Editing a new Insurance Company

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Medical Resources'
        Then a selected main nav should be expand

        When I click the sub nav 'Insurance Companies'
        Then a selected sub nav should be display 

        When I add new insurance company
        Then added new insurance company should appear 

        # When I edit insurance company 'Insurance Automation'
        # Then the edited insurance company should be updated