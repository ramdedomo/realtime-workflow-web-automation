/// <reference types="Cypress" />
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { AddBlankForm } from '../functions/BlankForms';

    When('i add new blank form on directory {string}', (directory) => {
        let addFormData = {
            directory: directory,
            file: "cypress/functions/data/Realtime_01.png"
        }

        AddBlankForm(addFormData) // add form data
    })

    Then('blank form should be added', () => {

    })
