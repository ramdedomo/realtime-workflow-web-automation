import PreAdmissionSelector from "../models/Patient/PreAdmissionSelector";
const preadmit = new PreAdmissionSelector();

import administrativeData from "./data/administrativeData";
import diagnosisData from "./data/diagnosisData";
import vitalData from "./data/vitalData";
import cognitiveData from "./data/cognitiveData";
import integumentaryData from "./data/integumentaryData";
import respiratoryData from "./data/respiratoryData";

import { 
    TaskAdministrative,
    TaskDiagnosis,
    TaskVital,
    TaskCognitive,
    TaskIntegumentary,
    TaskRespiratory,
    TaskNutrition,
    TaskMusculo,
    TaskMedication,
    TaskCare
} from "./TaskFunctions";



function Admission(patientData, admit = true, issave = false){
 
    cy.get('.mhc__btn-neutral').click()
    cy.wait(2000)
    //optional field => &&
    preadmit.referraldate.type(patientData.referraldate)
    preadmit.referraltime.type(patientData.referraltime)
    
    if(patientData.autoassign){
        preadmit.autoassign.click()
    }else{
        preadmit.manualassign.type(patientData.manualassign)
    }

    //if pre admit add soc date
    admit && preadmit.SOCdate.type(patientData.SOCdate)

    preadmit.lastname.type(patientData.lastname)
    preadmit.firstname.type(patientData.firstname)
    preadmit.middleinitial.type(patientData.middleinitial)
    
    patientData.suffix && preadmit.suffix.type(patientData.suffix)
    preadmit.birthdate.type(patientData.birthdate)

    const gender = patientData.gender
  
    for (const property in gender) {
        if (gender.hasOwnProperty(property)) {
            if(gender[property]){
                preadmit[property].click();
            }   
        }
    }

    preadmit.maritalClick.click()
    preadmit.maritalEnter.type(patientData.marital)
    cy.get('.highlighted').click();

    preadmit.ethnicityClick.click()
    preadmit.ethnicityEnter.type(patientData.ethnicity)
    cy.get('.highlighted').click();

    preadmit.languagespokenClick.click()
    const languagespoken = patientData.languagespoken
    languagespoken.map(data=>(
        preadmit.languagespokenClick.type(`${data}{enter}`)
    ))


    preadmit.ssn.type(patientData.ssn,{force: true})
    preadmit.street.type(patientData.street)
    patientData.additionalstreet && preadmit.additionalstreet.type(patientData.additionalstreet)
    patientData.majorstreet && preadmit.majorstreet.type(patientData.majorstreet)
    preadmit.city.type(patientData.city)

    preadmit.stateClick.click()
    preadmit.stateEnter.type(patientData.state)
    cy.get('.highlighted').click();

    preadmit.zip.type(patientData.zip)
    preadmit.phone1.type(patientData.phone1)
    preadmit.phone2.type(patientData.phone2)
    preadmit.email.type(patientData.email)

    //same address
    preadmit.sameas.click()

    const livingarrangement = patientData.livingarrangement
  
    for (const property in livingarrangement) {
        if (livingarrangement.hasOwnProperty(property)) {
            if(livingarrangement[property]){
                preadmit[property].click();
            }   
        }
    }

    const assistance = patientData.assistance
  
    for (const property in assistance) {
        if (assistance.hasOwnProperty(property)) {
            if(assistance[property]){
                preadmit[property].click();
            }   
        }
    }

    preadmit.caregiver.type(patientData.caregiver)
    preadmit.caregiverphone.type(patientData.caregiverphone)
    preadmit.emergencyname.type(patientData.emergencyname)
    preadmit.emergencyrelationship.type(patientData.emergencyrelationship)
    preadmit.emergencyphone1.type(patientData.emergencyphone1)
    preadmit.emergencyphone2.type(patientData.emergencyphone2)

    
    preadmit.attendingPhysicianClick.click()
    preadmit.attendingPhysicianClick.type(patientData.attendingphysician)
    cy.get('.highlighted').click();

    preadmit.primaryPhysicianClick.click()
    preadmit.primaryPhysicianEnter.type(patientData.primaryphysician)
    cy.get('.highlighted').click();

    const otherphysician = patientData.otherphysician
    preadmit.otherPhysicianClick.click()
    otherphysician.map(data => {
        preadmit.otherPhysicianEnter.type(`${data}{enter}`)
    })

    preadmit.primaryInsuranceClick.click()
    preadmit.primaryInsuranceEnter.type(patientData.primaryinsurance)
    cy.get('.highlighted').click();

    preadmit.primaryInsurancePolicy.type(patientData.primaryInsurancePolicy)
    preadmit.primaryInsuranceAuth.type(patientData.primaryInsuranceAuth)

    preadmit.secondaryInsuranceClick.click()
    preadmit.secondaryInsuranceEnter.type(patientData.secondaryinsurance)
    cy.get('.highlighted').click();

    preadmit.secondaryInsurancePolicy.type(patientData.secondaryInsurancePolicy)
    preadmit.secondaryInsuranceAuth.type(patientData.secondaryInsuranceAuth)

    preadmit.typeofadmissionClick.click()
    preadmit.typeofadmissionEnter.type(patientData.typeofadmission)
    cy.get('.highlighted').click();

    preadmit.pointoforiginClick.click()
    preadmit.pointoforiginEnter.type(patientData.pointoforigin)
    cy.get('.highlighted').click();

    preadmit.typeofreferralClick.click()
    preadmit.typeofreferralEnter.type(patientData.typeofreferral)
    cy.get('.highlighted').click();

    preadmit.referralsourcesClick.click()
    preadmit.referralsourcesEnter.type(patientData.referralsources)
    cy.get('.highlighted').click();

    preadmit.referralphone.type(patientData.referralphone)

    preadmit.hospitalClick.click()
    preadmit.hospitalEnter.type(patientData.hospital)
    cy.get('.highlighted').click();

    preadmit.hospitaladmit.type(patientData.hospitaladmit)
    preadmit.hospitaldischarge.type(patientData.hospitaldischarge)
    preadmit.diagnosissurgery.type(patientData.diagnosissurgery)

    if(patientData.allergiesnka){
        preadmit.allergiesnka.click()
    }else{
        const allergiestags = patientData.allergiestags
        preadmit.allergiestags.click()
        allergiestags.map(data => (
            preadmit.allergiestags.type(`${data}{enter}`)
        ))
    }


    const personcompleting = patientData.personcompleting
  
    for (const property in personcompleting) {
        if (personcompleting.hasOwnProperty(property)) {
            if(personcompleting[property]){
                preadmit[property].click();
            }   
        }
    }


    preadmit.rnClick.click()
    patientData.staff.rnName && preadmit.rnEnter.type(patientData.staff.rnName)
    patientData.staff.rnName && cy.get('.highlighted').click();

    preadmit.ptClick.click()
    patientData.staff.ptName && preadmit.ptEnter.type(patientData.staff.ptName)
    patientData.staff.ptName && cy.get('.highlighted').click();

    preadmit.stClick.click()
    patientData.staff.stName && preadmit.stEnter.type(patientData.staff.stName)
    patientData.staff.stName && cy.get('.highlighted').click();

    preadmit.otClick.click()
    patientData.staff.otName && preadmit.otEnter.type(patientData.staff.otName)
    patientData.staff.otName && cy.get('.highlighted').click();

    preadmit.cmClick.click()
    preadmit.cmEnter.type(patientData.staff.cmName)
    cy.get('.highlighted').click();


    issave && preadmit.save.click();
}

function EditPatient(patientData, patient, issave = false){
    cy.get('.searchbar__content > .ng-pristine').type(`${patient}{enter}`)
    cy.wait(2000)
    cy.get('.m-r-5 > .zmdi').click()
    cy.wait(2000)

    //check what to edit
    for (const property in patientData) {
        if (patientData.hasOwnProperty(property)) {    
            preadmit[property].clear()
            preadmit[property].type(patientData[property])
            cy.log(`patientData has ${property} property`);
        }
    }

    issave && preadmit.save.click();

}

function Task(patient, issave = false){
    cy.get('.searchbar__content > .ng-pristine').type(`${patient}{enter}`)
    cy.wait(2000)
    cy.get('.wdTabCusPatient').click()
    cy.wait(2000)

    cy.get('.sampletd > :nth-child(2) > .ng-binding').click()
    cy.wait(1000)
    cy.get('.btn__warning').click()
    cy.wait(2000)

    //TaskAdministrative(administrativeData, issave)
    //TaskDiagnosis(diagnosisData, issave)
    //TaskVital(vitalData, issave)
    //TaskCognitive(cognitiveData, issave)
    //TaskIntegumentary(integumentaryData, issave)
    TaskRespiratory(respiratoryData, issave)
    
    
}

export { Admission, EditPatient, Task }