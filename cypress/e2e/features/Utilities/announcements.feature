Feature: Announcements

    Scenario: Adding Announcements

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Utilities'
        Then a selected main nav should be expand

        When I click the sub nav 'Announcements'
        Then a selected sub nav should be display 

        When i add new annoucement
        Then added annoucement should be appear

        When i edit the created annoucement
        Then created annoucement should be updated

        When i delete the created annoucement
        Then created annoucement should be deleted
