Feature: MD Order Manager

    Scenario: Exploring MD Order Manager

        Given I navigate to Realtime Workflow Website
        When I enter login credentials 'ramddm@geeksnest' and 'Ram@1234567890'
        Then I should be logged in

        When I click the main nav 'Utilities'
        Then a selected main nav should be expand

        When I click the sub nav 'MD Order Manager'
        Then a selected sub nav should be display 

        When i search patient 'Peter'
        Then the patient should appear