export default class ReferralSourcesSelector{
    get fname(){
        return cy.get(':nth-child(2) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get lname(){
        return cy.get(':nth-child(3) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get company(){
        return cy.get(':nth-child(4) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get address(){
        return cy.get(':nth-child(5) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get city(){
        return cy.get(':nth-child(6) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get stateEnter(){
        return cy.get('#state_chosen > div > div > input[type=text]')
    }
    get stateClick(){
        return cy.get('.chosen-single')
    }
    get zip(){
        return cy.get('#zipCode')
    }
    get phone(){
        return  cy.get(':nth-child(9) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get fax(){
        return  cy.get(':nth-child(10) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get save(){
        return  cy.get('.btn__success')
    }
}