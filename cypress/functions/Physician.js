import PhysicianSelector from '../models/PhysicianSelector';
const physician = new PhysicianSelector();

function AddPhysician(physcianData = {}, issave = false) {
    cy.get('.globallist__header_content > :nth-child(1) > .btn').click()
    cy.wait(1000)
    cy.get('.mhc__btn-neutral').click()

    cy.wait(2000)
    
    physician.firstname.type(physcianData.fname)
    physician.middlename.type(physcianData.mname)
    physician.lastname.type(physcianData.fname)
    physician.address.type(physcianData.address)
    physician.city.type(physcianData.city)
    
    physician.stateClick.click();
    physician.stateEnter.type(physcianData.state)
    cy.get('.highlighted').click();

    physician.zip.type(physcianData.zip)
    physician.phone.type(physcianData.phone)
    physician.fax.type(physcianData.fax)
    physician.cellphone.type(physcianData.cellphone)
    physician.contactPerson.type(physcianData.contactPerson)
    physician.specialty.type(physcianData.specialty)
    physician.licenseNo.type(physcianData.licenseNo)
    physician.expirationDate.type(physcianData.expirationDate)
    physician.email.type(physcianData.email)

    if(issave){
        physician.save.click();
    }
}

function EditPhysician(physcianData = {}, person = "", issave = false) {
    //wait elements to load
    cy.wait(2000)

    cy.get('.searchbar__content > .ng-valid').type(person)
    cy.wait(2000)
    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive > table > tbody > tr:nth-child(2) > td:nth-child(5) > div > button:nth-child(2)').click()
    cy.wait(2000) 

    //check what to edit
    for (const property in physcianData) {
        if (physcianData.hasOwnProperty(property)) {    
            physician[property].clear()
            physician[property].type(physcianData[property])
            cy.log(`physcianData has ${property} property`);
        }
    }

    if(issave){
        physician.save.click();
    }
 
}

export { AddPhysician, EditPhysician }