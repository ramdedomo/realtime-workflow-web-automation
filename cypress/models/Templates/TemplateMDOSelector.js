export default class TemplateMDOSelector{
    get title(){
        return cy.get('.global__txtbox')
    }
    get description(){
        return cy.get('.oasis__answer > .form-control')
    }
}