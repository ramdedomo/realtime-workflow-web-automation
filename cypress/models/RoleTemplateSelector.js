export default [
    {
        main: "PatientManager",
        selector: '[ng-repeat="rg in rolegrp"][style=""] > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-valid',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_pm', name: "Intake" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_pm', name: "Deletepatientinformation" },
            { selector: ':nth-child(3) > .checkbox > .ng-binding > .rolegrp_flg_pm', name: "Accesspatientgrantedtouser" },
            { selector: ':nth-child(4) > .checkbox > .ng-binding > .rolegrp_flg_pm', name: "Accesspatientroster" },
        ]
    },
    {
        main: "PatientCare",
        selector: ':nth-child(2) > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-pristine',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Schedulepatientvisitstasks" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Authorizationmanager" },
            { selector: ':nth-child(3) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Uploadmanagemedicalrecords" },
            { selector: ':nth-child(4) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Managevisitstasks" },
            { selector: ':nth-child(5) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Manageuploads" },
            { selector: ':nth-child(6) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Allowcommenting" },
            { selector: ':nth-child(7) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Approvedocumentsandevaluation" },
            { selector: ':nth-child(8) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "LockUnlockOASIS" },
            { selector: ':nth-child(9) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "TranscribeMDOrders" },
            { selector: ':nth-child(10) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "MDOrderManagement" },
            { selector: ':nth-child(11) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Createcommunicationnotes" },
            { selector: ':nth-child(12) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Communicationnotesmanagement" },
            { selector: ':nth-child(13) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Specialproceduremanagement" },
            { selector: ':nth-child(14) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Createmedicationprofile" },
            { selector: ':nth-child(15) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Medicationprofilemanagement" },
            { selector: ':nth-child(16) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Miscellaneous" },
            { selector: ':nth-child(17) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "OASIS" },
            { selector: ':nth-child(18) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "RecordManagementOASISnotincluded" },
            { selector: ':nth-child(19) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Revert" },
            { selector: ':nth-child(20) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Caseconferencemanagement" },
            { selector: ':nth-child(21) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "AccessQASIS" },
            { selector: ':nth-child(22) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "CreateEvents" },
            { selector: ':nth-child(23) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "EventsManagement" },
            { selector: ':nth-child(24) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "TimestampMDOrder" },
            { selector: ':nth-child(25) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "AccessPTGOutcome" },
            { selector: ':nth-child(26) > .checkbox > .ng-binding > .rolegrp_flg_pc', name: "Surveyor" },
        ]
    },
    {
        main: "HumanResources",
        selector: ':nth-child(3) > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-pristine',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_hr', name: "AddEditusers" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_hr', name: "DeletePersonnel" },
            { selector: ':nth-child(3) > .checkbox > .ng-binding > .rolegrp_flg_hr', name: "Resetuseraccountpassword" },
            { selector: ':nth-child(4) > .checkbox > .ng-binding > .rolegrp_flg_hr', name: "Accessuserinfo" },
            { selector: ':nth-child(5) > .checkbox > .ng-binding > .rolegrp_flg_hr', name: "Accessroletemplatemanager" },
            { selector: ':nth-child(6) > .checkbox > .ng-binding > .rolegrp_flg_hr', name: "AllowEsign" },
            { selector: ':nth-child(7) > .checkbox > .ng-binding > .rolegrp_flg_hr', name: "UserAccountCreation" },
        ]
    },
    {
        main: "SystemPreference",
        selector: ':nth-child(4) > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-pristine',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_syp', name: "Mail" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_syp', name: "Accountprofilemodification" },
        ]
    },
    {
        main: "MedicalResources",
        selector: ':nth-child(5) > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-pristine',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_mr', name: "Physicianmanagement" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_mr', name: "Referralmanagement" },
            { selector: ':nth-child(3) > .checkbox > .ng-binding > .rolegrp_flg_mr', name: "Insurancemanagement" },
            { selector: ':nth-child(4) > .checkbox > .ng-binding > .rolegrp_flg_mr', name: "Facilitymanagement" },
            { selector: ':nth-child(5) > .checkbox > .ng-binding > .rolegrp_flg_mr', name: "Companies" },
            { selector: ':nth-child(6) > .checkbox > .ng-binding > .rolegrp_flg_mr', name: "Emergencyservices" },
        ]
    },
    {
        main: "DocumentsReports",
        selector: ':nth-child(6) > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-pristine',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_rpt', name: "Accessclinicaltasksreports" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_rpt', name: "Accesstherapytasksreports" },
            { selector: ':nth-child(3) > .checkbox > .ng-binding > .rolegrp_flg_rpt', name: "Accesscensusreports" },
            { selector: ':nth-child(4) > .checkbox > .ng-binding > .rolegrp_flg_rpt', name: "Accessadminreports" },
            { selector: ':nth-child(5) > .checkbox > .ng-binding > .rolegrp_flg_rpt', name: "Accessclinicianvisitreport" },
        ]
    },
    {
        main: "Utilities",
        selector: ':nth-child(7) > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-pristine',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_uti', name: "MDOrderManager" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_uti', name: "Announcements" },
            { selector: ':nth-child(3) > .checkbox > .ng-binding > .rolegrp_flg_uti', name: "Faxmanager" },
            { selector: ':nth-child(4) > .checkbox > .ng-binding > .rolegrp_flg_uti', name: "Visitmapper" },
            { selector: ':nth-child(5) > .checkbox > .ng-binding > .rolegrp_flg_uti', name: "MedicalSupplies" },
        ]
    },
    {
        main: "BillingManager",
        selector: ':nth-child(8) > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-pristine',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_bm', name: "ExportOASIS" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_bm', name: "Processclaims" },
            { selector: ':nth-child(3) > .checkbox > .ng-binding > .rolegrp_flg_bm', name: "Coder" },
        ]
    },
    {
        main: "Accounting",
        selector: ':nth-child(9) > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-pristine',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_acc', name: "ClinicianReport" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_acc', name: "ClinicianRates" },
            { selector: ':nth-child(3) > .checkbox > .ng-binding > .rolegrp_flg_acc', name: "AgencyRates" },
            { selector: ':nth-child(4) > .checkbox > .ng-binding > .rolegrp_flg_acc', name: "AgencyPeriod" },
        ]
    },
    {
        main: "Libraries",
        selector: ':nth-child(10) > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-pristine',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_lib', name: "Manageblankforms" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_lib', name: "Accessblankforms" },
            { selector: ':nth-child(3) > .checkbox > .ng-binding > .rolegrp_flg_lib', name: "Managedocumentations" },
            { selector: ':nth-child(4) > .checkbox > .ng-binding > .rolegrp_flg_lib', name: "Accessdocumentations" },
            { selector: ':nth-child(5) > .checkbox > .ng-binding > .rolegrp_flg_lib', name: "Templates" },
        ]
    },
    {
        main: "SystemSettings",
        selector: ':nth-child(11) > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-pristine',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_sys', name: "ChangeSystemSettings" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_sys', name: "Changeagencyinformation" },
            { selector: ':nth-child(3) > .checkbox > .ng-binding > .rolegrp_flg_sys', name: "Accesssystemlogs" },
            { selector: ':nth-child(4) > .checkbox > .ng-binding > .rolegrp_flg_sys', name: "Accessparameters" },
        ]
    },
    {
        main: "Dashboard",
        selector: ':nth-child(12) > tbody > :nth-child(1) > .global__table-section > .checkbox > .m-b-0 > .ng-pristine',
        sub: [
            { selector: ':nth-child(1) > .checkbox > .ng-binding > .rolegrp_flg_dash', name: "QualityAssuranceDashboard" },
            { selector: ':nth-child(2) > .checkbox > .ng-binding > .rolegrp_flg_dash', name: "QAReviewerAssignment" },
        ]
    }, 
]
