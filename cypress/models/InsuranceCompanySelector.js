export default class InsuranceCompanySelector{
    get name(){
        return cy.get(':nth-child(2) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get abbr(){
        return cy.get(':nth-child(3) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get contactperson(){
        return cy.get(':nth-child(4) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get address(){
        return cy.get(':nth-child(5) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get city(){
        return cy.get(':nth-child(6) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get stateClick(){
        return cy.get('#state_chosen > .chosen-single')
    }
    get stateEnter(){
        return cy.get('#state_chosen > div > div > input[type=text]')
    }
    get zip(){
        return cy.get('#zipCode')
    }
    get phone(){
        return cy.get(':nth-child(9) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get fax(){
        return cy.get(':nth-child(10) > .oasis__answer > .fg-line > .global__txtbox')
    }
    
    get payertypeClick(){
        return cy.get(':nth-child(11) > .oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get payertypeEnter(){
        return cy.get('#content > data > div.container.p-15.global__form_center-true.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(11) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }

    get invoicetypeClick(){
        return cy.get(':nth-child(12) > .oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get invoicetypeEnter(){
        return cy.get('#content > data > div.container.p-15.global__form_center-true.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(12) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }
   

    get typeofbillClick(){
        return cy.get(':nth-child(13) > .oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get typeofbillEnter(){
        return cy.get('#content > data > div.container.p-15.global__form_center-true.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(13) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }

    get chargetypeClick(){
        return cy.get(':nth-child(14) > .oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get chargetypeEnter(){
        return cy.get('#content > data > div.container.p-15.global__form_center-true.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(14) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }

    get parentinsuranceClick(){
        return cy.get(':nth-child(15) > .oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get parentinsuranceEnter(){
        return cy.get('#content > data > div.container.p-15.global__form_center-true.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(15) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }

    get entermediaryClick(){
        return cy.get(':nth-child(16) > .oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get entermediaryEnter(){
        return cy.get('#content > data > div.container.p-15.global__form_center-true.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(16) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }

    get providercode(){
        return cy.get(':nth-child(17) > .oasis__answer > .fg-line > .global__txtbox')
    }
    get providerno(){
        return cy.get(':nth-child(18) > .oasis__answer > .fg-line > .global__txtbox')
    }

    get insurancewilluseClick(){
        return cy.get(':nth-child(19) > .oasis__answer > .fg-line > .select > .chosen-container > .chosen-single')
    }
    get insurancewilluseEnter(){
        return cy.get('#content > data > div.container.p-15.global__form_center-true.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(19) > td.oasis__answer > div > div > div > div > div > input[type=text]')
    }

    get save(){
        return cy.get('.btn__success')
    }

    get followmedicareCheck(){
        return cy.get('#content > data > div.container.p-15.global__form_center-true.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(22) > td > div:nth-child(1) > label:nth-child(1) > input')
    }
    get exportableOASISCheck(){
        return cy.get('#content > data > div.container.p-15.global__form_center-true.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(22) > td > div:nth-child(2) > label > input')
    }
    get comprehensiveEvaluationCheck(){
        return cy.get('#content > data > div.container.p-15.global__form_center-true.ng-scope.global__form_center-false > div > form > fieldset > div > table > tbody > tr:nth-child(22) > td > div:nth-child(1) > label:nth-child(2) > input')
    }
   

}