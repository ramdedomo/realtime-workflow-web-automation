export default {
    TemplateName: "Test Role Template Automation",
    PatientManager: {
        selectall: true, //if editing remain this as true if you have true value on sub
        sub: {
            Intake: true,
            Deletepatientinformation: true,
            Accesspatientgrantedtouser: true,
            Accesspatientroster: true,
        }
    },
    PatientCare: {
        selectall: true,
        sub: {
            Schedulepatientvisitstasks: true,
            Authorizationmanager: true,
            Uploadmanagemedicalrecords: true,
            Managevisitstasks: true,
            Manageuploads: true,
            Allowcommenting: true,
            Approvedocumentsandevaluation: true,
            LockUnlockOASIS: true,
            TranscribeMDOrders: true,
            MDOrderManagement: true,
            Createcommunicationnotes: true,
            Communicationnotesmanagement: true,
            Specialproceduremanagement: true,
            Createmedicationprofile: true,
            Medicationprofilemanagement: true,
            Miscellaneous: true,
            OASIS: true,
            RecordManagementOASISnotincluded: true,
            Revert: true,
            Caseconferencemanagement: true,
            AccessQASIS: true,
            CreateEvents: true,
            EventsManagement: true,
            TimestampMDOrder: true,
            AccessPTGOutcome: true,
            Surveyor: true,
        }
    },
    HumanResources: {
        selectall: true,
        sub: {
            AddEditusers: true,
            DeletePersonnel: true,
            Resetuseraccountpassword: true,
            Accessuserinfo: true,
            Accessroletemplatemanager: true,
            AllowEsign: true,
            UserAccountCreation: true,
        }
    },
    SystemPreference:{
        selectall: true,
        sub: {          
            Mail: true,
            Accountprofilemodification: true,
        }
    },
    MedicalResources:{
        selectall: true,
        sub: {          
            Physicianmanagement: true,
            Referralmanagement: true,
            Insurancemanagement: true,
            Facilitymanagement: true,
            Companies: true,
            Emergencyservices: true,
        }
    },
    DocumentsReports:{
        selectall: true,
        sub: {          
            Accessclinicaltasksreports: true,
            Accesstherapytasksreports: true,
            Accesscensusreports: true,
            Accessadminreports: true,
            Accessclinicianvisitreport: true,
        }
    },
    Utilities:{
        selectall: true,
        sub: {          
            MDOrderManager: true,
            Announcements: true,
            Faxmanager: true,
            Visitmapper: true,
            MedicalSupplies: true,
        }
    },
    BillingManager:{
        selectall: true,
        sub: {          
            ExportOASIS: true,
            Processclaims: true,
            Coder: true,
        }
    },
    Accounting:{
        selectall: true,
        sub: {          
            ClinicianReport: true,
            ClinicianRates: true,
            AgencyRates: true,
            AgencyPeriod: true,
        }
    },
    Libraries:{
        selectall: true,
        sub: {          
            Manageblankforms: true,
            Accessblankforms: true,
            Managedocumentations: true,
            Accessdocumentations: true,
            Templates: true,
        }
    },
    SystemSettings:{
        selectall: true,
        sub: {          
            ChangeSystemSettings: true,
            Changeagencyinformation: true,
            Accesssystemlogs: true,
            Accessparameters: true,
        }
    },
    Dashboard:{
        selectall: true,
        sub: {          
            QualityAssuranceDashboard: true,
            QAReviewerAssignment: true,
        }
    },
}
  